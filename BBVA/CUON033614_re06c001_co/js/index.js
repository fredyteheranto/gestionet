﻿//version de la plantilla 2.0
var jsSCORM = '',
separadorBarraInferior = 108,
scoActual = 0,
TrakActual = 0,
cargaXMLcompleta = !1,
subMenus = new Array,
idsubMenus = 0,
subMnuActual = 0,
pertenece = new Array,
intervaloEsperaApi,
TrakUltimo = 0,
metido = !1,
tmpAyuda = '',
rutaActual = '',
fileActual = '',
online = !1,
nombreArchivo = '',
nombreArchivoXML = '',
tablaCompletaLista = '',
Evaluaciones = new Array,
ListaEvaluaciones = new Array,
MostrarTabla = !0,
DatoSuspend = '',
TotalEvaluaciones = 0,
XMLEvaluaciones = new Array,
CalificacionTotalSCOS = 0,
totalSunMnu = 0,
VentanaSalida = '',
esultimosco = !1,
esprimero = !1,
inactivarNext = !0,
ultimoLocguardado = '',
ultimoEstado = '',
SilenciosaEval = !1,
SilencioEvalInteracciones = !1,
navtamano,
brw,
esperaiframeFF2,
_Debug = !1,
DesdeTema = !1,
botones = [
    'API 2004',
    'API EXTENDIDA SUMTOTAL',
    'Iniciar',
    'leer suspendata',
    'guardar suspendata',
    'leer status',
    'leer pasado',
    'completado',
    'incompleto',
    'pasado',
    'reprobado',
    'leer calificacion',
    'guardar calificacion',
    'anterior sco',
    'siguiente sco',
    'vaciar'
],
strfinal = '',
idlink,
APIEXT = null,
TimerCarga,
TiempoCarga = 5,
evitarefrescar = !1;
$(document).ready(function (a) {
    function e() {
        N = a(window).width(),
        B = a(window).height(),
        a('#tablacontenidos').css({
            height: B - separadorBarraInferior + 16
        }),
        a('#tablaContenido').css({
            height: B - separadorBarraInferior + 16
        }),
        a('#EvaluacionesDetalles').css({
            left: N / 2,
            top: B / 2
        }),
        a('#todo').css({
            width: N,
            height: B
        }),
        a('#cuerpo').css({
            height: B - separadorBarraInferior
        }),
        a('#ifr').css({
            width: N,
            height: B - separadorBarraInferior
        }),
        t()
    }
    function t() {
        var e = 0;
        if (a.browser.msie) {
            if (6 == a.browser.version) {
                a('#todo').addClass('fixed-top'),
                a('#alertas').addClass('fixed-top'),
                a('#alertas_ventana').addClass('fixed-top'),
                a('#pie').offset({
                    top: B - separadorBarraInferior + a('#ifr').position().top
                }),
                a('#pie').position().top - (B - separadorBarraInferior) != 70 ? (e = a('#pie').position().top - (B - separadorBarraInferior) - 70, a('#pie').offset({
                    top: B - separadorBarraInferior + a('#ifr').position().top - e
                }), navtamano = e)  : navtamano = 'undefined';
                {
                    a('#pie').position()
                }
                a('#navegacion').height() > 16 && (e = a('#pie').position().top - (B - separadorBarraInferior) - 70, a('#ifr').height(B - e - separadorBarraInferior - 16))
            }
            brw.version > 6 && brw.version < 11 && (a('#navegacion').css('height', 'auto'), navtamano = a('#navegacion').height(), a('#pie').offset({
                top: B - separadorBarraInferior + a('#ifr').position().top
            }), a('#navegacion').height() > 16 && (a('#pie').position().top - (B - separadorBarraInferior) != 70 ? (e = a('#pie').position().top - (B - separadorBarraInferior) - 70, a('#pie').offset({
                top: B - separadorBarraInferior + a('#ifr').position().top - e
            }))  : navtamano = 'undefined', a('#ifr').height(B - e - separadorBarraInferior)))
        } else ('ff' == brw.code || 'sf' == brw.code | 'ch' == brw.code || 'op' == brw.code) && (a('#navegacion').css('height', 'auto'), navtamano = a('#navegacion').height(), a('#pie').offset({
            top: B - separadorBarraInferior + a('#ifr').position().top
        }), a('#navegacion').height() > 16 && (a('#pie').position().top - (B - separadorBarraInferior) != 70 ? (e = a('#pie').position().top - (B - separadorBarraInferior) - 70, a('#pie').offset({
            top: B - separadorBarraInferior + a('#ifr').position().top - e
        }))  : navtamano = 'undefined', a('#ifr').height(B - e - separadorBarraInferior)))
    }
    function i() {
        _(esperaiframeFF2)
    }
    function n() {
        jsSCORM = 'js/api2004.js';
        try {
            waitForAPI(),
            r()
        } catch (a) {
        }
    }
    function r() {
        if (APIOK() || !online) {
            G('Iniciar', '');
            var a,
            e,
            t = new Array;
            ultimoEstado = G('LeerStatus', '');
            var i,
            n,
            o,
            u,
            d,
            m,
            f = G('LeerUltimoSCO', ''),
            p = G('LeerDato', ''),
            v = new Array,
            h = new Array,
            g = new Array,
            S = new Array,
            b = '',
            k = new Array,
            w = new Array,
            C = new Array,
            O = new Array;
            if (b = p + '<br/>', TotalEvaluaciones > 0 && '' != p && 'false' != p && 'undefined' != p && 'null' != p && null != p && 0 != p) {
                - 1 != p.indexOf('|') ? v = p.split('|')  : v[0] = p;
                for (var T = 0; T < v.length; T++) {
                    if (h = v[T].split('-'), i = parseInt(h[0]), u = parseInt(h[1]), o = parseInt(h[2]), n = parseInt(h[3]), h[4]) {
                        g = String(h[4]).split(',');
                        for (var A = 0; A < g.length; A++) S[A] = parseInt(g[A])
                    } else S = new Array;
                    if (h[5]) for (k = String(h[5]).split(','), A = 0; A < k.length; A++) C[A] = parseInt(k[A]);
                     else C = new Array;
                    if (h[6]) for (w = String(h[6]).split(','), A = 0; A < w.length; A++) O[A] = parseInt(w[A]);
                     else O = new Array;
                    d = parseFloat(h[7]),
                    m = parseInt(h[8]),
                    b += S.join(',') + ' id ' + i + '| ',
                    TotalEvaluaciones > 0 && (Evaluaciones[i].intentos = n, Evaluaciones[i].estado = u, Evaluaciones[i].peso = o, Evaluaciones[i].calificaciones = S, Evaluaciones[i].stok = C, Evaluaciones[i].stoksel = O, Evaluaciones[i].tiempo = d, Evaluaciones[i].aciertos = m)
                }
            }
            if ( - 1 != f.indexOf('|') && (t = f.split('|'), e = String(t[0]), a = String(t[1])), null != a && '' != a && 'false' != a && 'unknow' != a && 'undefined' != a) {
                TrakActual = parseInt(e),
                TrakUltimo = parseInt(a);
                {
                    new Array
                }
                _(TrakActual)
            } else l(TrakActual);
            s(),
            c('PrimeraVentana', !1),
            MostrarTabla ? c('btntcontenidos', !0)  : c('btntcontenidos', !1),
            evitarefrescar = !0,
            c('todo', !0),
            G('GuardarIncompleto', '')
        } else setTimeout(r, 1000)
    }
    function o() {
        metido = !metido,
        metido ? a('#tablacontenidos').animate({
            left: '-250'
        }, 700)  : MostrarTabla && a('#tablacontenidos').animate({
            left: '0'
        }, 700)
    }
    function s() {
        var e = 0,
        t = '',
        i = '',
        n = 0;
        tablaCompletaLista = '';
        for (var r = 0; r < D.length; r++) {
            if ('tema' == D[r].t) e++;
             else if ('mnu' == D[r].t) {
                n = D[r].imalinksIds.length;
                for (var o = 0; n > o; o++) D[r].imalinksIds[o] + 1 < D.length ? TrakUltimo >= D[r].imalinksIds[o] && (t = D[D[r].imalinksIds[o] + 1], D[TrakUltimo].submnu > D[r].submnu ? D[r].imalinks[o] = 'url(img/Plantilla/toc/c.gif)' : 'tema' == t.t || 'mnu' == t.t || 'eval' == t.t ? D[r].imalinks[o] = 'url(img/Plantilla/toc/c.gif)' : 'subtema' == t.t && ('url(img/Plantilla/toc/na.gif)' == D[r].imalinks[o] && (D[r].imalinks[o] = 'url(img/Plantilla/toc/i.gif)'), n > o + 1 ? TrakUltimo >= D[r].imalinksIds[o + 1] - 1 && (D[r].imalinks[o] = 'url(img/Plantilla/toc/c.gif)')  : TrakUltimo + 1 < D.length ? (i = D[TrakUltimo + 1], ('mnu' == i.t || 'eval' == i.t) && (D[r].imalinks[o] = 'url(img/Plantilla/toc/c.gif)'))  : TrakUltimo == D.length - 1 && (D[r].imalinks[o] = 'url(img/Plantilla/toc/c.gif)')))  : TrakUltimo + 1 == D.length && (D[r].imalinks[o] = 'url(img/Plantilla/toc/c.gif)')
            }('mnu' == D[r].t || 'eval' == D[r].t || 'portada' == D[r].t) && ('' != D[r].titulosuperior && (tablaCompletaLista += '<li id=\'tctisup_' + r + '\'><span class=\'TCsup\'>' + D[r].titulosuperior + '</span></li>'), '' != D[r].titulogrupo && (tablaCompletaLista += '<li id=\'tctigrp_' + r + '\'><span class=\'TCgrp\'>' + D[r].titulogrupo + '</span></li>'), tablaCompletaLista += '<li id=\'tclista_' + r + '\'><a id=\'tclnk\' href=\'#\'><div class=\'statusSCO\' id=\'sp_' + r + '\'>' + D[r].titulosubmnu + '</div></a></li>')
        }
        var s = '<div id=\'tablaTitulo\' class=\'TCTit\'>' + String(L[15]) + '</div>',
        l = '<br/><ul id=\'TCList\'>' + tablaCompletaLista + '</ul>';
        l += '<div id=\'detalles\'>Detalles</div>',
        a('#tablaContenido').html(s + l),
        0 == TotalEvaluaciones && c('detalles', !1);
        a('#tablaContenido').find('*').each(function () {
            var e,
            t,
            i,
            n = String(a(this).attr('id'));
            if ( - 1 != n.indexOf('_')) switch (e = n.split('_'), t = parseInt(e[1]), i = String(e[0])) {
                case 'tclista':
                    t > TrakUltimo + 1 && a(this).find('#tclnk').each(function () {
                        a(this).css('cursor', 'default'),
                        a(this).hover(function () {
                            a(this).css('background-color', '#2E9AFE')
                        }, function () {
                            a(this).css('background-color', '#2E9AFE')
                        })
                    }),
                    a(this).click(function () {
                        TrakUltimo + 1 >= t && _(t)
                    });
                    break;
                case 'sp':
                    var r = '0';
                    'eval' == D[t].t ? r = String(Evaluaciones[t].estado)  : 'mnu' == D[t].t && (D[t].submnu < D[TrakUltimo].submnu ? r = '2' : D[t].submnu == D[TrakUltimo].submnu && (TrakUltimo + 1 < D.length ? D[t].submnu < D[TrakUltimo + 1].submnu ? r = '2' : D[t].submnu == D[TrakUltimo + 1].submnu && (r = '1')  : TrakUltimo + 1 == D.length && (r = '2'))),
                    a(this).css('background-image', 'url(img/Plantilla/estadoSCO/' + r + '.png)')
            } else 'detalles' == n && a(this).click(function () {
                var e = !1;
                if (TotalEvaluaciones > 0) {
                    var t = '<div id=\'DEtitulo\'>Resultados<span id=\'DEcerrar\'>X</span></div>';
                    t += '<div id=\'DEcontenido\'>';
                    for (var i = 0; i < ListaEvaluaciones.length; i++) {
                        e = !0,
                        t += '<div id=\'DEseparador\'>',
                        t += '<div id=\'DEnombre\'>' + D[ListaEvaluaciones[i]].titulosubmnu + '</div>';
                        for (var n = 0; n < Evaluaciones[ListaEvaluaciones[i]].calificaciones.length; n++) t += '<div id=\'DEcalif\'>Intento ' + (n + 1) + '  calificación obtenida ' + Evaluaciones[ListaEvaluaciones[i]].calificaciones[n] + '</div>'
                    }
                    t += '</div>'
                }
                e && (a('#EvaluacionesDetalles').html(t), c('EvaluacionesDetalles', !0), a('#DEcerrar').click(function () {
                    c('EvaluacionesDetalles', !1)
                }))
            })
        })
    }
    function l() {
        var i = '';
        tmpAyuda = '';
        var n = 0;
        i = '' != D[TrakActual].folder && null != D[TrakActual].folder ? D[TrakActual].folder + '/' + D[TrakActual].html : D[TrakActual].html,
        c('progreso,index,atras,siguiente,pie,navegacion,cabecera', !0),
        c('alertas,alertas_ventana,btnresultados', !1),
        '' != L[14] ? c('ayuda', !0)  : c('ayuda', !1),
        '' != L[17] ? c('glosario', !0)  : c('glosario', !1),
        '' != L[16] ? c('documentos', !0)  : c('documentos', !1),
        MostrarTabla && (c('btntcontenidos', !0), M || (M = !1, k()), metido = !1, o()),
        F = x,
        U = I,
        DesdeTema ? a('#ifr').attr('src', i)  : window.open(i, 'ifr', 'params'),
        DesdeTema = !1,
        esprimero && a('#atras').css({
            opacity: '1',
            cursor: 'pointer'
        }),
        inactivarBack = !0,
        'mnu' == D[TrakActual].t ? (TrakUltimo = TrakActual > TrakUltimo ? TrakActual : TrakUltimo, c('index', !1), a('#siguiente').css({
            opacity: '1',
            cursor: 'pointer'
        }), inactivarNext = !0, esprimero && 0 == TrakActual && (a('#atras').css({
            opacity: '.2',
            cursor: 'default'
        }), inactivarBack = !1))  : 'eval' == D[TrakActual].t ? (c('index', !1), a('#siguiente').css({
            opacity: '1',
            cursor: 'pointer'
        }))  : (TrakUltimo = TrakActual > TrakUltimo ? TrakActual : TrakUltimo, c('index', !0), D.length - 1 == TrakActual && esultimosco ? (inactivarNext = !1, a('#siguiente').css({
            opacity: '0.2',
            cursor: 'default'
        }))  : (a('#siguiente').css({
            opacity: '1',
            cursor: 'pointer'
        }), inactivarNext = !0)),
        S(D[TrakActual].textonavegacion),
        g(D[TrakActual].paginar),
        n = parseInt((TrakUltimo + 1) / D.length * 10),
        0 == TrakUltimo && (n = 0),
        f(),
        s(),
        'ie' == brw.code && '6' == brw.version ? setTimeout(t, 500)  : setTimeout(e, 500)
    }
    function c(e, t) {
        var i = new Array;
        - 1 != e.indexOf(',') ? i = e.split(',')  : i[0] = e;
        for (var n = 0; n < i.length; n++) t ? a('div#' + i[n]).show()  : a('div#' + i[n]).hide()
    }
    function u(e) {
        a('#progreso').css('background-image', 'url(' + e + ')')
    }
    function d() {
        for (var a = new Array, e = 0, t = 0, i = 0; i < Evaluaciones.length; i++) if (void 0 != Evaluaciones[i] && null != Evaluaciones[i]) {
            if (a.push(Evaluaciones[i].id + '-' + Evaluaciones[i].estado + '-' + Evaluaciones[i].peso + '-' + Evaluaciones[i].intentos + '-' + Evaluaciones[i].calificaciones.join(',') + '-' + Evaluaciones[i].stok.join(',') + '-' + Evaluaciones[i].stoksel.join(',') + '-' + Evaluaciones[i].tiempo + '-' + Evaluaciones[i].aciertos), t = - 1, Evaluaciones[i].calificaciones.length > 0) {
                t = Evaluaciones[i].calificaciones[0];
                for (var n = 1; n < Evaluaciones[i].calificaciones.length; n++) Evaluaciones[i].calificaciones[n] > t && (t = Evaluaciones[i].calificaciones[n])
            }
            e += - 1 != t ? parseInt(t * (Evaluaciones[i].peso / 100))  : 0
        }
        DatoSuspend = a.join('|'),
        G('GuardarDato', DatoSuspend),
        SilenciosaEval || G('GuardarCalificacion', e);
        var r = G('LeerPasado', '');
        'passed' != r && 'failed' != r && m() && !SilenciosaEval && (e >= L[5] ? G('GuardarPasado', '')  : G('GuardarFallado', '')),
        s(),
        f()
    }
    function m() {
        for (var a, e = 0, t = 0; t < ListaEvaluaciones.length; t++) a = Evaluaciones[ListaEvaluaciones[t]],
        a.estado > 1 && e++;
        return e == ListaEvaluaciones.length ? !0 : !1
    }
    function f() {
        if (G('GuardarUltimoSCO', TrakActual + '|' + TrakUltimo), TrakUltimo == D.length - 1) if (0 == TotalEvaluaciones) G('GuardarCompleto', '');
         else {
            for (var a, e = 0, t = 0; t < ListaEvaluaciones.length; t++) a = Evaluaciones[ListaEvaluaciones[t]],
            a.estado > 1 && e++;
            e == ListaEvaluaciones.length && G('GuardarCompleto', '')
        } else G('GuardarIncompleto', '');
        var i = (TrakUltimo + 1) / D.length;
        i >= 0 && 1 >= i && G('GuardarProgreso', i)
    }
    function p(a) {
        G('GuardarObjetivo', a)
    }
    function v(a) {
        SilencioEvalInteracciones || G('GuardarInteracciones', a)
    }
    function h(a) {
        var e = a.split('/'),
        t = 1,
        i = 1,
        n = 0;
        return t = parseInt(e[0]),
        i = parseInt(e[1]),
        n = parseInt(t / i * 10)
    }
    function g(e) {
        var t = 0;
        if (D[TrakUltimo].submnu > D[TrakActual].submnu) t = 10;
         else if (D[TrakUltimo].submnu == D[TrakActual].submnu) if ('mnu' == D[TrakUltimo].t) t = 0;
         else if ('eval' == D[TrakUltimo].t) t = 10;
         else {
            for (var i = - 1, n = - 1, r = 0, o = 0; o < D.length; o++) D[o].submnu == D[TrakUltimo].submnu && (i++, TrakUltimo >= o && n++);
            r = parseInt(n / i * 9) + 1,
            t = r
        } else D[TrakUltimo].submnu < D[TrakActual].submnu && 'eval' == D[TrakActual].t && (t = '1' == String(e.split('/').shift()) ? 0 : h(e));
        u('img/Plantilla/progreso/progreso_' + t + '.png'),
        a('#numeracion').html(e)
    }
    function S(e) {
        a('#navegacion').html(e),
        a('#navegacion').find('*').each(function () {
            var e,
            t,
            i,
            n = String(a(this).attr('id'));
            if ( - 1 != n.indexOf('_')) switch (e = n.split('_'), t = parseInt(e[1]), i = String(e[0])) {
                case 'nav':
                    a(this).click(function () {
                        _(t)
                    })
            }
        })
    }
    function b() {
        var e = !1;
        if (TotalEvaluaciones > 0) {
            var t = '<div id=\'DEtitulo\'>Resultados</div><div id=\'DEcerrar\'>X</div>';
            t += '<div id=\'DEcontenido\'>';
            for (var i = 0; i < ListaEvaluaciones.length; i++) {
                e = !0,
                t += '<div id=\'DEseparador\'>',
                t += '<div id=\'DEnombre\'>' + D[ListaEvaluaciones[i]].titulosubmnu + '</div>';
                for (var n = 0; n < Evaluaciones[ListaEvaluaciones[i]].calificaciones.length; n++) t += '<div id=\'DEcalif\'>Intento ' + (n + 1) + '  calificación obtenida ' + Evaluaciones[ListaEvaluaciones[i]].calificaciones[n] + '</div>'
            }
            t += '</div>'
        }
        e && (a('#EvaluacionesDetalles').html(t), c('EvaluacionesDetalles', !0), a('#DEcerrar').click(function () {
            c('EvaluacionesDetalles', !1)
        }))
    }
    function k() {
        M = !M,
        M ? (a('#btntcontenidos').css('background-image', 'url(img/Plantilla/toc_show.png)'), c('ifr', !0))  : (a('#btntcontenidos').css('background-image', 'url(img/Plantilla/toc_hide.png)'), c('ifr', !1)),
        MostrarTabla && c('tablacontenidos', !0),
        o()
    }
    function w(a) {
        var e = 'height=400,width=650,scrollTo,resizable=1,scrollbars=1,location=0';
        return '' != a && void 0 != a && null != a && (nueva = window.open(a, 'Popup', e)),
        !1
    }
    function C(a) {
        var e = 'height=400,width=650,scrollTo,resizable=1,scrollbars=1,location=0';
        return '' != a && void 0 != a && null != a && (nueva = window.open(a, 'Popup', e)),
        !1
    }
    function O(a) {
        var e = 'height=400,width=650,scrollTo,resizable=1,scrollbars=1,location=0';
        return '' != a && void 0 != a && null != a && (nueva = window.open(a, 'Popup', e)),
        !1
    }
    function T() {
        window.Alertas('¿Estás seguro que deseas salir?', 'confirmar', A)
    }
    function A(e) {
        e && (c('todo,tablacontenidos', !1), - 1 != VentanaSalida.indexOf('.htm') ? (a('#PrimeraVentana').css('top', 50), a('#PrimeraVentana').css('left', 100), a('#PrimeraVentana').css('width', N), a('#PrimeraVentana').css('height', B), a('#PrimeraVentana').html('<iframe id=\'ifrFinal\' name=\'ifrFinal\' frameborder=\'0\' scrolling=\'no\'></iframe>'), window.open(VentanaSalida, 'ifrFinal', 'params'), a('#ifrFinal').css('top', 50), a('#ifrFinal').css('left', 100), a('#ifrFinal').css('width', N), a('#ifrFinal').css('height', B))  : a('#PrimeraVentana').html('Ahora puede cerrar la ventana'), c('PrimeraVentana', !0), top.frames.length > 0 ? window.top.close()  : window.close())
    }
    function _(a) {
        TrakActual = a,
        l(TrakActual)
    }
    function E() {
        TrakActual = D[TrakActual].idSubActual,
        l(TrakActual)
    }
    function I() {
        TrakActual - 1 >= 0 ? (TrakActual--, l(TrakActual))  : inactivarBack && 'false' != G('SePuedeAnteriorSCO', '') && (G('AnteriorSCO', ''), c('atras', !1))
    }
    function x() {
        if (inactivarNext) {
            inactivarBack = !0;
            TrakActual + 1 < D.length ? (TrakActual++, l(TrakActual), TrakActual + 1 == D.length && f())  : 'false' != G('SePuedeSiguienteSCO', '') && (G('SiguienteSCO', ''), inactivarNext = !1, setTimeout(function () {
                inactivarNext || G('SiguienteSCO', '')
            }, 5000))
        }
    }
    function y(a, e) {
    }
    function V() {
        this.fullName = 'unknow',
        this.name = 'unknow',
        this.code = 'unknow',
        this.fullVersion = 'unknow',
        this.version = 'unknow',
        this.mobile = !1,
        this.width = screen.width,
        this.height = screen.height,
        this.platform = 'unknow',
        this.init = function () {
            var a,
            e = [
                {
                    name: 'Opera Mobi',
                    fullName: 'Opera Mobile',
                    pre: 'Version/'
                },
                {
                    name: 'Opera Mini',
                    fullName: 'Opera Mini',
                    pre: 'Version/'
                },
                {
                    name: 'Opera',
                    fullName: 'Opera',
                    pre: 'Version/'
                },
                {
                    name: 'MSIE',
                    fullName: 'Microsoft Internet Explorer',
                    pre: 'MSIE '
                },
                {
                    name: 'BlackBerry',
                    fullName: 'BlackBerry Navigator',
                    pre: '/'
                },
                {
                    name: 'BrowserNG',
                    fullName: 'Nokia Navigator',
                    pre: 'BrowserNG/'
                },
                {
                    name: 'Midori',
                    fullName: 'Midori',
                    pre: 'Midori/'
                },
                {
                    name: 'Kazehakase',
                    fullName: 'Kazehakase',
                    pre: 'Kazehakase/'
                },
                {
                    name: 'Chromium',
                    fullName: 'Chromium',
                    pre: 'Chromium/'
                },
                {
                    name: 'Flock',
                    fullName: 'Flock',
                    pre: 'Flock/'
                },
                {
                    name: 'Galeon',
                    fullName: 'Galeon',
                    pre: 'Galeon/'
                },
                {
                    name: 'RockMelt',
                    fullName: 'RockMelt',
                    pre: 'RockMelt/'
                },
                {
                    name: 'Fennec',
                    fullName: 'Fennec',
                    pre: 'Fennec/'
                },
                {
                    name: 'Konqueror',
                    fullName: 'Konqueror',
                    pre: 'Konqueror/'
                },
                {
                    name: 'Arora',
                    fullName: 'Arora',
                    pre: 'Arora/'
                },
                {
                    name: 'Swiftfox',
                    fullName: 'Swiftfox',
                    pre: 'Firefox/'
                },
                {
                    name: 'Maxthon',
                    fullName: 'Maxthon',
                    pre: 'Maxthon/'
                },
                {
                    name: 'Firefox',
                    fullName: 'Mozilla Firefox',
                    pre: 'Firefox/'
                },
                {
                    name: 'Chrome',
                    fullName: 'Google Chrome',
                    pre: 'Chrome/'
                },
                {
                    name: 'Safari',
                    fullName: 'Apple Safari',
                    pre: 'Version/'
                }
            ],
            t = navigator.userAgent;
            for (l in e) if (t.indexOf(e[l].name) > - 1) {
                a = e[l].pre,
                this.name = e[l].name.toLowerCase(),
                this.fullName = e[l].fullName,
                'msie' == this.name && (this.name = 'iexplorer'),
                'opera mobi' == this.name && (this.name = 'opera'),
                'opera mini' == this.name && (this.name = 'opera');
                break
            }
            if ((idx = t.indexOf(a)) > - 1) {
                this.fullVersion = '',
                this.version = '';
                var i = 0,
                n = t.length,
                r = idx + a.length;
                for (j = r; n > j; j++) {
                    var o = t.charCodeAt(j);
                    o >= 48 && 57 >= o || 46 == o ? (46 == o && i++, 2 > i && (this.version += t.charAt(j)), this.fullVersion += t.charAt(j))  : j = n
                }
                this.version = parseInt(this.version)
            }
            var s = [
                'mobi',
                'mobile',
                'mini',
                'iphone',
                'ipod',
                'ipad',
                'android',
                'blackberry'
            ];
            for (var l in s) t.indexOf(s[l]) > - 1 && (this.mobile = !0);
            (this.width < 700 || this.height < 600) && (this.mobile = !0);
            var c = navigator.platform;
            ('Win32' == c || 'Win64' == c) && (this.platform = 'Windows'),
            - 1 != t.indexOf('NT 5.1') && (this.platform = 'Windows XP'),
            - 1 != t.indexOf('NT 6') && (this.platform = 'Windows Vista'),
            - 1 != t.indexOf('NT 6.1') && (this.platform = 'Windows 7'),
            - 1 != t.indexOf('Mac') && (this.platform = 'Macintosh'),
            - 1 != t.indexOf('Linux') && (this.platform = 'Linux'),
            - 1 != t.indexOf('iPhone') && (this.platform = 'iOS iPhone'),
            - 1 != t.indexOf('iPod') && (this.platform = 'iOS iPod'),
            - 1 != t.indexOf('iPad') && (this.platform = 'iOS iPad'),
            - 1 != t.indexOf('Android') && (this.platform = 'Android'),
            'unknow' != this.name && (this.code = this.name + '', 'opera' == this.name && (this.code = 'op'), 'firefox' == this.name && (this.code = 'ff'), 'chrome' == this.name && (this.code = 'ch'), 'safari' == this.name && (this.code = 'sf'), 'iexplorer' == this.name && (this.code = 'ie'), 'maxthon' == this.name && (this.code = 'mx')),
            'safari' == this.name && 'Linux' == this.platform && (this.name = 'unknow', this.fullName = 'unknow', this.code = 'unknow')
        },
        this.init()
    }
    function P(a) {
        return - 1 == String(a).indexOf('<') || - 1 == String(a).indexOf('</') && - 1 == String(a).indexOf('/>') ? !1 : !0
    }
    function G(a, e) {
        var t;
        if (APIOK()) {
            if ('1.2' == L[3]) switch (String(a)) {
                case 'Iniciar':
                    t = SCOInitialize();
                    break;
                case 'Finalizar':
                    t = SCOFinish();
                    break;
                case 'LeerPrimeravez':
                    t = SCOGetValue('cmi.core.entry');
                    break;
                case 'LeerStatus':
                    t = SCOGetValue('cmi.core.lesson_status');
                    break;
                case 'LeerPasado':
                    t = SCOGetValue('cmi.core.lesson_status');
                    break;
                case 'GuardarCompleto':
                    0 == TotalEvaluaciones && (t = SCOSetStatusCompleted(), SCOCommit());
                    break;
                case 'GuardarIncompleto':
                    'incomplete' != ultimoEstado && 'completed' != ultimoEstado && (ultimoEstado = 'incomplete', t = SCOSetValue('cmi.core.lesson_status', 'incomplete'), SCOCommit());
                    break;
                case 'GuardarPasado':
                    t = SCOSetValue('cmi.core.lesson_status', 'passed'),
                    SCOCommit();
                    break;
                case 'GuardarFallado':
                    t = SCOSetValue('cmi.core.lesson_status', 'failed'),
                    SCOCommit();
                    break;
                case 'LeerDato':
                    t = SCOGetValue('cmi.suspend_data');
                    break;
                case 'GuardarDato':
                    t = SCOSetValue('cmi.suspend_data', e),
                    SCOCommit();
                    break;
                case 'LeerCalificacion':
                    t = SCOGetValue('cmi.core.score.raw');
                    break;
                case 'GuardarCalificacion':
                    t = SCOSetValue('cmi.core.score.raw', e),
                    SCOCommit();
                    break;
                case 'LeerUltimoSCO':
                    t = SCOGetValue('cmi.core.lesson_location');
                    break;
                case 'GuardarUltimoSCO':
                    t = SCOSetValue('cmi.core.lesson_location', e),
                    SCOCommit();
                    break;
                case 'LeerProgreso':
                    t = !1;
                    break;
                case 'GuardarProgreso':
                    t = !1;
                    break;
                case 'GuardarInteracciones':
                    var i = SCOGetValue('cmi.interactions._count'),
                    n = 'cmi.interactions.' + i + '.';
                    null != e[0] && SCOSetValue(n + 'id', e[0]),
                    null != e[1] && SCOSetValue(n + 'time', e[1]),
                    null != e[2] && SCOSetValue(n + 'objectives.0.id', e[2]),
                    null != e[3] && SCOSetValue(n + 'type', normalizeInteractionType(e[3])),
                    null != e[4] && SCOSetValue(n + 'correct_responses.0.pattern', checkInteractionResponse(e[4])),
                    null != e[5] && SCOSetValue(n + 'student_response', checkInteractionResponse(e[5])),
                    null != e[6] && SCOSetValue(n + 'result', e[6]),
                    null != e[7] && SCOSetValue(n + 'weighting', e[7]),
                    null != e[8] && SCOSetValue(n + 'latency', e[8]),
                    null != e[9]
            } else if ('2004' == L[3]) switch (String(a)) {
                case 'Iniciar':
                    t = SCOInitialize();
                    break;
                case 'Finalizar':
                    t = SCOFinish();
                    break;
                case 'LeerPrimeravez':
                    t = SCOGetValue('cmi.entry');
                    break;
                case 'LeerStatus':
                    t = SCOGetValue('cmi.completion_status');
                    break;
                case 'LeerPasado':
                    t = SCOGetValue('cmi.success_status');
                    break;
                case 'GuardarCompleto':
                    'completed' != ultimoEstado && (ultimoEstado = 'completed', t = SCOSetStatusCompleted());
                    break;
                case 'GuardarIncompleto':
                    'incomplete' != ultimoEstado && 'completed' != ultimoEstado && (ultimoEstado = 'incomplete', t = SCOSetValue('cmi.completion_status', 'incomplete'));
                    break;
                case 'GuardarPasado':
                    t = SCOSetValue('cmi.success_status', 'passed');
                    break;
                case 'GuardarFallado':
                    t = SCOSetValue('cmi.success_status', 'failed');
                    break;
                case 'LeerDato':
                    t = SCOGetValue('cmi.suspend_data');
                    break;
                case 'GuardarDato':
                    TotalEvaluaciones > 0 && (t = SCOSetValue('cmi.suspend_data', e));
                    break;
                case 'LeerCalificacion':
                    t = SCOGetValue('cmi.score.raw');
                    break;
                case 'GuardarCalificacion':
                    TotalEvaluaciones > 0 && (t = SCOSetValue('cmi.score.raw', e), t = SCOSetValue('cmi.score.scaled', e / 100));
                    break;
                case 'LeerUltimoSCO':
                    t = SCOGetValue('cmi.location');
                    break;
                case 'GuardarUltimoSCO':
                    ultimoLocguardado != e && (ultimoLocguardado = e, t = SCOSetValue('cmi.location', e));
                    break;
                case 'LeerProgreso':
                    t = SCOGetValue('cmi.progress_measure');
                    break;
                case 'GuardarProgreso':
                    break;
                case 'GuardaCalifMin':
                    t = SCOSetValue('cmi.scaled_passing_score', e);
                    break;
                case 'AnteriorSCO':
                    t = SCOSetValue('adl.nav.request', 'previous'),
                    SCOFinish();
                    break;
                case 'SiguienteSCO':
                    t = SCOSetValue('adl.nav.request', 'continue'),
                    SCOFinish();
                    break;
                case 'SePuedeSiguienteSCO':
                    t = SCOGetValue('adl.nav.request_valid.continue');
                    break;
                case 'SePuedeAnteriorSCO':
                    t = SCOGetValue('adl.nav.request_valid.previous');
                    break;
                case 'GuardarInteracciones':
                    var i = SCOGetValue('cmi.interactions._count'),
                    n = 'cmi.interactions.' + i + '.';
                    null != e[0] && SCOSetValue(n + 'id', e[0]),
                    null != e[1] && SCOSetValue(n + 'timestamp', e[1]),
                    null != e[2] && SCOSetValue(n + 'objectives.0.id', e[2]),
                    null != e[3] && SCOSetValue(n + 'type', normalizeInteractionType(e[3])),
                    null != e[4] && SCOSetValue(n + 'correct_responses.0.pattern', checkInteractionResponse(e[4])),
                    null != e[5] && SCOSetValue(n + 'learner_response', checkInteractionResponse(e[5])),
                    null != e[6] && SCOSetValue(n + 'result', e[6]),
                    null != e[7] && SCOSetValue(n + 'weighting', e[7]),
                    null != e[8] && SCOSetValue(n + 'latency', e[8]),
                    null != e[9] && (P(e[9]) || SCOSetValue(n + 'description', e[9]));
                    break;
                case 'GuardarObjetivo':
                    null != e[0] && SCOSetObjectiveData(0, 'id', e[0]),
                    null != e[1] && SCOSetObjectiveData(0, 'score.scaled', e[1]),
                    null != e[2] && SCOSetObjectiveData(0, 'score.raw', e[2]),
                    null != e[3] && SCOSetObjectiveData(0, 'score.min', e[3]),
                    null != e[4] && SCOSetObjectiveData(0, 'score.max', e[4]),
                    null != e[5] && SCOSetObjectiveData(0, 'success_status', e[5]),
                    null != e[6] && SCOSetObjectiveData(0, 'completion_status', e[6]),
                    null != e[7] && SCOSetObjectiveData(0, 'progress_measure', e[7]),
                    null != e[8] && (P(e[8]) || SCOSetObjectiveData(0, 'description', e[8]))
            }
            y(String(a), t)
    }
    return 'GuardarUltimoSCO' == String(a) && 'false' == String(t) && GuardaraEnLMS && online && location.reload(),
    String(t)
}
a(document) [0].oncontextmenu = function () {
    return !1
},
a(document).keydown(function (a) {
    var e = a.keyCode ? a.keyCode : a.which;
    return 116 == e || 8 == e || 17 == e || 90 == e || 67 == e || 18 == e || 93 == e ? (a.preventDefault(), !1)  : void 0
}),
brw = new V,
c('alertas,alertas_ventana,glosario,ayuda,documentos,EvaluacionesDetalles,tablacontenidos,PrimeraVentana,todo,btnresultados', !1),
'' == rutaActual && (rutaActual = jQuery(location).attr('href'), fileActual = String(rutaActual).split(':').shift(), nombreArchivo = String(rutaActual).split('/').pop(), nombreArchivo = nombreArchivo.split('.').shift(), nombreArchivoXML = 'xml/' + nombreArchivo + '.xml', online = 'file' == fileActual ? !1 : !0);
var L = new Array,
D = new Array,
M = !0,
U = function () {
},
F = function () {
};

TimerCarga = setTimeout(function () {
    a.ajax({
        type: 'GET',
        url: nombreArchivoXML,
        cache: !1,
        dataType: a.browser.msie ? 'text' : 'xml',
        success: function (e) {
            var t;
            'string' == typeof e ? (t = new ActiveXObject('Microsoft.XMLDOM'), t.async = !1, t.loadXML(e))  : t = e,
            a(t).find('Data').each(function () {
                if (L[0] = a(this).find('IdiomaDefaul').text(), L[1] = a(this).find('Idiomas').text(), L[2] = a(this).find('NombreLocObj').text(), L[3] = a(this).find('Scorm').text(), L[4] = a(this).find('TipoSCO').text(), L[5] = parseInt(a(this).find('Calif').text()), L[6] = a(this).find('Time').text(), L[7] = a(this).find('Auto').text(), L[8] = a(this).find('MP3').text(), L[9] = a(this).find('PrefijoIdioma').text(), L[10] = a(this).find('PrefijoIdiomaCarpeta').text(), L[11] = a(this).find('CarpetaImagenes').text(), L[12] = a(this).find('CarpetaScripts').text(), L[13] = a(this).find('TablaCont').text(), L[14] = a(this).find('ayudafile').text(), L[15] = a(this).find('cursename').text(), L[16] = a(this).find('doctos').text(), L[17] = a(this).find('glos').text(), L[18] = a(this).find('vsalida').text(), L[19] = a(this).find('ultimosco').text(), L[20] = a(this).find('pimersco').text(), VentanaSalida = String(L[18]), esultimosco = 'si' == String(L[19]).toLowerCase() ? !0 : !1, esprimero = 'si' == String(L[20]).toLowerCase() ? !0 : !1, '1.2' != L[3] && '2004' != L[3]) online = !1;
                 else try {
                    SCOInitialize()
                } catch (e) {
                }
                MostrarTabla = 'no' == L[13] ? !1 : !0,
                a(this).find('Item').each(function () {
                    a(this).attr('html');
                    D.push({
                        id: a(this).attr('id'),
                        folder: String(a(this).attr('folder')),
                        html: a(this).attr('html'),
                        t: a(this).attr('t'),
                        titulosubmnu: a(this).attr('titulosubmnu'),
                        titulosuperior: a(this).attr('titulosuperior'),
                        titulogrupo: a(this).attr('titulogrupo'),
                        textonavegacion: '',
                        progreso: 'img/Plantilla/progreso/progreso_0.png',
                        botones: 'menu,progreso,glosario,documentos,ayuda,index,atras,siguiente',
                        submnu: 0,
                        links: '',
                        textolinks: '',
                        imalinks: '',
                        imalinksIds: '',
                        linkstatus: '',
                        idSubActual: '',
                        estado: '',
                        intentos: '',
                        htmlSubMenu: '',
                        paginar: '1/1'
                    })
                })
            });
            for (var i = new Array, r = new Array, o = new Array, s = new Array, l = '', u = '', d = 0, m = 0, f = '', p = '', v = 0, h = 0, g = new Array, S = - 1, b = D.length - 1; b > - 1; b--) if (l = D[b].id, u = D[b].titulosubmnu, m = 0, 'mnu' == D[b].t) D[b].paginar = '1/1',
            D[b].links = i.reverse(),
            D[b].textolinks = r.reverse(),
            D[b].imalinks = g,
            D[b].imalinksIds = s.reverse(),
            g = new Array,
            i = new Array,
            r = new Array,
            o = new Array,
            s = new Array;
             else if ('eval' == D[b].t) D[b].paginar = '';
             else if ('portada' == D[b].t) D[b].paginar = '';
             else if ('tema' == D[b].t) i.push(l),
            g.push('url(img/Plantilla/toc/na.gif)'),
            s.push(b),
            r.push(u),
            D[b].paginar = '1/' + (d + 1),
            d = 0;
             else if ('subtema' == D[b].t) {
                for (var k = b; 'subtema' == D[k].t; k--) m++;
                D[b].paginar = m + 1 + '/' + (m + d + 1),
                d++
            }
            for (b = 0; b < D.length; b++) 'mnu' == D[b].t ? (D[b].submnu = ++S, f = D[b].titulosubmnu, v = b, D[b].textonavegacion = '<span class=\'navFin\'>' + D[b].titulosubmnu + '</span>')  : 'eval' == D[b].t ? (ListaEvaluaciones.push(b), Evaluaciones[b] = {
                id: b,
                estado: 0,
                peso: 0,
                intentos: 0,
                califMax: 0,
                calificaciones: new Array,
                objetivo: TotalEvaluaciones,
                maxIntentos: 0,
                stok: new Array,
                stoksel: new Array,
                tiempo: 0,
                aciertos: 0
            }, TotalEvaluaciones++, D[b].submnu = ++S, f = '', D[b].textonavegacion = '<span class=\'navFin\'>' + D[b].titulosubmnu + '</span>')  : 'portada' == D[b].t ? (D[b].submnu = ++S, f = '', D[b].textonavegacion = '<span class=\'navFin\'>' + D[b].titulosubmnu + '</span>')  : 'tema' == D[b].t ? (D[b].submnu = S, p = D[b].titulosubmnu, h = b, D[b].idSubActual = v, D[b].textonavegacion = f, D[b].textonavegacion = '<div id=\'nav_' + v + '\' class=\'nav\'>' + f + '</div><span>   / </span><span class=\'navFin\'>' + D[b].titulosubmnu + '</span>')  : 'subtema' == D[b].t && (D[b].submnu = S, D[b].idSubActual = v, D[b].textonavegacion = '<div id=\'nav_' + v + '\' class=\'nav\'>' + f + '</div><span>   / </span><div id=\'nav_' + h + '\' class=\'nav\'>' + p + '</div><span>   ' + ('' != D[b].titulosubmnu ? '/' : '') + ' </span><span class=\'navFin\'>' + D[b].titulosubmnu + '</span>');
            totalSunMnu = S,
            c('PrimeraVentana', !0),
            n()
        },
        error: function () {
            alert('Error al cargar xml principal\nPorfavor intente nuevamente seleccionando el tema que desea.')
        }
    });
}, 1000 * TiempoCarga);



var N = a(window).width(),
B = a(window).height();
a('#tablacontenidos').css({
    height: B - separadorBarraInferior + 16
}),
a('#tablaContenido').css({
    height: B - separadorBarraInferior + 16
}),
a('#EvaluacionesDetalles').css({
    left: N / 2,
    top: B / 2
}),
a('#todo').css({
    width: N,
    height: B
}),
a('#cuerpo').css({
    height: B - separadorBarraInferior
}),
a('#ifr').css({
    width: N,
    height: B - separadorBarraInferior
}),
t(),
a(window).resize(function () {
    e()
}),
window.navegacionTam = function () {
    return navtamano
},
window.EvaluacionSilenciosa = function (a) {
    SilenciosaEval = a
},
window.EvaluacionInteraccionSilenciosa = function (a) {
    SilencioEvalInteracciones = a
},
window.QuitaDetalles = function () {
    c('EvaluacionesDetalles', !1)
},
window.Alertas = function (e, t, i) {
    switch (t) {
        case 'confirmar':
            c('alertas', !0),
            c('alertas_ventana', !0),
            a('#alertas_ventana_top').html('Mensaje<div id=\'alertas_ventana_top_close\'></div>'),
            a('#alertas_ventana_msg').html(e),
            a('#alertas_ventana_botones').html('<div id=\'alertas_ventana_botones_si\'>Si</div><div id=\'alertas_ventana_botones_no\'>No</div>'),
            a('#alertas_ventana_botones_no,#alertas_ventana_top_close').click(function () {
                a('#alertas_ventana_msg').html(''),
                c('alertas', !1),
                c('alertas_ventana', !1),
                i && i(!1)
            }),
            a('#alertas_ventana_botones_si').click(function () {
                a('#alertas_ventana_msg').html(''),
                c('alertas', !1),
                c('alertas_ventana', !1),
                i && i(!0)
            });
            break;
        case 'mensaje':
            break;
        case 'retro':
            break;
        case 'preguntas':
            c('alertas', !0),
            c('alertas_ventana', !0),
            a('#alertas_ventana_top').html('Retroalimentación<div id=\'alertas_ventana_top_close\'></div>'),
            a('#alertas_ventana_msg').html(e),
            a('#alertas_ventana_botones').html('<div id=\'alertas_ventana_botones_aceptar\'>Aceptar</div>'),
            a('#alertas_ventana_botones_aceptar,#alertas_ventana_top_close').click(function () {
                a('#alertas_ventana_msg').html(''),
                c('alertas', !1),
                c('alertas_ventana', !1),
                i && i()
            });
            break;
        case 'fineval':
            c('alertas', !0),
            c('alertas_ventana', !0),
            a('#alertas_ventana_top').html('Mensaje Final<div id=\'alertas_ventana_top_close\'></div>'),
            a('#alertas_ventana_msg').html(e),
            a('#alertas_ventana_botones').html('<div id=\'alertas_ventana_botones_aceptar\'>Aceptar</div>'),
            a('#alertas_ventana_botones_aceptar,#alertas_ventana_top_close').click(function () {
                a('#alertas_ventana_msg').html(''),
                c('alertas', !1),
                c('alertas_ventana', !1),
                i && i()
            });
            break;
        default:
            c('alertas', !0),
            c('alertas_ventana', !0),
            a('#alertas_ventana_top').html('Mensaje<div id=\'alertas_ventana_top_close\'></div>'),
            a('#alertas_ventana_msg').html(e),
            a('#alertas_ventana_botones').html('<div id=\'alertas_ventana_botones_aceptar\'>Aceptar</div>'),
            a('#alertas_ventana_botones_aceptar,#alertas_ventana_top_close').click(function () {
                a('#alertas_ventana_msg').html(''),
                c('alertas', !1),
                c('alertas_ventana', !1),
                i && i()
            })
    }
},
window.fnesPrimerSCO = function () {
    return esprimero
},
window.fnesUltimoSCO = function () {
    return esultimosco
},
window.pieTop = function () {
    return B - separadorBarraInferior
},
window.pieLeft = function () {
    return N
},
window.RegistraTrakUltimo = function () {
    TrakUltimo = TrakActual > TrakUltimo ? TrakActual : TrakUltimo
},
window.AhySiguiente = function () {
    var a = G('SePuedeSiguienteSCO', '');
    return a = 1 == a || 'true' == a ? !0 : !1
},
window.AhyAtras = function () {
    var a = G('SePuedeAnteriorSCO', '');
    return a = 1 == a || 'true' == a ? !0 : !1
},
window.SetXmlEval = function (a) {
    XMLEvaluaciones[TrakActual] = a
},
window.GetXmlEval = function () {
    return XMLEvaluaciones[TrakActual]
},
window.AsignaFuncion = function (a, e) {
    switch (a) {
        case 'atras':
            U = null == e && U != I ? I : e;
            break;
        case 'siguiente':
            F = null == e && F != x ? x : e
    }
},
window.GuardaAvanceEvaluacion = function () {
    d()
},
window.SendInteraccion = function (a) {
    SilencioEvalInteracciones || v(a)
};
new Array;
window.SendInteraccionArr = function (a) {
    if (!SilencioEvalInteracciones) for (var e = 0; e < a.length; e++) setTimeout(function () {
        v(a[e])
    }, 500 * e)
},
window.SendObjetivo = function (a) {
    p(a)
},
window.GetActual = function () {
    return TrakActual
},
window.GetUltimo = function () {
    return TrakUltimo
},
window.GetTotal = function () {
    return D.length
},
window.GetEvaluaciones = function () {
    return Evaluaciones[TrakActual]
},
window.SetEvaluaciones = function (a) {
    Evaluaciones[TrakActual] = a
},
window.PaginarRemoto = function (a) {
    g(a)
},
window.ProgresoEval = function (a) {
    u('img/Plantilla/progreso/progreso_' + a + '.png')
},
window.ValoresPropObj = function () {
    return L
},
window.ValoresItemsObj = function () {
    return D[TrakActual]
},
window.fnNext = function () {
    DesdeTema = !0,
    x()
},
window.fnBack = function () {
    DesdeTema = !0,
    I()
},
window.wir = function (a) {
    TrakUltimo + 2 >= a && (esperaiframeFF2 = a - 1, _(a - 1), 'ff' == brw.code && brw.version < 4 && setTimeout(i, 200))
},
window.PaginaRemoto = function (a) {
    g(a)
},
window.ProgresoRemoto = function (a) {
    var e = parseFloat(a),
    t = 0;
    t = parseInt(10 * e),
    u('img/Plantilla/progreso/progreso_' + t + '.png')
},
window.irA = function (a) {
    window.open(a, 'ifr', 'params')
},
window.showhide = function (a, e) {
    c(a, e)
},
window.showhideTablaContenidos = function (a) {
    metido = a,
    o()
},
window.AyudaRemoto = function (a) {
    tmpAyuda = a
},
window.RegistraCalificacion = function (a) {
    var e = G('LeerCalificacion', '');
    '' != e && 'false' != e && 'unknow' != e && parseInt(e) < a && !SilenciosaEval && G('GuardarCalificacion', a)
},
window.GetShowTabla = function () {
    return MostrarTabla
},
window.EsperaApi = function () {
    r()
},
window.GetScrollIframe = function () {
    return a('#ifr').contents().scrollTop()
},
window.SetScrollIframe = function () {
},
o(),
a('#btnresultados,#btntcontenidos,#close,#glosario,#documentos,#ayuda,#index,#atras,#siguiente,#tablaBarra').click(function () {
    switch (c('EvaluacionesDetalles', !1), this.id) {
        case 'close':
            T();
            break;
        case 'ayuda':
            w(L[14]);
            break;
        case 'glosario':
            C(L[17]);
            break;
        case 'documentos':
            O(L[16]);
            break;
        case 'index':
            E();
            break;
        case 'atras':
            null == U && (U = I),
            U();
            break;
        case 'siguiente':
            null == F && (F = x),
            F();
            break;
        case 'btntcontenidos':
            k();
            break;
        case 'tablaBarra':
            k();
            break;
        case 'btnresultados':
            b()
    }
})
});
