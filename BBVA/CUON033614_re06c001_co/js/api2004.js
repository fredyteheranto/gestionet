// Cambiar estos valores preestablecidos para adaptarse a las propias preferencias y requisitos.
// Forma de buscar un objeto de API (0 - se inicia de abajo arriba; 1 - se inicia de arriba abajo)
var g_intAPIOrder = 1;
var g_bShowApiErrors = false; 	// cambiar a true para mostrar mensajes de error
var g_bInitializeOnLoad = true; // cambiar a false para no inicializar LMS al cargar la p�gina HTML
// Traducir estas cadenas si g_bShowApiErrors es true y necesita localizar la aplicaci�n
var g_strAPINotFound = "Management system interface not found.";
var g_strAPITooDeep = "Cannot find API - too deeply nested.";
var g_strAPIInitFailed = "Found API but LMSInitialize failed.";
var g_strAPISetError = "Trying to set value but API not available.";
var g_strFSAPIError = 'LMS API adapter returned error code: "%1"\nWhen FScommand called API.%2\nwith "%3"';
var g_strDisableErrorMsgs = "Select cancel to disable future warnings.";
// Cambiar g_bSetCompletedAutomatically a true si desea que el estado se establezca autom�ticamente en finalizado al llamar a LMSFinish.
// Normalmente, este indicador permanece en false si la propia pel�cula Flash establece el estado en finalizado mediante el env�o de un FSCommand para establecer el estado en "finalizado", "pasado" o "fallo" (ambos implican "finalizado") "passed" or "failed" (both of which imply "completed")"
var g_bSetCompletedAutomatically = false;
// Este valor suele venir del LMS, pero si no aparece, �ste es el valor predeterminado que se utiliza para determinar si ha pasado o ha dado fallo.
// Establezca este valor en null si el ActionScript de Flash utiliza su propio m�todo para determinar si ha pasado o ha dado fallo; en caso contrario, establezca un valor de 0 a 1 incluidos (puede ser un valor de coma flotante, por ejemplo, "0,75").
var g_SCO_MasteryScore = null; // valores permitidos: 0,0..1,0 o nulo
//==================================================================
// ADVERTENCIA
// No modifique nada de lo que sigue a esta l�nea, a menos que sepa exactamente lo que est� haciendo.
// No cambie estos dos valores, ya que los valores preestablecidos de la plantilla Flash est�n basados en ellos.
var g_nSCO_ScoreMin = 0; 		// debe ser un n�mero
var g_nSCO_ScoreMax = 100; 		// debe ser un n�mero > nSCO_Score_Min
// Por cada especificaci�n SCORM, la puntuaci�n superior proporcionada por el LMS, si la hay, suplantar� al SCO en especificar si la puntuaci�n debe interpretarse cuando se determina el estado de superado/error.
// La plantilla intenta obtener la puntuaci�n superior y, si �sta est� disponible, establecer el estado de superado o error seg�n corresponda cuando el SCO env�a una puntuaci�n. 
// Es posible que el LMS no realice realmente la determinaci�n hasta que el SCO haya terminado.
// El valor predeterminado de este indicador es true. Establecer en false si no desea predecir la manera en que el LMS establecer� el estado de superado o error en funci�n de la puntuaci�n superior (el LMS ganar� al final de todos modos).
var g_bInterpretMasteryScore = false;
// Este script implementa diferentes aspectos de comportamiento l�gico com�n de un SCO.
/////////// INICIALIZACI�N DE LA INTERFAZ API Y FUNCIONES DE CAPTURA ////////
var g_nFindAPITries = 0;
var g_objAPI = null;
var g_bInitDone = false;
var g_bFinishDone = false;
var	g_bSCOBrowse = false;
var g_dtmInitialized = new Date(); // se ajustar� despu�s de inicializar
var g_bMasteryScoreInitialized = false;
var g_varInterval = "";			// intervalo global
var g_intIntervalSecs = 3; 		// N�mero de segundos de espera para cargar la API de SCORM
var g_intPollSecs = 1;			// N�mero de segundos de espera para sondear la API
var g_intCurrentTime = new Date().getTime();
var g_intAPI = 0;				// Tipo de API que se empezar� a buscar; valores permitidos: 0 - SCORM 2004; 1 - SCORM 1.2 (o 1.1)
var g_aryAPI = ["1.0", "0.2"]	// Matriz que almacena las versiones de la API
var g_strAPIVersion = -1;
function AlertUserOfAPIError(strText) {
	if (g_bShowApiErrors) {
		var s = strText + "\n\n" + g_strDisableErrorMsgs;
		if (!confirm(s)){
			g_bShowApiErrors = false
		}
	}
}
function ExpandString(s){
	var re = new RegExp("%","g")
	for (i = arguments.length-1; i > 0; i--){
		s2 = "%" + i;
		if (s.indexOf(s2) > -1){
			re.compile(s2,"g")
			s = s.replace(re, arguments[i]);
		}
	}
	return s
}
function findAPI(win)
{
	// Buscar la jerarqu�a de ventanas de un objeto denominado "API_1484_11" para SCORM 2004 o "API" para SCORM 1.2 o inferior
	// Buscar en la ventana actual (win) y buscar recursivamente en los fotogramas secundarios
	if(g_intAPI == 0)
	{
		if(win.API_1484_11 != null)
		{
			return win.API_1484_11;
		}
	} else if(g_intAPI == 1 || g_intAPI == "") {
		if (win.API != null)
		{
			g_strAPIVersion = g_aryAPI[g_intAPI];
			return win.API;
		}
	}
	if (win.length > 0)  // comprobar fotogramas
	{
		for (var i=0;i<win.length;i++)
		{
			var objAPI = findAPI(win.frames[i]);
			if (objAPI != null)
			{
				return objAPI;
			}
		}
	}
	return null;
}
function getAPI(intAPISearchOrder)
{
	// intAPISearchOrder es 0 - iniciar en la ventana actual y trabajar hacia arriba; 1 - iniciar en la ventana superior y trabajar hacia abajo.
	var objAPI = null;
	intAPISearchOrder=((typeof(intAPISearchOrder)=='undefined')?0:intAPISearchOrder);
	if(intAPISearchOrder==0)
	{
		// iniciar en la ventana actual y buscar recursivamente en ventanas o fotogramas principales
		objAPI = findAPI(window);
		if((objAPI==null) && (window.opener != null) && (typeof(window.opener) != "undefined"))
		{
			objAPI = findAPI(window.opener);
		} else if((objAPI==null) && (window.parent != null) && (typeof(window.parent) != "undefined")) {
			objAPI = findAPI(window.parent);
		}
		if((objAPI==null) && (g_intAPI < (g_aryAPI.length-1)))
		{
			g_intAPI++;
			objAPI = getAPI(intAPISearchOrder);
		}
	} else {
		// iniciar en la ventana superior y buscar recursivamente en fotogramas secundarios
		objAPI = findAPI(this.top);
		if (objAPI == null)
		{
			// la API no se encontr� en la jerarqu�a de la ventana actual. Si la ventana actual fue iniciada por otra ventana, compruebe la jerarqu�a de la ventana de apertura.
			objTopWindow=window.top;
			objTopWindow = objTopWindow.opener;
			while (objTopWindow && !objAPI)
			{
				//comprobando ventana de apertura
				objAPI = findAPI(objTopWindow.top);
				if (objAPI==null) objTopWindow = objTopWindow.opener;
			}
			if(objAPI==null && g_intAPI < (g_aryAPI.length-1))
			{
				g_intAPI++;
				objAPI = getAPI(intAPISearchOrder);
			}
		}
	}
	if(objAPI==null)
	{
		// no se encuentra la API
	} else if(objAPI != null && g_strAPIVersion == -1) {
		g_strAPIVersion = objAPI.version;
	}
	return objAPI;
}
function waitForAPI(){
	if(new Date().getTime() > (g_intCurrentTime + g_intIntervalSecs*1000) || APIOK()){
		// se agot� el tiempo de espera o se encontr� la API
		clearInterval(g_varInterval);
		if(!APIOK()){
			g_objAPI = null;
		} else {
			if (g_bInitializeOnLoad) {
				SCOInitialize()
			}
		}
	} else {
		g_objAPI = getAPI(g_intAPIOrder);
	}
}
function APIOK() {
	return ((typeof(g_objAPI)!= "undefined") && (g_objAPI != null))
}
function SCOInitialize() {
	var err = true;
	if (!g_bInitDone) {
		if (!APIOK()) {
			AlertUserOfAPIError(g_strAPINotFound);
			err = false
		} else {
			err = g_objAPI.Initialize("");
			SCOSetValue("cmi.exit", "suspend");
			//SCOSetValue("cmi.entry", "resume");
			if (err == "true") {
				g_bSCOBrowse = (g_objAPI.GetValue("cmi.mode") == "browse");
				if (!g_bSCOBrowse) {
					if (g_objAPI.GetValue("cmi.completion_status") == "not attempted") {
						err = g_objAPI.SetValue("cmi.completion_status","incomplete")
					}
				}
			} else {
				AlertUserOfAPIError(g_strAPIInitFailed)
			}
		}
		if (typeof(SCOInitData) != "undefined") {
			// La funci�n SCOInitData puede definirse en otro script del SCO
			SCOInitData()
		}
		g_dtmInitialized = new Date();
	}
	g_bInitDone = true;
	return (err + "") // Forzar tipo para cadena
}
function SCOFinish() {
	if ((APIOK()) && (g_bFinishDone == false)) {
		SCOReportSessionTime()
		if (g_bSetCompletedAutomatically){
			SCOSetStatusCompleted();
		}//else if(SCOGetValue("cmi.completion_status")!="completed"){			
			//SCOSetValue("cmi.exit", "suspend");
			//SCOSetValue("cmi.entry", "resume");
		//}
		//SCOSetValue("cmi.entry", "resume");				
		//SCOGetValue('adl.nav.request', 'exitAll')
		if (typeof(SCOSaveData) != "undefined"){
			// La funci�n SCOSaveData puede definirse en otro script del SCO
			SCOSaveData();
		}
		//SCOCommit();
		
	}
	//alert("SCOFINISH");
	//if(!(API_SUMTOTAL === null || API_SUMTOTAL === undefined)){		
	//	API_SUMTOTAL.SetNavCommand("exit");
	//	SCOSetValue('adl.nav.request', 'suspendAll') 
	//}
	//SCOSetValue("cmi.exit", "suspend");
	if(APIOK() && g_bFinishDone==false){
		//SCOSetValue("cmi.exit", "suspend");
		//SCOCommit();	
		g_bFinishDone = (g_objAPI.Terminate("") == "true");
	}
	return (g_bFinishDone + "" ) // Forzar tipo para cadena
}
// Llame a estas funciones de captura en lugar de intentar llamar directamente al adaptador de API
function SCOGetValue(nam)			{return ((APIOK())?g_objAPI.GetValue(nam.toString()):"")}
function SCOCommit()					{return ((APIOK())?g_objAPI.Commit(""):"false")}
function SCOGetLastError()		{return ((APIOK())?g_objAPI.GetLastError():"-1")}
function SCOGetErrorString(n)	{return ((APIOK())?g_objAPI.GetErrorString(n):"No API")}
function SCOGetDiagnostic(p)	{return ((APIOK())?g_objAPI.GetDiagnostic(p):"No API")}
//LMSSetValue se implementa con m�s datos complejos l�gica de gesti�n a continuaci�n
var g_bMinScoreAcquired = false;
var g_bMaxScoreAcquired = false;
// Aqu� comienza la l�gica especial
function SCOSetValue(nam,val){
	var err = "";
	if (!APIOK()){
			AlertUserOfAPIError(g_strAPISetError + "\n" + nam + "\n" + val);
			err = "false"
	} else if (nam == "cmi.score.raw") err = privReportRawScore(val)
	if (err == ""){
			err = g_objAPI.SetValue(nam,val.toString() + "");
			if (err != "true") return err
	}
	if (nam == "cmi.score.min"){
		g_bMinScoreAcquired = true;
		g_nSCO_ScoreMin = val
	}
	else if (nam == "cmi.score.max"){
		g_bMaxScoreAcquired = true;
		g_nSCO_ScoreMax = val
	}
	return err
}
function privReportRawScore(nRaw) { // llamado s�lo por SCOSetValue
	var g_bMinScoreAcquired = false;
	var g_bMaxScoreAcquired = false;

	if (isNaN(nRaw)) return "false";
	if (g_bMinScoreAcquired==false){
		if (g_objAPI.SetValue("cmi.score.min",g_nSCO_ScoreMin+"")!= "true") return "false"
	}
	if (!g_bMaxScoreAcquired){
		if (g_objAPI.SetValue("cmi.score.max",g_nSCO_ScoreMax+"")!= "true") return "false"
	}
	if (g_objAPI.SetValue("cmi.score.raw", nRaw)!= "true") return "false";
	g_bMinScoreAcquired = false;
	g_bMaxScoreAcquired = false;
	if (!g_bMasteryScoreInitialized){
		var nTemp = SCOGetValue("cmi.scaled_passing_score");
		nTemp = (nTemp <= 0?0:nTemp * 100);
		var nMasteryScore = parseInt(nTemp,10);
		if (!isNaN(nMasteryScore)) g_SCO_MasteryScore = nMasteryScore
	}
  	if ((g_bInterpretMasteryScore)&&(!isNaN(g_SCO_MasteryScore))){
    	var stat = (nRaw >= g_SCO_MasteryScore? "passed" : "failed");
    	if (SCOSetValue("cmi.success_status",stat) != "true") return "false";
  	}
  	return "true"
}
function MillisecondsToCMIDuration(n) {
//Convertir duraci�n de milisegundos a formato 0000:00:00.00
	var hms = "";
	var dtm = new Date();	dtm.setTime(n);
	var h = "000" + Math.floor(n / 3600000);
	var m = "0" + dtm.getMinutes();
	var s = "0" + dtm.getSeconds();
	var cs = "0" + Math.round(dtm.getMilliseconds() / 10);
	hms = "PT" + h.substr(h.length-4)+"H"+m.substr(m.length-2)+"M";
	hms += s.substr(s.length-2)+"S";
	return hms
}
// Este script llama autom�ticamente a SCOReportSessionTime, aunque tambi�n puede llamarlo en cualquier momento desde el SCO
function SCOReportSessionTime() {
	var dtm = new Date();
	var n = dtm.getTime() - g_dtmInitialized.getTime();
	return SCOSetValue("cmi.session_time",MillisecondsToCMIDuration(n))
}
// Puesto que s�lo el dise�ador de un SCO sabe el significado de finalizado, otro script del SCO puede llamar a esta funci�n para establecer el estado de finalizado.
// La funci�n comprueba que el SCO no est� en modo de examen e impide pasar a un estado "pasado" o "fallo", puesto que ambos implican "finalizado".
function SCOSetStatusCompleted(){
	var stat = SCOGetValue("cmi.completion_status");
	if (SCOGetValue("cmi.lesson_mode") != "browse"){
		if ((stat!="completed") && (stat != "passed") && (stat != "failed")){
			return SCOSetValue("cmi.completion_status","completed")
		}
	} else return "false"
}
// L�gica de gesti�n de objetivos
function SCOSetObjectiveData(id, elem, v) {
	var result = "false";
	var i = SCOGetObjectiveIndex(id);
	if (isNaN(i)) {
		i = parseInt(SCOGetValue("cmi.objectives._count"));
		if (isNaN(i)) i = 0;
		if (SCOSetValue("cmi.objectives." + i + ".id", id) == "true"){
			result = SCOSetValue("cmi.objectives." + i + "." + elem, v)
		}
	} else {
		result = SCOSetValue("cmi.objectives." + i + "." + elem, v);
		if (result != "true") {
			// Quiz�s este LMS acepta s�lo entradas de asientos contables
			i = parseInt(SCOGetValue("cmi.objectives._count"));
			if (!isNaN(i)) {
				if (SCOSetValue("cmi.objectives." + i + ".id", id) == "true"){
					result = SCOSetValue("cmi.objectives." + i + "." + elem, v)
				}
			}
		}
	}
	return result
}
function SCOGetObjectiveData(id, elem) {
	var i = SCOGetObjectiveIndex(id);
	if (!isNaN(i)) {
		return SCOGetValue("cmi.objectives." + i + "."+elem)
	}
	return ""
}
function SCOGetObjectiveIndex(id){
	var i = -1;
	var nCount = parseInt(SCOGetValue("cmi.objectives._count"));
	if (!isNaN(nCount)) {
		for (i = nCount-1; i >= 0; i--){ //volver atr�s en caso de que LMS realice entradas contables
			if (SCOGetValue("cmi.objectives." + i + ".id") == id) {
				return i
			}
		}
	}
	return NaN
}
// Funciones para convertir tokens compatibles con AICC o abreviaturas en tokens de SCORM
function AICCTokenToSCORMToken(strList,strTest){
	var a = strList.split(",");
	var c = strTest.substr(0,1).toLowerCase();
	for (i=0;i<a.length;i++){
			if (c == a[i].substr(0,1)) return a[i]
	}
	return strTest
}
function normalizeStatus(status){
	return AICCTokenToSCORMToken("completed,incomplete,not attempted,failed,passed", status)
}
function normalizeInteractionType(theType){
	return AICCTokenToSCORMToken("true-false,choice,fill-in,matching,performance,sequencing,likert,numeric", theType)
}
function normalizeInteractionResult(result){
	var strInteractionResult = AICCTokenToSCORMToken("correct,wrong,unanticipated,neutral", result)
	strInteractionResult = (strInteractionResult=="wrong"?"incorrect":strInteractionResult);
	return strInteractionResult;
}
function checkInteractionResponse(response_str)
{
	var result_str = "";
	for(var char_int=0;char_int<response_str.length;char_int++)
	{
		if(response_str.substr(char_int,1) == "." || response_str.substr(char_int,1) == ",")
		{
			if(response_str.substr(char_int - 1,1) != "[" && response_str.substr(char_int + 1,1) != "]")
			{
				result_str += "[" + response_str.substr(char_int,1) + "]";
			} else {
				result_str += response_str.substr(char_int,1);
			}
		} else {
			result_str += response_str.substr(char_int,1);
		}
	}
	result_str = (result_str==""?"0":result_str);
	return result_str;
}
function formatTimestamp(time_var)
{
	return formatDate() + "T" + formatTime(time_var, undefined, undefined, 2);
}
// ******************************************************************
// *
// *     M�todo:           formatTime
// *     Descripci�n:      da formato PTxHyMzS a los segundos (transferidos como par�metro)
// *     Valor devuelto:          Cadena (formato de hora HHHH:MM:SS)
// *
// ******************************************************************
function formatTime(time_var, minutes_str, seconds_str, typeFormat_int)
{
	var days_str, hours_str, formattedTime_str;
	days_str = "0";
	if(time_var == undefined)
	{
		// crear hora bas�ndose en la hora actual de hoy
		var time_obj = new Date();
		hours_str = time_obj.getHours();
		minutes_str = time_obj.getMinutes();
		seconds_str = time_obj.getSeconds();
	} else if(typeof(time_var) == "string" && time_var.indexOf(":") > -1) {
		var time_obj = time_var.split(":");
		hours_str = time_obj[0];
		minutes_str = time_obj[1];
		seconds_str = time_obj[2];
	} else {
		days_str    = "0";
		seconds_str = "0";
		minutes_str = "0";
		hours_str     = "0";
		seconds_str = Math.round(time_var);
		if(seconds_str > 59)
		{
			minutes_str = Math.round(seconds_str / 60);
			seconds_str = seconds_str - (minutes_str * 60);
		}
		if(minutes_str > 59)
		{
			hours_str = Math.round(minutes_str / 60);
			minutes_str = minutes_str - (hours_str * 60);
		}
		if(hours_str > 23)
		{
			days_str = Math.round(hours_str / 24);
			hours_str = hours_str - (days_str * 24);
		}
	}
	if(typeFormat_int == undefined || typeFormat_int == 1)
	{
		formattedTime_str = "P";
		if(days_str != "0")
		{
			formattedTime_str += days_str + "D";
		}
		formattedTime_str += "T" + hours_str + "H" + minutes_str + "M" + seconds_str + "S";
	} else {
		formattedTime_str = formatNum(hours_str, 2) + ":" + formatNum(minutes_str, 2) + ":" + formatNum(seconds_str, 2);
	}
	return formattedTime_str;
}
// ******************************************************************
// *
// *     M�todo:           formatDate
// *     Descripci�n:      Formatea los segundos o "MM/DD/AAAA"
// *     Valor devuelto:          Cadena (formato de fecha "AAAA-MM-DD")
// *
// ******************************************************************
function formatDate(date_var, day_str, year_str)
{
	if (date_var == undefined) {
		// Crear fecha bas�ndose en la fecha de hoy
		var date_obj = new Date();
		date_var = formatNum((date_obj.getMonth()+1), 2);
		day_str  = formatNum((date_obj.getDate()), 2);
		year_str = (date_obj.getFullYear());
	} else if(typeof(date_var) == "string" && date_var.indexOf("/") > -1) {
		// Convertir de MM/DD/AAAA
		var date_obj = date_var.split("/");
		date_var = formatNum(date_obj[0], 2);
		day_str  = formatNum(date_obj[1], 2);
		year_str = formatNum(date_obj[2], 4);
	}
	var formattedDate_str = (year_str + "-" + date_var + "-" + day_str);
	return formattedDate_str;
}
// ******************************************************************
// *
// *    M�todo:         formatNum
// *    Descripci�n:     Convierte el n�mero pasado a esta funci�n en un valor completado con ceros, normalmente un n�mero de 2 d�gitos o de 4 d�gitos (p. ej., convierte 2 en 02, o 2 en 0002)
// *    Valor devuelto:        Cadena (completada con el n�mero de ceros pasado
// *
// ******************************************************************
function formatNum (initialValue_var, numToPad_int)
{
	var paddedValue_str = "";                         // Cadena; contiene el valor completado con ceros
	var i = 0;                                     // Entero; variable que se utiliza en la reproducci�n indefinida
	var initialValue_str = initialValue_var.toString();    // Cadena; convierte el par�metro "initializeValue_var" expl�citamente en una cadena
	if (initialValue_str.length > numToPad_int)
	{
		// error - la longitud del valor inicial ya supera el n�mero que se completar� con ceros.  Devolver� initialValue_var sin completar el n�mero con ceros
	} else {
		for (var i = 1; i <= (numToPad_int - initialValue_str.length); i++)
		{
			paddedValue_str = paddedValue_str + "0";
		}
	}
	paddedValue_str = paddedValue_str + initialValue_var;
	return paddedValue_str;
}
// Detectar Internet Explorer
var g_bIsInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
// Gestionar mensajes de FSCommand desde una pel�cula Flash, reasignando todos los comandos de plantilla Flash de AICC a SCORM si es necesario
function nswf_DoFSCommand(command, args){
	var nswfObj = g_bIsInternetExplorer ? nswf : document.nswf;
	// no funciona si no hay disponible una API de SCORM
	//alert(command+" - "+args);
	var myArgs = new String(args);
	var cmd = new String(command);
	var v = "";
	var err = "true";
	var arg1, arg2, n, s, i;
	var sep = myArgs.indexOf(",");
	if (sep > -1){
		arg1 = myArgs.substr(0, sep); // Nombre de elemento de datos para obtener desde API
		arg2 = myArgs.substr(sep+1) 	// Nombre de variable de pel�cula Flash para establecer
	} else {
		arg1 = myArgs
	}
	if (!APIOK()){
		alert("No se encontro el Api");
		return;
	}
	if (cmd.substring(0,3) == "LMS"){
		// Gestionar FSCommands "LMSxxx" (compatible con plantilla html fsSCORM)
		if ( cmd == "LMSInitialize" ){
			err = (APIOK() + "")
			// El LMSInitialize real lo llama autom�ticamente la plantilla
		}	else if ( cmd == "LMSSetValue" ){
			//alert('LMSSetValue: \r\rArg1: ' + arg1 + '\rArg2: ' + arg2);
			err = SCOSetValue(arg1,arg2)
		} else if ( cmd == "LMSFinish" ){
			err = SCOFinish()
			// Gestionado autom�ticamente por la plantilla, aunque la pel�cula puede llamarlo antes
		}	else if ( cmd == "LMSCommit" ){
			err = SCOCommit()
		}	else if ( cmd == "LMSFlush" ){
			// no-op
			// LMSFlush no est� definido en SCORM y si se llama causa errores de prueba
		}	else if ((arg2) && (arg2.length > 0)){
			if ( cmd == "LMSGetValue") {
				//alert('LMSSetValue: \r\rArg1: ' + arg1 + '\rArg2: ' + arg2);
				nswfObj.SetVariable(arg2, SCOGetValue(arg1));
			}	else if ( cmd == "LMSGetLastError") {
				nswfObj.SetVariable(arg2, SCOGetLastError(arg1));
			}	else if ( cmd == "LMSGetErrorString") {
				nswfObj.SetVariable(arg2, SCOGetLastError(arg1));
			}	else if ( cmd == "LMSGetDiagnostic") {
				nswfObj.SetVariable(arg2, SCOGetDiagnostic(arg1));
			}	else {
				// para una extensi�n LMSGetxxxx desconocida
				v = eval('g_objAPI.' + cmd + '(\"' + arg1 + '\")');
				nswfObj.SetVariable(arg2,v);
			}
		} else if (cmd.substring(0,3) == "LMSGet") {
			err = "-2: No Flash variable specified"
		}
		// fin de control de comandos "LMSxxx"
	} else if ((cmd.substring(0,6) == "MM_cmi")||(cmd.substring(0,6) == "CMISet")) {
		// Gestionar FSCommands de componentes de aprendizaje de Macromedia.
		// Utilizan convenciones del modelo de datos AICC HACP, por lo que hay que reasignar datos de AICC a SCORM seg�n sea necesario.
		var F_intData = myArgs.split(";");
		if (cmd == "MM_cmiSendInteractionInfo") {
			n = SCOGetValue("cmi.interactions._count");
			s = "cmi.interactions." + n + ".";
			// Captura errores graves para evitar fallos de prueba de conformidad SCORM
			// Si no se proporciona un ID para esta interacci�n, no podemos registrarla
			v = F_intData[2]
			if ((v == null) || (v == "")) err = 201; // Si no hay ID, no tiene sentido registrar
			if (err =="true"){
				err = SCOSetValue(s + "id", v)
			}
			if (err =="true"){
				var re = new RegExp("[{}]","g")
				for (i=1; (i<9) && (err=="true"); i++){
					v = F_intData[i];
					if ((v == null) || (v == "")) continue
					if (i == 1){
						err = SCOSetValue(s + "timestamp", formatTimestamp(v))
					} else if (i == 3){
						err = SCOSetValue(s + "objectives.0.id", v)
					} else if (i == 4){
						err = SCOSetValue(s + "type", normalizeInteractionType(v))
					} else if (i == 5){
						// strip out "{" and "}" from response
						v = v.replace(re, "");
						err = SCOSetValue(s + "correct_responses.0.pattern", checkInteractionResponse(v))
					} else if (i == 6){
						// strip out "{" and "}" from response
						v = v.replace(re, "");
						err = SCOSetValue(s + "learner_response", checkInteractionResponse(v))
					} else if (i == 7){
						err = SCOSetValue(s + "result", normalizeInteractionResult(v))
					} else if (i == 8){
						err = SCOSetValue(s + "weighting", v)
					} else if (i == 9){
						err = SCOSetValue(s + "latency", v)
					}
				}
			}
		} else if (cmd == "MM_cmiSendObjectiveInfo"){
			err = SCOSetObjectiveData(F_intData[1], ".score.raw", F_intData[2])
			if (err=="true"){
				SCOSetObjectiveData(F_intData[1], ".status", normalizeStatus(F_intData[3]))
			}
		} else if ((cmd=="CMISetScore") ||(cmd=="MM_cmiSendScore")){
			err = SCOSetValue("cmi.score.raw", F_intData[0]);
		} else if ((cmd=="CMISetStatus") || (cmd=="MM_cmiSetLessonStatus")){
			var strTempStatus = normalizeStatus(F_intData[0]);
			if (strTempStatus == "passed" || strTempStatus == "failed")
			{
				err = SCOSetValue("cmi.success_status", normalizeStatus(F_intData[0]))
			} else {
				err = SCOSetValue("cmi.completion_status", normalizeStatus(F_intData[0]))
			}
		} else if (cmd=="CMISetTime"){
			err = SCOSetValue("cmi.session_time", formatTime(F_intData[0]))
		} else if (cmd=="CMISetCompleted"){
			err = SCOSetStatusCompleted()
		} else if (cmd=="CMISetStarted"){
			err = SCOSetValue("cmi.completion_status", "incomplete")
		} else if (cmd=="CMISetPassed"){
			SCOSetValue("cmi.completion_status", "completed");
			err = SCOSetValue("cmi.success_status", "passed")
		} else if (cmd=="CMISetFailed"){
			SCOSetValue("cmi.completion_status", "completed");
			err = SCOSetValue("cmi.success_status", "failed")
		} else if (cmd=="CMISetData"){
			err = SCOSetValue("cmi.suspend_data", F_intData[0])
		} else if (cmd=="CMISetLocation"){
			err = SCOSetValue("cmi.location", F_intData[0])
		} else if (cmd=="CMISetTimedOut"){
			err = SCOSetValue("cmi.exit", "time-out")
		} // Otros FSCommands del componente de aprendizaje no funcionan en este contexto
	} else {
		if (cmd=="CMIFinish" || cmd=="CMIExitAU"){
			err = SCOFinish()
		} else if (cmd=="CMIInitialize" || cmd=="MM_StartSession"){
			err = SCOInitialize()
		} else {
			// Comando desconocido; puede estar invocando una extensi�n de API
			// Si los comandos vienen con un segundo argumento, asumir que se espera un valor en caso contrario, asumir que se trata s�lo de un comando
			if (eval('g_objAPI.' + cmd)) {
				v = eval('g_objAPI.' + cmd + '(\"' + arg1 + '\")');
				if ((arg2) && (arg2.length > 0)){
					nswfObj.SetVariable(arg2,v)
				} else {
					err = v
				}
			} else {
				err = "false"
			}
		}
	}
	// Fin de conversi�n y procesamiento de comandos
	// gestionar errores detectados, como devoluciones de errores de LMS
	if ((g_bShowApiErrors) && (err != "true")) {
		AlertUserOfAPIError(ExpandString(g_strFSAPIError, err, cmd, args))
	}
	return err
}


var findExAPITries;
var API_SUMTOTAL=null;
API_SUMTOTAL=getExtendedAPI();
function scanAPI(win){
   while ((win.API_Extended == null) && (win.parent != null) && (win.parent != win))   {
      findExAPITries ++;
      // Note: 7 is an arbitrary number, but should be more than sufficient
      if (findExAPITries  > 50){
         return null;
      }      
      win = win.parent;
   }
   return win.API_Extended;
}

function getExtendedAPI(){
	findExAPITries = 0;
	var theAPI = scanAPI(window);
	if ((theAPI == null) && (window.opener != null) && (typeof(window.opener) != "undefined")){
		theAPI = scanAPI(window.opener);
	}
	return theAPI
}