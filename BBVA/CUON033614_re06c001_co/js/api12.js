// Cambiar estos valores preestablecidos para adaptarse a las propias preferencias y requisitos.
var g_bShowApiErrors = false; 	// cambiar a true para mostrar mensajes de error
var g_bInitializeOnLoad = false; // cambiar a false para no inicializar LMS al cargar la p�gina HTML
// Traducir estas cadenas si g_bShowApiErrors es true y necesita localizar la aplicaci�n
var g_strAPINotFound = "Management system interface not found.";
var g_strAPITooDeep = "Cannot find API - too deeply nested.";
var g_strAPIInitFailed = "Found API but LMSInitialize failed.";
var g_strAPISetError = "Trying to set value but API not available.";
var g_strFSAPIError = 'LMS API adapter returned error code: "%1"\nWhen FScommand called API.%2\nwith "%3"';
var g_strDisableErrorMsgs = "Select cancel to disable future warnings.";
// Cambiar g_bSetCompletedAutomatically a true si desea que el estado se establezca autom�ticamente en finalizado al llamar a LMSFinish.
// Normalmente, este indicador permanece en false si la propia pel�cula Flash establece el estado en finalizado mediante el env�o de un FSCommand para establecer el estado en "finalizado", "pasado" o "fallo" (ambos implican "finalizado") "passed" or "failed" (both of which imply "completed")
var g_bSetCompletedAutomatically = false;
// Este valor suele venir del LMS, pero si no aparece, �ste es el valor predeterminado que se utiliza para determinar si ha pasado o ha dado fallo.
// Establezca este valor en null si el ActionScript de Flash utiliza su propio m�todo para determinar si ha pasado o ha dado fallo; en caso contrario, establezca un valor de 0 a 1 incluidos (puede ser un valor de coma flotante, por ejemplo, "0,75").
var g_SCO_MasteryScore = null; // valores permitidos: 0,0..1,0 o nulo
//==================================================================
// ADVERTENCIA
// No modifique nada de lo que sigue a esta l�nea, a menos que sepa exactamente lo que est� haciendo.
// No cambie estos dos valores, ya que los valores preestablecidos de la plantilla Flash est�n basados en ellos.
var g_nSCO_ScoreMin = 0; 		// debe ser un n�mero
var g_nSCO_ScoreMax = 100; 		// debe ser un n�mero > nSCO_Score_Min
// Por cada especificaci�n SCORM, la puntuaci�n superior proporcionada por el LMS, si la hay, suplantar� al SCO en especificar si la puntuaci�n debe interpretarse cuando se determina el estado de superado/error.

// La plantilla intenta obtener la puntuaci�n superior y, si �sta est� disponible, establecer el estado de superado o error seg�n corresponda cuando el SCO env�a una puntuaci�n. 
// Es posible que el LMS no realice realmente la determinaci�n hasta que el SCO haya terminado.
// El valor predeterminado de este indicador es true. Establecer en false si no desea predecir la manera en que el LMS establecer� el estado de superado o error en funci�n de la puntuaci�n superior (el LMS ganar� al final de todos modos).
var g_bInterpretMasteryScore = false;

// Este script implementa diferentes aspectos de comportamiento l�gico com�n de un SCO.
/////////// INICIALIZACI�N DE LA INTERFAZ API Y FUNCIONES DE CAPTURA ////////
var g_nFindAPITries = 0;
var g_objAPI = null;
var g_bInitDone = false;
var g_bFinishDone = false;
var	g_bSCOBrowse = false;
var g_dtmInitialized = new Date(); // se ajustar� despu�s de inicializar
var g_bMasteryScoreInitialized = false;

var g_varInterval = "";			// intervalo global
var g_intIntervalSecs = 3; 		// N�mero de segundos de espera para cargar la API de SCORM
var g_intPollSecs = 0.25;			// N�mero de segundos de espera para sondear la API
var g_intCurrentTime = new Date().getTime();

function AlertUserOfAPIError(strText) {
	if (g_bShowApiErrors) {
		var s = strText + "\n\n" + g_strDisableErrorMsgs;
		if (!confirm(s)){
			g_bShowApiErrors = false
		}
	}
}

function ExpandString(s){
	var re = new RegExp("%","g")
	for (i = arguments.length-1; i > 0; i--){
		s2 = "%" + i;
		if (s.indexOf(s2) > -1){
			re.compile(s2,"g")
			s = s.replace(re, arguments[i]);
		}
	}
	return s
}

function FindAPI(win) {
	while ((win.API == null) && (win.parent != null) && (win.parent != win)) {
		g_nFindAPITries ++;
		if (g_nFindAPITries > 500) {
			AlertUserOfAPIError(g_strAPITooDeep);
			return null;
		}
		win = win.parent;
	}
	return win.API;
}
function waitForAPI(){
	if(new Date().getTime() > (g_intCurrentTime + g_intIntervalSecs*1000) || APIOK()){
		// se agot� el tiempo de espera o se encontr� la API
		clearInterval(g_varInterval);
		if(!APIOK()){
			g_objAPI = null;
		} else {
			if (g_bInitializeOnLoad) {
				SCOInitialize()
			}
		}
	}
}
function APIOK() {
	return ((typeof(g_objAPI)!= "undefined") && (g_objAPI != null))
}
function waitForAPI(){
	//SCOInitialize();
}
function SCOInitialize() {
	var err = true;
	if (!g_bInitDone) {
		if ((window.parent) && (window.parent != window)){
			g_objAPI = FindAPI(window.parent)
		}
		if ((g_objAPI == null) && (window.opener != null))	{
			g_objAPI = FindAPI(window.opener)
		}
		if (!APIOK()) {
			AlertUserOfAPIError(g_strAPINotFound);
			err = false
		} else {
			err = g_objAPI.LMSInitialize("");
			if (err == "true") {
				g_bSCOBrowse = (g_objAPI.LMSGetValue("cmi.core.lesson_mode") == "browse");						
				if (!g_bSCOBrowse) {
					if (g_objAPI.LMSGetValue("cmi.core.lesson_status") == "not attempted") {
						err = g_objAPI.LMSSetValue("cmi.core.lesson_status","incomplete")
					}
				}
			} else {
				AlertUserOfAPIError(g_strAPIInitFailed)
			}
		}
		if (typeof(SCOInitData) != "undefined") {
			// La funci�n SCOInitData puede definirse en otro script del SCO
			parent.SCOInitData()
		}
		g_dtmInitialized = new Date();
	}
	g_bInitDone = true;
	return (err + "") // Forzar tipo para cadena
}

function SCOFinish() {
	if ((APIOK()) && (g_bFinishDone == false)) {
		SCOReportSessionTime()
		if (g_bSetCompletedAutomatically){
			SCOSetStatusCompleted();
		}
		if (typeof(SCOSaveData) != "undefined"){
			// La funci�n SCOSaveData puede definirse en otro script del SCO
			SCOSaveData();
		}
		g_bFinishDone = (g_objAPI.LMSFinish("") == "true");
	}
	return (g_bFinishDone + "" ) // Forzar tipo para cadena
}

// Llame a estas funciones de captura en lugar de intentar llamar directamente al adaptador de API
function SCOGetValue(nam)			{return ((APIOK())?g_objAPI.LMSGetValue(nam.toString()):"")}
function SCOCommit()					{return ((APIOK())?g_objAPI.LMSCommit(""):"false")}
function SCOGetLastError()		{return ((APIOK())?g_objAPI.LMSGetLastError():"-1")}
function SCOGetErrorString(n)	{return ((APIOK())?g_objAPI.LMSGetErrorString(n):"No API")}
function SCOGetDiagnostic(p)	{return ((APIOK())?g_objAPI.LMSGetDiagnostic(p):"No API")}
//LMSSetValue se implementa con m�s datos complejos l�gica de gesti�n a continuaci�n

var g_bMinScoreAcquired = false;
var g_bMaxScoreAcquired = false;
// Aqu� comienza la l�gica especial

function SCOSetValue(nam,val){
	var err = "";
	if (!APIOK()){
			AlertUserOfAPIError(g_strAPISetError + "\n" + nam + "\n" + val);
			err = "false"
	} else if (nam == "cmi.core.score.raw") err = privReportRawScore(val)
	if (err == ""){
			err = g_objAPI.LMSSetValue(nam,val.toString() + "");
			if (err != "true") return err
	}
	if (nam == "cmi.core.score.min"){
		g_bMinScoreAcquired = true;
		g_nSCO_ScoreMin = val
	}
	else if (nam == "cmi.core.score.max"){
		g_bMaxScoreAcquired = true;
		g_nSCO_ScoreMax = val
	}
	return err
}

function privReportRawScore(nRaw) { // llamado s�lo por SCOSetValue
	if (isNaN(nRaw)) return "false";
	if (!g_bMinScoreAcquired){
		if (g_objAPI.LMSSetValue("cmi.core.score.min",g_nSCO_ScoreMin+"")!= "true") return "false"
	}
	if (!g_bMaxScoreAcquired){
		if (g_objAPI.LMSSetValue("cmi.core.score.max",g_nSCO_ScoreMax+"")!= "true") return "false"
	}
	if (g_objAPI.LMSSetValue("cmi.core.score.raw", nRaw)!= "true") return "false";
	g_bMinScoreAcquired = false;
	g_bMaxScoreAcquired = false;
	if (!g_bMasteryScoreInitialized){
		var nMasteryScore = parseInt(SCOGetValue("cmi.student_data.mastery_score"),10);
		if (!isNaN(nMasteryScore)) g_SCO_MasteryScore = nMasteryScore
	}
  	if ((g_bInterpretMasteryScore)&&(!isNaN(g_SCO_MasteryScore))){
    	var stat = (nRaw >= g_SCO_MasteryScore? "passed" : "failed");
    	if (SCOSetValue("cmi.core.lesson_status",stat) != "true") return "false";
  	}
  	return "true"
}

function MillisecondsToCMIDuration(n) {
//Convertir duraci�n de milisegundos a formato 0000:00:00.00
	var hms = "";
	var dtm = new Date();	dtm.setTime(n);
	var h = "000" + Math.floor(n / 3600000);
	var m = "0" + dtm.getMinutes();
	var s = "0" + dtm.getSeconds();
	var cs = "0" + Math.round(dtm.getMilliseconds() / 10);
	hms = h.substr(h.length-4)+":"+m.substr(m.length-2)+":";
	hms += s.substr(s.length-2)+"."+cs.substr(cs.length-2);
	return hms
}

// Este script llama autom�ticamente a SCOReportSessionTime, aunque tambi�n puede llamarlo en cualquier momento desde el SCO
function SCOReportSessionTime() {
	var dtm = new Date();
	var n = dtm.getTime() - g_dtmInitialized.getTime();
	return SCOSetValue("cmi.core.session_time",MillisecondsToCMIDuration(n))
}

// Puesto que s�lo el dise�ador de un SCO sabe el significado de finalizado, otro script del SCO puede llamar a esta funci�n para establecer el estado de finalizado.
// La funci�n comprueba que el SCO no est� en modo de examen e impide pasar a un estado "pasado" o "fallo", puesto que ambos implican "finalizado".
function SCOSetStatusCompleted(){
	var stat = SCOGetValue("cmi.core.lesson_status");
	if (SCOGetValue("cmi.core.lesson_mode") != "browse"){
		if ((stat!="completed") && (stat != "passed") && (stat != "failed")){
			return SCOSetValue("cmi.core.lesson_status","completed")
		}
	} else return "false"
}

// L�gica de gesti�n de objetivos
function SCOSetObjectiveData(id, elem, v) {
	var result = "false";
	var i = SCOGetObjectiveIndex(id);
	if (isNaN(i)) {
		i = parseInt(SCOGetValue("cmi.objectives._count"));
		if (isNaN(i)) i = 0;
		if (SCOSetValue("cmi.objectives." + i + ".id", id) == "true"){
			result = SCOSetValue("cmi.objectives." + i + "." + elem, v)
		}
	} else {
		result = SCOSetValue("cmi.objectives." + i + "." + elem, v);
		if (result != "true") {
			// Quiz�s este LMS acepta s�lo entradas de asientos contables
			i = parseInt(SCOGetValue("cmi.objectives._count"));
			if (!isNaN(i)) {
				if (SCOSetValue("cmi.objectives." + i + ".id", id) == "true"){
					result = SCOSetValue("cmi.objectives." + i + "." + elem, v)
				}
			}
		}
	}
	return result
}

function SCOGetObjectiveData(id, elem) {
	var i = SCOGetObjectiveIndex(id);
	if (!isNaN(i)) {
		return SCOGetValue("cmi.objectives." + i + "."+elem)
	}
	return ""
}

function SCOGetObjectiveIndex(id){
	var i = -1;
	var nCount = parseInt(SCOGetValue("cmi.objectives._count"));
	if (!isNaN(nCount)) {
		for (i = nCount-1; i >= 0; i--){ //volver atr�s en caso de que LMS realice entradas contables
			if (SCOGetValue("cmi.objectives." + i + ".id") == id) {
				return i
			}
		}
	}
	return NaN
}

// Funciones para convertir tokens compatibles con AICC o abreviaturas en tokens de SCORM
function AICCTokenToSCORMToken(strList,strTest){
	var a = strList.split(",");
	var c = strTest.substr(0,1).toLowerCase();
	for (i=0;i<a.length;i++){
			if (c == a[i].substr(0,1)) return a[i]
	}
	return strTest
}

function normalizeStatus(status){
	return AICCTokenToSCORMToken("completed,incomplete,not attempted,failed,passed", status)
}

function normalizeInteractionType(theType){
	theType=(theType=="other")?"choice":theType;
	return AICCTokenToSCORMToken("true-false,choice,fill-in,matching,performance,sequencing,likert,numeric", theType);
}

function normalizeInteractionResult(result){
	result=(result=="incorrect")?"wrong":result;
	return AICCTokenToSCORMToken("correct,wrong,unanticipated,neutral", result)
}

// Detectar Internet Explorer
var g_bIsInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;

// Gestionar mensajes de FSCommand desde una pel�cula Flash, reasignando todos los comandos de plantilla Flash de AICC a SCORM si es necesario
function nswf_DoFSCommand(command, args){
	var nswfObj = g_bIsInternetExplorer ? nswf : document.nswf;
	// no funciona si no hay disponible una API de SCORM
	var myArgs = new String(args);
	var cmd = new String(command);
	var v = "";
	var err = "true";
	var arg1, arg2, n, s, i;
	var sep = myArgs.indexOf(",");

	if (sep > -1){
		arg1 = myArgs.substr(0, sep); // Nombre de elemento de datos para obtener desde API
		arg2 = myArgs.substr(sep+1) 	// Nombre de variable de pel�cula Flash para establecer
	} else {
		arg1 = myArgs
	}

	if (!APIOK()) return;

	if (cmd.substring(0,3) == "LMS"){
		// Gestionar FSCommands "LMSxxx" (compatible con plantilla html fsSCORM)
		if ( cmd == "LMSInitialize" ){
			err = (APIOK() + "")
			// El LMSInitialize real lo llama autom�ticamente la plantilla
		}	else if ( cmd == "LMSSetValue" ){
			err = SCOSetValue(arg1,arg2)
		} else if ( cmd == "LMSFinish" ){
			err = SCOFinish()
			// Gestionado autom�ticamente por la plantilla, aunque la pel�cula puede llamarlo antes
		}	else if ( cmd == "LMSCommit" ){
			err = SCOCommit()
		}	else if ( cmd == "LMSFlush" ){
			// no-op
			// LMSFlush no est� definido en SCORM y si se llama causa errores de prueba
		}	else if ((arg2) && (arg2.length > 0)){
			if ( cmd == "LMSGetValue") {
				nswfObj.SetVariable(arg2, SCOGetValue(arg1));
			}	else if ( cmd == "LMSGetLastError") {
				nswfObj.SetVariable(arg2, SCOGetLastError(arg1));
			}	else if ( cmd == "LMSGetErrorString") {
				nswfObj.SetVariable(arg2, SCOGetLastError(arg1));
			}	else if ( cmd == "LMSGetDiagnostic") {
				nswfObj.SetVariable(arg2, SCOGetDiagnostic(arg1));
			}	else {
				// para una extensi�n LMSGetxxxx desconocida
				v = eval('g_objAPI.' + cmd + '(\"' + arg1 + '\")');
				nswfObj.SetVariable(arg2,v);
			}
		} else if (cmd.substring(0,3) == "LMSGet") {
			err = "-2: No Flash variable specified"
		}
		// fin de control de comandos "LMSxxx"
	} else if ((cmd.substring(0,6) == "MM_cmi")||(cmd.substring(0,6) == "CMISet")) {
		// Gestionar FSCommands de componentes de aprendizaje de Macromedia.
		// Utilizan convenciones del modelo de datos AICC HACP, por lo que hay que reasignar datos de AICC a SCORM seg�n sea necesario.
		var F_intData = myArgs.split(";");
		if (cmd == "MM_cmiSendInteractionInfo") {
			n = SCOGetValue("cmi.interactions._count");
			s = "cmi.interactions." + n + ".";
			// Captura errores graves para evitar fallos de prueba de conformidad SCORM
			// Si no se proporciona un ID para esta interacci�n, no podemos registrarla
			v = F_intData[2]
			if ((v == null) || (v == "")) err = 201; // Si no hay ID, no tiene sentido registrar
			if (err =="true"){
				err = SCOSetValue(s + "id", v)
			}
			if (err =="true"){
				var re = new RegExp("[{}]","g")
				for (i=1; (i<9) && (err=="true"); i++){
					v = F_intData[i];
					if ((v == null) || (v == "")) continue
					if (i == 1){
						err = SCOSetValue(s + "time", v)
					} else if (i == 3){
						err = SCOSetValue(s + "objectives.0.id", v)
					} else if (i == 4){
						err = SCOSetValue(s + "type", normalizeInteractionType(v))
					} else if (i == 5){
						// extrae "{" y "}" de la respuesta
						v = v.replace(re, "");
						err = SCOSetValue(s + "correct_responses.0.pattern", v)
					} else if (i == 6){
						// extrae "{" y "}" de la respuesta
						v = v.replace(re, "");
						err = SCOSetValue(s + "student_response", v)
					} else if (i == 7){
						err = SCOSetValue(s + "result", normalizeInteractionResult(v))
					} else if (i == 8){
						err = SCOSetValue(s + "weighting", v)
					} else if (i == 9){
						err = SCOSetValue(s + "latency", v)
					}
				}
			}
		} else if (cmd == "MM_cmiSendObjectiveInfo"){
			err = SCOSetObjectiveData(F_intData[1], ".score.raw", F_intData[2])
			if (err=="true"){
				SCOSetObjectiveData(F_intData[1], ".status", normalizeStatus(F_intData[3]))
			}
		} else if ((cmd=="CMISetScore") ||(cmd=="MM_cmiSendScore")){
			err = SCOSetValue("cmi.core.score.raw", F_intData[0]);
		} else if ((cmd=="CMISetStatus") || (cmd=="MM_cmiSetLessonStatus")){
			err = SCOSetValue("cmi.core.lesson_status", normalizeStatus(F_intData[0]))
		} else if (cmd=="CMISetTime"){
			err = SCOSetValue("cmi.core.session_time", F_intData[0])
		} else if (cmd=="CMISetCompleted"){
			err = SCOSetStatusCompleted()
		} else if (cmd=="CMISetStarted"){
			err = SCOSetValue("cmi.core.lesson_status", "incomplete")
		} else if (cmd=="CMISetPassed"){
			err = SCOSetValue("cmi.core.lesson_status", "passed")
		} else if (cmd=="CMISetFailed"){
			err = SCOSetValue("cmi.core.lesson_status", "failed")
		} else if (cmd=="CMISetData"){
			err = SCOSetValue("cmi.suspend_data", F_intData[0])
		} else if (cmd=="CMISetLocation"){
			err = SCOSetValue("cmi.core.lesson_location", F_intData[0])
		} else if (cmd=="CMISetTimedOut"){
			err = SCOSetValue("cmi.core.exit", "time-out")
		} // Otros FSCommands del componente de aprendizaje no funcionan en este contexto
	} else {
		if (cmd=="CMIFinish" || cmd=="CMIExitAU"){
			err = SCOFinish()
		} else if (cmd=="CMIInitialize" || cmd=="MM_StartSession"){
			err = SCOInitialize()
		} else {
			// Comando desconocido; puede estar invocando una extensi�n de API
			// Si los comandos vienen con un segundo argumento, asumir que se espera un valor otherwise assume it is just a cmd
			if (eval('g_objAPI.' + cmd)) {
				v = eval('g_objAPI.' + cmd + '(\"' + arg1 + '\")');
				if ((arg2) && (arg2.length > 0)){
					nswfObj.SetVariable(arg2,v)
				} else {
					err = v
				}
			} else {
				err = "false"
			}
		}
	}
	// Fin de conversi�n y procesamiento de comandos
	// gestionar errores detectados, como devoluciones de errores de LMS
	if ((g_bShowApiErrors) && (err != "true")) {
		AlertUserOfAPIError(ExpandString(g_strFSAPIError, err, cmd, args))
	}
	return err
}

function checkInteractionResponse(response_str)
{
	var result_str = "";
	for(var char_int=0;char_int<response_str.length;char_int++)
	{
		if(response_str.substr(char_int,1) == "." || response_str.substr(char_int,1) == ",")
		{
			if(response_str.substr(char_int - 1,1) != "[" && response_str.substr(char_int + 1,1) != "]")
			{
				result_str += "[" + response_str.substr(char_int,1) + "]";
			} else {
				result_str += response_str.substr(char_int,1);
			}
		} else {
			result_str += response_str.substr(char_int,1);
		}
	}
	result_str = (result_str==""?"0":result_str);
	return result_str;
}


var findExAPITries;
var API_SUMTOTAL=null;
API_SUMTOTAL=getExtendedAPI();
function scanAPI(win){
   while ((win.API_Extended == null) && (win.parent != null) && (win.parent != win))   {
      findExAPITries ++;
      // Note: 7 is an arbitrary number, but should be more than sufficient
      if (findExAPITries  > 50){
         return null;
      }      
      win = win.parent;
   }
   return win.API_Extended;
}

function getExtendedAPI(){
	findExAPITries = 0;
	var theAPI = scanAPI(window);
	if ((theAPI == null) && (window.opener != null) && (typeof(window.opener) != "undefined")){
		theAPI = scanAPI(window.opener);
	}
	return theAPI
}