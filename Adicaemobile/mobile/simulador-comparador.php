﻿<?php 
	session_start();
	error_reporting(E_ALL & ~E_NOTICE);
	require_once 'inc/new_header.php';	
	
	if(isset($_SESSION['valores'])):
	    foreach ($_SESSION['valores'] as $key => $valor):
	        $$key = $valor;
	    endforeach;
	endif;
?>	
	<div class="simulador">
		<div class="subheader">
			<h3>COMPARADOR DE HIPOTECAS</h3>			
		</div>

		<p>Busque la hipoteca que más le convenga.</p>

		<p>Seleccione el tipo de producto (hipotecas  con plazo hasta 30 años o con plazo mayor de 30 años).</p>

		<p>Compare las condiciones ofrecidas por los diferentes bancos.</p>

		<p>Visualice el producto y las advertencias sobre el mismo.</p>

		<form method="post" action="simulador-comparador-pruebas.php?action=form&result=off&opcion=1">
			<input type="submit" value="PLAZO DE HASTA 30 AÑOS"/>
		</form>
	
		<form method="post" action="simulador-comparador-pruebas.php?action=form&result=off&opcion=2">
			<input type="submit" value="MÁS DE 30 AÑOS"/>
		</form>
	</div>
<?php
	require_once 'inc/new_footer.php';
	
	if(isset($_SESSION['errores'])):
		unset($_SESSION['errores']);
	endif;
	if(isset($_SESSION['valores'])):
		unset($_SESSION['valores']);
	endif;
?>

<!--script type="text/javascript">
	<?php if($_GET['result']=="on"):?>
		setTimeout(function () {
			$('html, body').animate({scrollTop: $("select#euribor_revision").offset().top }, 1500);
		}, 500);
	<?php endif; ?>
	<?php if (isset($_SESSION['errores'])): ?>
		setTimeout(function () {
			$('html, body').animate({scrollTop: $("span.byline").offset().top }, 1500);
		}, 500);
	<?php endif;?>
	
	$(document).ready(function(){
		$('#btn_clean').bind('click', function() {
			$(':text').val('');
			$( "select" ).val('');
			$( "select#revision" ).val('A');
			$( "select#euribor_revision" ).val('1');
			$('span.alert').remove();
			$('.result').remove();
			$('html, body').animate({scrollTop:0 }, 1500);
		});
	});
</script-->
