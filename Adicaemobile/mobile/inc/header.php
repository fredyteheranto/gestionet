<!DOCTYPE HTML>
<!--
	Solarize by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>ADICAE GANA LA MACRODEMANDA DE CLÁUSULAS SUELO</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="cláusulas suelo, hipotecas, afectados, suelo, cláusulas, demanda, adicae, colectivo, " />
        <meta name="description" content="Afectados Cláusulas Suelo. Información para los afectados con cláusulas suelo en su hipoteca y sobre cómo unirse a las demandas presentadas por ADICAE" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="../js/jquery.min.js"></script>
		<script src="../js/jquery.dropotron.min.js"></script>
		<script src="../js/skel.min.js"></script>
		<!--script src="js/skel-layers.min.js"></script-->
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="../css/skel.css" />
			<link rel="stylesheet" href="../css/style.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body>
        <!--a href="http://adicae.net/preinscripcion-socios-paso1.html?modalidad=103"><img id="socio" src="images/Banner_2014_socios_v3.png" title="Hazte socios de ADICAE"></a-->
		<!-- Header Wrapper -->
			<!--div class="wrapper style1">
				<div id="header">
					<div class="container">
							
							<img id="logo" src="images/adicae_trans.png" title="ADICAE"><h4><a href="index.php" id="logo">La gran fuerza colectiva de los consumidores</a></h4>
                            <h1>Plataforma de afectados<br>de cláusula suelo</h1>
                            <h2>Contacto: 900 80 10 76 - 91 540 05 13</h2>
                            <h3>(de lunes a viernes de 10:00 a 13:00<br>y de 17:00 a 19:00)</h3>
							<nav id="nav">
								<ul>
									<li class="active"><a href="index.php">INICIO</a></li>
                                    <li><a href="inscribirse.php">APÚNTATE A LA PLATAFORMA</a></li>
                                    <li><a href="simulador-datos.php?action=form&result=off">CALCULA CUÁNTO RECUPERARÁS</a></li>
                                    <li><a href="porque-reclamar.php">POR QUÉ RECLAMAR</a></li>
                                    <li><a href="proximas-asambleas.php">ASAMBLEAS INFORMATIVAS</a></li>
                                    <li><a href="http://adicae.net/localizacion-sedes.html">SEDE MÁS CERCANA</a></li>
                                    <li><a href="preguntas-frecuentes.php">PREGUNTAS MÁS FRECUENTES</a></li>
                                    <li><a href="ofertas-bancos.php">OJO CON LAS OFERTAS</a></li>
                                    <li><a href="proximas-movilizaciones.php">MOVILIZACIONES</a></li>
                                    <li><a href="ultimas-noticias.php">ÚLTIMAS NOTICIAS</a></li>
                                    <li><a href="http://publicaciones.adicae.net/publicaciones/pdf/Sentenciamacro.pdf">DESCARGA LA SENTENCIA</a></li>
                                    <li><a href="material-informativo.php">MATERIAL INFORMATIVO</a></li>
								</ul>
							</nav> 
	
					</div>
				</div>
			</div-->
