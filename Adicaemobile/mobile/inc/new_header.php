<!DOCTYPE HTML>
<html>
<head>
	<title>ADICAE GANA LA MACRODEMANDA DE CLÁUSULAS SUELO</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="cláusulas suelo, hipotecas, afectados, suelo, cláusulas, demanda, adicae, colectivo, " />
	<meta name="description" content="Afectados Cláusulas Suelo. Información para los afectados con cláusulas suelo en su hipoteca y sobre cómo unirse a las demandas presentadas por ADICAE" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/slicknav.min.css" />
</head>	
<body>
	<header id="nav">
		<div id="brand">
			<div class="image"><img src="images/logo.svg"/></div>
			<div class="nombre">Adicae</div>
		</div>
		<ul id="menu" style="display:none;">
			<li><a href="simulador-datos.php" class="balanza">Cl&aacute;usulas suelo</a></li>
			<li><a href="simulador-hipotecario.php" class="calculadora">Simulador hipotecario</a></li>
			<li><a href="simulador-comparador.php" class="lupa">Comparador de hipotecas</a></li>
			<li><a href="noticias.php" class="noticias">Noticias</a></li>
		</ul>
	</Header>