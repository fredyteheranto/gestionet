﻿<?php 
	session_start();
	error_reporting(E_ALL & ~E_NOTICE);
	require_once 'inc/new_header.php';	
	

	require_once 'inc/new_header.php';	
	

	//Valor por defecto
		$capital_inicial="";
		$anyos="";
		$meses="";
		$tae="";
		
		$cuota="";

	if(isset($_SESSION['valores'])){
	    foreach ($_SESSION['valores'] as $key => $valor):
	        $$key = $valor;
	    endforeach;
	}
?>	
		<div class="simulador">
			<div class="subheader">
				<h3>SIMULADOR HIPOTECARIO</h3>				
			</div>
                        
			<p>Completa el formulario para calcular la cuota y el cuadro de amortización que podría tener la hipoteca. El tipo de interés es el tipo de referencia (EURIBOR, IRPH, CECA, etc) + el diferencial (0.25%, 0.75%, 1% etc). Indica los decimales con un punto de separación (ej: 5.15). </p>
	

			<form method="post" action="process/simulador-calcular-hipotecario.php">
				
				<?php if (isset($_SESSION['errores'])): ?>
					<span class="alert corner-all">
						<?php foreach ($_SESSION['errores'] as $id => $error): ?>
							<p><?php echo $error ?></p>
						<?php endforeach; ?>
					</span>
				<?php endif;?>

				<div class="form_block">
					<div class="form_label">
						<label for="capital_inicial">Importe de la hipoteca (€)</label>
					</div>
					<div class="form_input">
						<input type="text" name="capital_inicial" id="capital_inicial" value="<?php echo $capital_inicial; ?>"/> 
					</div>
				</div>

				<div class="form_block" style="clear:both;">
					<div class="form_label"><label for="duracion">Duración del préstamo</label></div>
					
					<div class="form_label_esp"><label for="anyos">Años:</label></div>
					<div class="form_input_esp">
						<input type="text" name="anyos" id="anyos" value="<?php echo $anyos; ?>"/> 
					</div>

					<div class="form_label_esp"><label for="meses">y meses:</label></div>
					<div class="form_input_esp">
						<input type="text" name="meses" id="meses" value="<?php echo $meses; ?>"/> 
					</div>
				</div>

				<div class="form_block">
					<div class="form_label"><label for="tae">Tipo de interés T.A.E. (%)</label></div>
					<div class="form_input">
						<input type="text" name="tae" id="tae" value="<?php echo $tae; ?>"/> 
					</div>
				</div>

				<input type="button" id="btn_clean" value=" Limpiar "/>
				<input type="submit" value="Calcular"/>
            </form>

			<?php if($_GET['result']=="on"){ ?>

				<h3>Tabla de amortización incluyendo la cuota mensual a pagar.</h3>

				<!--<div class="form_block result">
					<div class="form_label">
						<label for="cuota">Cuota mensual a pagar:  (€)</label>
					</div>
					<div class="form_input">
						<input type="text" class="" name="cuota" id="cuota" size="18"
						value="<?php echo $cuota; ?>"/> 
					</div>
				</div>-->
				
				<table style="text-align:center;" class="result">
					<tr>
						<th style="text-align:center;">A&ntilde;o</th>
						<th style="text-align:center;">Mes</th>
						<th style="text-align:center;">Cuota</th>
						<th style="text-align:center;">Interés</th>
						<th style="text-align:center;">Amortización</th>
						<th style="text-align:center;">Pendiente</th>
					<tr>
					<?php
						$arrTabla = $_SESSION["tabla_datos_meses"];
						if(is_array($arrTabla)){
							foreach($arrTabla as $anio=>$meses){
								if(is_array($meses)){
									foreach($meses as $mes=>$datos){
										?>
										<tr>
											<td style="text-align:center;"><?php echo($anio); ?></td>
											<td style="text-align:center;"><?php echo($mes); ?></td>
											<td style="text-align:center;"><?php echo($datos["cuota_C"]); ?></td>
											<td style="text-align:center;"><?php echo($datos["interes_C"]); ?></td>
											<td style="text-align:center;"><?php echo($datos["amortizacion_C"]); ?></td>
											<td style="text-align:center;"><?php echo($datos["capital_C"]); ?></td>
										<tr>
										<?php
									}
								}
							}
						}
					?>
				</table>

			<?php }?>

		</div>
<?php	
	require_once 'inc/new_footer.php';
	
	if(isset($_SESSION['errores'])){
		unset($_SESSION['errores']);
	}
	if(isset($_SESSION['valores'])){
		unset($_SESSION['valores']);
	}	
?>

<script type="text/javascript">
	
	$(document).ready(function(){

		$('#btn_clean').bind('click', function() {
			$(':text').val('');
			$('span.alert').remove();
			$('.result').remove();
			$('html, body').animate({scrollTop:0 }, 1500);
		});

	});
</script>
