<?php session_start();
error_reporting(E_ALL & ~E_NOTICE);
ini_set("log_errors", 1);
ini_set("error_log", "err.log");
include('../../lib/functions.php');
//echo '<pre>'; print_r($_POST); echo '</pre>'; die; // DEBUG

foreach ($_POST as $key => $value) {
    if (!is_array($value)) {
        $$key = $value;
    } else {
        $$key = $value;
    }
}
$errores = array();

// VALIDACIONES
$capital_inicial=trim($capital_inicial);
if ($capital_inicial == '') {
	$errores['capital_inicial'] = "Debe introducir un importe del capital inicial del préstamo";
}else{
	if(!preg_match("/^-?[0-9]+([.][0-9]*)?$/", $capital_inicial)){
		if(preg_match("/^-?[0-9]+([,][0-9]*)?$/", $capital_inicial) || (preg_match("[.]",$capital_inicial) && preg_match("[,]",$capital_inicial))){
			$errores['capital_inicial']='Los decimales del capital inicial deben ir separados por puntos.';
		}else{
    		$errores['capital_inicial']='El capital inicial tiene que ser numérico y decimal (ej. 105000.55)';
    	}
    }elseif ($capital_inicial < 0) {
		$errores['capital_inicial'] = "El capital inicial no puede ser negativo";
	}
}

$tipo_fijo_1p=trim($tipo_fijo_1p);
if(empty($dia_fecha_firma) || empty($mes_fecha_firma) || empty($ano_fecha_firma)) {
        $errores['fecha_fecha_firma'] = 'Debe seleccionar una fecha de firma';
}elseif((empty($dia_fecha_1rev) || empty($mes_fecha_1rev) || empty($ano_fecha_1rev)) && !(empty($dia_fecha_1rev) && empty($mes_fecha_1rev) && empty($ano_fecha_1rev) && empty($tipo_fijo_1p))) {
        $errores['fecha_fecha_1rev'] = 'Debe seleccionar una fecha de revisión';
}elseif(!empty($dia_fecha_firma) || !empty($mes_fecha_firma) || !empty($ano_fecha_firma) || !empty($dia_fecha_1rev) || !empty($mes_fecha_1rev) || !empty($ano_fecha_1rev))
{
    if (empty($dia_fecha_firma) || empty($mes_fecha_firma) || empty($ano_fecha_firma))
    {
        $errores['fecha_fecha_firma'] = 'El formato de fecha de firma no es válido';
    }elseif((empty($dia_fecha_1rev) || empty($mes_fecha_1rev) || empty($ano_fecha_1rev))&&((!empty($dia_fecha_1rev) || !empty($mes_fecha_1rev) || !empty($ano_fecha_1rev)))){
    	$errores['fecha_fecha_1rev'] = 'El formato de fecha de la primera revisión no es válido';
    }
    else
    {
		if (!checkdate($mes_fecha_firma, $dia_fecha_firma, $ano_fecha_firma))
		{
			$errores['fecha_fecha_firma'] = 'La fecha de firma no existe en el calendario';
		}else{
			$fecha_firma = $dia_fecha_firma.'/'.$mes_fecha_firma.'/'.$ano_fecha_firma;	
		}
		
		if(!(empty($dia_fecha_1rev)&&empty($mes_fecha_1rev)&&empty($ano_fecha_1rev)&&empty($tipo_fijo_1p))){
			if ((!checkdate($mes_fecha_1rev, $dia_fecha_1rev, $ano_fecha_1rev)) && !(empty($dia_fecha_1rev) && empty($mes_fecha_1rev) && empty($ano_fecha_1rev) && empty($tipo_fijo_1p)))
			{
				$errores['fecha_fecha_1rev'] = 'La fecha de revisión no existe en el calendario';
			}
	    	// Fechas completas
	    	$fecha_1rev = $dia_fecha_1rev.'/'.$mes_fecha_1rev.'/'.$ano_fecha_1rev;
	    	if(comparar_fechas($fecha_firma, $fecha_1rev) !== false && comparar_fechas($fecha_firma, $fecha_1rev) > 0)
	    	{
	    		$errores['fecha_fecha_firma'] = 'El rango de fecha firma y fecha revisión no es válido';
	    	}			
		}
    }
}

$anyos=trim($anyos);	
if ($anyos == '') {
	$errores['anyos'] = "Debe introducir un número de años del préstamo";
}elseif (!validate_number($anyos)) {
	$errores['anyos'] = "El número de años debe ser un valor numérico entero";
}else{
		$decimal_punto_anyos = strpos($anyos, '.');
		$decimal_coma_anyos = strpos($anyos, ',');
		if (($decimal_punto_anyos + $decimal_coma_anyos) > 0) {
			$errores['anyos'] = "El número de años tiene que ser un valor entero";
		}else{
			$anyos = abs($anyos);
			if ($anyos <=0 || $anyos > 50) {
				$errores['anyos'] = "El número de años no puede ser superior a 50";
			}
		}
}

$meses=trim($meses);
if ($meses != '') {
	if (!validate_number($meses)) {
		$errores['meses'] = "El número de meses debe ser un valor numérico entero";
	}else{
		$decimal_punto_meses = strpos($meses, '.');
		$decimal_coma_meses = strpos($meses, ',');
		if (($decimal_punto_meses + $decimal_coma_meses) > 0) {
			$errores['meses'] = "El número de meses tiene que ser un valor entero";
		}else{
			$meses = abs($meses);
			if ($meses <=0 || $meses > 12) {
				$errores['meses'] = "El número de meses no puede ser superior a 12";
			}
		}
	}
}

$diferencial=trim($diferencial);
if ($diferencial == '') {
	$errores['diferencial'] = "Debe introducir un valor diferencial del préstamo (euribor+)";
}else{
	if(!preg_match("/^-?[0-9]+([.][0-9]*)?$/", $diferencial)){
		if(preg_match("/^-?[0-9]+([,][0-9]*)?$/", $diferencial) || (preg_match("[.]",$diferencial) && preg_match("[,]",$diferencial))){
			$errores['diferencial']='Los decimales del diferencial deben ir separados por puntos.';
		}else{
    		$errores['diferencial']='El diferencial tiene que ser numérico y decimal (ej. 1.25)';
    	}
    }elseif ($diferencial < 0) {
		$errores['diferencial'] = "El diferencial no puede ser negativo";
	}elseif ($diferencial > 10) {
		$errores['diferencial'] = "El diferencial no puede ser superior a 10";
	}
}

if(!(empty($dia_fecha_1rev) && empty($mes_fecha_1rev) && empty($ano_fecha_1rev) && empty($tipo_fijo_1p))){
	if ($tipo_fijo_1p == '') {
		$errores['tipo_fijo_1p'] = "Debe introducir un valor tipo fijo primer período";
	}else{
		if(!preg_match("/^-?[0-9]+([.][0-9]*)?$/", $tipo_fijo_1p)){
			if(preg_match("/^-?[0-9]+([,][0-9]*)?$/", $tipo_fijo_1p) || (preg_match("[.]",$tipo_fijo_1p) && preg_match("[,]",$tipo_fijo_1p))){
				$errores['tipo_fijo_1p']='Los decimales del tipo fijo primer período deben ir separados por puntos.';
			}else{
	    		$errores['tipo_fijo_1p']='El tipo fijo primer período tiene que ser numérico y decimal (ej. 1.25)';
	    	}
	    }elseif ($tipo_fijo_1p < 0) {
			$errores['tipo_fijo_1p'] = "El tipo fijo primer período no puede ser negativo";
		}elseif ($tipo_fijo_1p > 10) {
			$errores['tipo_fijo_1p'] = "El tipo fijo primer período no puede ser superior a 10";
		}
	}
}

$clausula_suelo=trim($clausula_suelo);
if ($clausula_suelo == '') {
	$errores['clausula_suelo'] = "Debe introducir un valor de cláusula suelo";
}else{
	if(!preg_match("/^-?[0-9]+([.][0-9]*)?$/", $clausula_suelo)){
		if(preg_match("/^-?[0-9]+([,][0-9]*)?$/", $clausula_suelo) || (preg_match("[.]",$clausula_suelo) && preg_match("[,]",$clausula_suelo))){
			$errores['clausula_suelo']='Los decimales de la cláusula suelo deben ir separados por puntos.';
		}else{
    		$errores['clausula_suelo']='El valor de cláusula suelo tiene que ser numérico y decimal (ej. 1.25)';
    	}
    }elseif ($clausula_suelo < 0) {
		$errores['clausula_suelo'] = "El valor de cláusula suelo no puede ser negativo";
	}elseif ($clausula_suelo > 10) {
		$errores['clausula_suelo'] = "El valor de cláusula suelo no puede ser superior a 10";
	}
}

// Compruebo si tras la validación hay errores
$num_errores = count($errores);

if ($num_errores > 0) {
    $_SESSION['errores'] = $errores;
    $_SESSION['valores'] = $_POST;
    redirectTo('../simulador-datos-pruebas.php?action=form&result=off&opcion='.$opcion);
	
} else {

	// Efectuamos el cálculo

	$month = array('', 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE');
	
	//Carga array del Euribor
	$euribor_array=array();	

	//Cargamos archivo de euribor histórico
	if($indice_referencia == "euribor"){
		$fp = fopen("../../process/euribor.csv", "r");
	}else{
		$fp = fopen("../../process/irph.csv", "r");
	}

    while (!feof($fp)){

        // SE LEE SEPARADO POR PUNTO  Y COMA
        $data  = explode(";", fgets($fp));

        if(isset($data[0]) && isset($data[1]) && isset($data[2])){
        	$euribor_array[$data[0]][$data[1]]=$data[2];	
        }
    }

    fclose($fp);

	//Cargamos archivos de actualizaciones de euribor
    for ($n=2016; $n <= date("Y") ; $n++) { 
    	//Recoge el euribor de archivos descargados
		if($indice_referencia == "euribor"){
			$filename="../../archivos/myav_EURIBOR_".$n.".csv";
		}else{
			$filename="../../archivos/myav_IRPH_".$n.".csv";
		}
    	 
    	//Recoge el euribor de la web de EMMI (descomentar y comentar la anterior)
    	//$filename="http://www.emmi-benchmarks.eu/assets/modules/rateisblue/file_processing/publication/processed/myav_EURIBOR_".$n.".csv";
	    
	    if (($fp2 = @fopen($filename, "r")) !== FALSE) {
		    
		    $n_linea_12M = count(file($filename));
		    $counter=1;

			while (( $data = fgetcsv ( $fp2 , 1000 , "," )) !== FALSE ) {

				if($counter==$n_linea_12M){
					$i=0;
				    foreach($data as $row) {
				    	if($i!=0 && $i<=12){
							$euribor_array[$month[$i]][$n]=$row/100;
				    	}
				        $i++ ;
				    }
				}
				$counter++;
			}

			fclose ( $fp2 );
		}
    }

    //Meses de la hipoteca
    $total_meses_prestamo = ($anyos*12) + $meses;

    //Meses que lleva con la hipoteca hasta el día de hoy
	$total_dias = comparar_fechas(date("d/m/Y"),$fecha_firma);
	$total_anyos = floor($total_dias/365);
	$resto_dias= $total_dias % 365;
	if($total_anyos!=0){
		$dias_bisiestos = floor($total_anyos/4);
	}
	$resto_dias = $resto_dias - $dias_bisiestos;
	if($resto_dias!=0){
		$total_meses = (12 * $total_anyos) +(floor($resto_dias/30));	
	}else{
		$total_meses = (12 * $total_anyos);
	}

    //Meses de tipo fijo
	if(empty($dia_fecha_1rev) && empty($mes_fecha_1rev) && empty($ano_fecha_1rev) && empty($tipo_fijo_1p)){
		$tipo_fijo_meses=0;
	}else{
		$tipo_fijo_dias = comparar_fechas($fecha_1rev,$fecha_firma);
		$tipo_fijo_anyos = floor($tipo_fijo_dias/365);
		$tipo_fijo_resto_dias= $tipo_fijo_dias % 365;
		$tipo_fijo_meses = (12 * $tipo_fijo_anyos) +(floor($tipo_fijo_resto_dias/30));
	}

	if($opcion==2){
		//Meses desde la firma de la hipoteca hasta la fecha de no recuperación de cláusula del Supremo
		$Sup_fecha="31/5/2013";
	    
		$Sup_total_dias = comparar_fechas($Sup_fecha,$fecha_firma);
		$Sup_total_anyos = floor($Sup_total_dias/365);
		$Sup_resto_dias= $Sup_total_dias % 365;
		if($Sup_total_anyos!=0){
			$Sup_dias_bisiestos = floor($Sup_total_anyos/4);
		}
		$Sup_resto_dias = $Sup_resto_dias - $Sup_dias_bisiestos;
		if($Sup_resto_dias!=0){
			$Sup_total_meses = (12 * $Sup_total_anyos) +(floor($Sup_resto_dias/30));	
		}else{
			$Sup_total_meses = (12 * $Sup_total_anyos);
		}

	}else{
		$Sup_total_meses=0;
	}

	//Porcentajes en tantos por 1 mensuales
	if($tipo_fijo_1p!=0){
		$tipo_fijo_1p=$tipo_fijo_1p/100;	
	}
	$clausula_suelo=$clausula_suelo/100;

	//Inicializamos los valores de capital
	$capital_C=$capital_S=$capital_inicial;

	//Saco el euribor aplicable	en la primera revisión
	if(empty($dia_fecha_1rev) && empty($mes_fecha_1rev) && empty($ano_fecha_1rev) && empty($tipo_fijo_1p)){
		$n_mes_euribor = $mes_fecha_firma - $euribor_revision;
		if($n_mes_euribor<1){
			$n_mes_euribor=12+$n_mes_euribor;
			$n_ano_fecha_firma = $ano_fecha_firma - 1;
		}else{
			$n_ano_fecha_firma = $ano_fecha_firma;
		}
		$month_euribor=$month[$n_mes_euribor];
		$ano_euribor=$n_ano_fecha_firma;
	}else{
		$n_mes_euribor = $mes_fecha_1rev - $euribor_revision;
		if($n_mes_euribor<1){
			$n_mes_euribor=12+$n_mes_euribor;
			$n_ano_fecha_1rev = $ano_fecha_1rev - 1;
		}else{
			$n_ano_fecha_1rev = $ano_fecha_1rev;
		}
		$month_euribor=$month[$n_mes_euribor];
		$ano_euribor=$n_ano_fecha_1rev;		
	}

	//Precauciones ante una carga tardía del euribor en el archivo csv (euribor desactualizado)
	if(!isset($euribor_array[$month_euribor][$ano_euribor])||empty($euribor_array[$month_euribor][$ano_euribor])){
		if($n_mes_euribor==1){
			$euribor=$euribor_array[$month[12]][$ano_euribor-1];
		}else{
			$euribor=$euribor_array[$month[($n_mes_euribor-1)]][$ano_euribor];
		}
	}else{
		$euribor=$euribor_array[$month_euribor][$ano_euribor];	
	}

	$contador = 13; //Para que entre la primera vez

	$importe_pagado_demas=0;
	$tipo_variable=($diferencial/100) + $euribor;
	
	if($revision=="A"){
		$limite_contador=12;
	}elseif($revision=="S"){
		$limite_contador=6;
	}

	$tiempo_restante=$total_meses_prestamo;

	//Comprobamos si la hipoteca ha terminado
	$fin_hipoteca=0;
	if($total_meses_prestamo<=$total_meses){
		$total_meses=$total_meses_prestamo;
		$fin_hipoteca=1;
	}
	
	$cuota_C=$interes_C=$amortizacion_C=$cuota_S=$interes_S=$amortizacion_S=0;
	//Calculamos cuota, interés, capital vivo y acumulamos diferencia de cuotas por cada mes
	$_SESSION["tabla_datos_meses"] = array();
	$arrTabla = array();
	for($i = $total_meses; $i > 0; $i--){ 
		$date = strtotime("+". ($total_meses-$i+1) ." months", strtotime($_POST["dia_fecha_firma"] ."-". $_POST["mes_fecha_firma"] ."-". $_POST["ano_fecha_firma"]));
		
		$mes_tabla = date("m", $date);
		$anio_tabla = date("Y", $date);
		
		if($tipo_fijo_meses>0){
			//Periodo de tipo fijo			
			$cuota_C=$capital_inicial/((1-pow((1+$tipo_fijo_1p/12),-$total_meses_prestamo))/($tipo_fijo_1p/12));
			$cuota_C=round($cuota_C, 2);
			$interes_C=($capital_C*$tipo_fijo_1p)/12;
			$interes_C=round($interes_C, 2);
			$amortizacion_C=$cuota_C-$interes_C;
			$capital_C=$capital_C-$amortizacion_C;
			$capital_S = $capital_C;
			$tipo_fijo_meses--;
			$tiempo_restante--;
			$Sup_total_meses--;


		}else{

			//Controla la selección de euribor según la revisión de hipoteca
			if ($contador>$limite_contador){
				//Calculamos las cuotas de esta revisión
				$tipo_variable=($diferencial/100) + $euribor;

				if($tipo_variable>=$clausula_suelo){ //Tipo por encima de la cláusula suelo
					$cuota_C=$capital_C/((1-pow((1+$tipo_variable/12),-$tiempo_restante))/($tipo_variable/12));
					$cuota_C=round($cuota_C, 2);
					if($capital_S!=$capital_C){ //Cuando se ha ejecutado alguna vez la cláusula suelo
						$cuota_S=$capital_S/((1-pow((1+$tipo_variable/12),-$tiempo_restante))/($tipo_variable/12));
						$cuota_S=round($cuota_S, 2);
					}
				}elseif($tipo_variable<$clausula_suelo){ //Tipo por debajo de la cláusula suelo - Ejecución de la cláusula
					$cuota_C=$capital_C/((1-pow((1+$clausula_suelo/12),-$tiempo_restante))/($clausula_suelo/12));
					$cuota_C=round($cuota_C, 2);
					$cuota_S=$capital_S/((1-pow((1+$tipo_variable/12),-$tiempo_restante))/($tipo_variable/12));
					$cuota_S=round($cuota_S, 2);
				}

				if(($revision=="S" && $total_meses>=6)||($revision=="A" && $total_meses>=12)){
					//Calculamos el euribor de la siguiente revisión				
					if($revision=="S" && $n_mes_euribor <= 6){
						$n_mes_euribor = $n_mes_euribor + 6;
						$month_euribor=$month[$n_mes_euribor];
					}elseif($revision=="S" && $n_mes_euribor > 6){
						$n_mes_euribor = ($n_mes_euribor + 6)-12;
						$month_euribor=$month[$n_mes_euribor];
						$ano_euribor++;
					}elseif($revision=="A") {
						$ano_euribor++;
					}

					//Precauciones ante una carga tardía del euribor en el archivo csv (euribor desactualizado)
					if($ano_euribor<date("Y") || ($ano_euribor==date("Y") && $n_mes_euribor<date("m"))){ 
						if(!isset($euribor_array[$month_euribor][$ano_euribor])||empty($euribor_array[$month_euribor][$ano_euribor])){
							if($n_mes_euribor==1){
								$euribor=$euribor_array[$month[12]][$ano_euribor-1];
							}else{
								$euribor=$euribor_array[$month[($n_mes_euribor-1)]][$ano_euribor];
							}
						}else{
							$euribor=$euribor_array[$month_euribor][$ano_euribor];	
						}
					}
				}

				$contador=1;
			}


			if($Sup_total_meses>0){
				//Periodo de anterior a 2013
				if ($contador<=$limite_contador){

					if($tipo_variable>=$clausula_suelo){ //Tipo por encima de la cláusula suelo
						
						$interes_C=($capital_C*$tipo_variable)/12;

					}elseif($tipo_variable<$clausula_suelo){ //Tipo por debajo de la cláusula suelo - Ejecución de la cláusula

						$interes_C=($capital_C*$clausula_suelo)/12;

					}

					$interes_C=round($interes_C, 2);
					$amortizacion_C=$cuota_C-$interes_C;
					$capital_C=$capital_C-$amortizacion_C;
					$capital_S=$capital_C;
	
					$Sup_total_meses--;
					$tiempo_restante--;
					$contador++;

				}
			}else{
				//Periodo de tipo variable
				if ($contador<=$limite_contador){

					if($tipo_variable>=$clausula_suelo){ //Tipo por encima de la cláusula suelo
						$interes_C=($capital_C*$tipo_variable)/12;
						$interes_C=round($interes_C, 2);
						$amortizacion_C=$cuota_C-$interes_C;
						
						if($capital_S!=$capital_C){ //Cuando se ha ejecutado alguna vez la cláusula suelo
							$interes_S=($capital_S*$tipo_variable)/12;
							$interes_S=round($interes_S, 2);
							$amortizacion_S=$cuota_S-$interes_S;
							$capital_S=$capital_S-$amortizacion_S;
							$capital_C=$capital_C-$amortizacion_C;
							$importe_pagado_demas = $importe_pagado_demas + ($cuota_C-$cuota_S);
						}elseif($capital_S==$capital_C){ //Cuando no se ha ejecutado todavía la cláusula suelo
							$capital_C=$capital_C-$amortizacion_C;
							$capital_S=$capital_C;
						}					
						
					}elseif($tipo_variable<$clausula_suelo){ //Tipo por debajo de la cláusula suelo - Ejecución de la cláusula

						$interes_C=($capital_C*$clausula_suelo)/12;
						$interes_C=round($interes_C, 2);
						$amortizacion_C=$cuota_C-$interes_C;
						$capital_C=$capital_C-$amortizacion_C;

						$interes_S=($capital_S*$tipo_variable)/12;
						$interes_S=round($interes_S, 2);
						$amortizacion_S=$cuota_S-$interes_S;
						$capital_S=$capital_S-$amortizacion_S;

						$importe_pagado_demas = $importe_pagado_demas + ($cuota_C-$cuota_S);
					}

					$tiempo_restante--;
					$contador++;

				}

			}

		}
		
		$arrTabla[$anio_tabla][$mes_tabla]["cuota_C"] = $cuota_C;
		$arrTabla[$anio_tabla][$mes_tabla]["interes_C"] = $interes_C;
		$arrTabla[$anio_tabla][$mes_tabla]["amortizacion_C"] = $amortizacion_C;
		$arrTabla[$anio_tabla][$mes_tabla]["capital_C"] = $capital_C;
		
		if($arrTabla[$anio_tabla][$mes_tabla]["cuota_C"]<0){
			$arrTabla[$anio_tabla][$mes_tabla]["cuota_C"] = "0.00";
		}else{
			$arrTabla[$anio_tabla][$mes_tabla]["cuota_C"] = round($arrTabla[$anio_tabla][$mes_tabla]["cuota_C"], 2);
		}
		
		if($arrTabla[$anio_tabla][$mes_tabla]["interes_C"]<0){
			$arrTabla[$anio_tabla][$mes_tabla]["interes_C"] = "0.00";
		}else{
			$arrTabla[$anio_tabla][$mes_tabla]["interes_C"] = round($arrTabla[$anio_tabla][$mes_tabla]["interes_C"], 2);
		}
		
		if($arrTabla[$anio_tabla][$mes_tabla]["amortizacion_C"]<0){
			$arrTabla[$anio_tabla][$mes_tabla]["amortizacion_C"] = "0.00";
		}else{
			$arrTabla[$anio_tabla][$mes_tabla]["amortizacion_C"] = round($arrTabla[$anio_tabla][$mes_tabla]["amortizacion_C"], 2);
		}
		
		if($arrTabla[$anio_tabla][$mes_tabla]["capital_C"]<0){
			$arrTabla[$anio_tabla][$mes_tabla]["capital_C"] = "0.00";
		}else{
			$arrTabla[$anio_tabla][$mes_tabla]["capital_C"] = round($arrTabla[$anio_tabla][$mes_tabla]["capital_C"], 2);
		}

    }
	$_SESSION["tabla_datos_meses"] = $arrTabla;
	
	
    if($fin_hipoteca==1){
    	$capital_no_amortizado = 0;	
    }else{
	    $capital_no_amortizado = $capital_C - $capital_S;
	    $capital_no_amortizado = round($capital_no_amortizado, 2);    	
    }

    $importe_pagado_demas = round($importe_pagado_demas, 2);  

    $total_perjuicio = $capital_no_amortizado + $importe_pagado_demas;

	// Mostraremos los resultados
	$_POST['capital_no_amortizado'] = $capital_no_amortizado;
	$_POST['importe_pagado_demas'] = $importe_pagado_demas;
	$_POST['total_perjuicio'] = $total_perjuicio;
    $_SESSION['valores'] = $_POST;
	
	redirectTo('../simulador-datos-pruebas.php?action=list&result=on&opcion='.$opcion);
}


/*
 * Función para validar números
 */
function validate_number($number) {
	$regex = '/^[0-9]+$/';
    if(preg_match($regex, $number)) return true;
    else return false;
}

/*
 * Función para validar fechas
 */
function comparar_fechas($primera, $segunda)
 {
  $valoresPrimera = explode ("/", $primera);
  $valoresSegunda = explode ("/", $segunda);
  $diaPrimera    = $valoresPrimera[0];
  $mesPrimera  = $valoresPrimera[1];
  $anyoPrimera   = $valoresPrimera[2];
  $diaSegunda   = $valoresSegunda[0];
  $mesSegunda = $valoresSegunda[1];
  $anyoSegunda  = $valoresSegunda[2];
  $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);
  $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);
  if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
    // "La fecha ".$primera." no es válida";
    return false;
  }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
    // "La fecha ".$segunda." no es válida";
    return false;
  }else{
    return  $diasPrimeraJuliano - $diasSegundaJuliano;
  }
}


?>
