<?php session_start();
error_reporting(E_ALL & ~E_NOTICE);
ini_set("log_errors", 1);
ini_set("error_log", "err.log");
include('../../lib/functions.php');
//echo '<pre>'; print_r($_POST); echo '</pre>'; die; // DEBUG

foreach ($_POST as $key => $value) {
    if (!is_array($value)) {
        $$key = $value;
    } else {
        $$key = $value;
    }
}
$errores = array();

// VALIDACIONES
$capital_inicial=trim($capital_inicial);
if ($capital_inicial == '') {
	$errores['capital_inicial'] = "Debe introducir un importe del capital inicial del préstamo";
}else{
	if(!preg_match("/^-?[0-9]+([.][0-9]*)?$/", $capital_inicial)){
		if(preg_match("/^-?[0-9]+([,][0-9]*)?$/", $capital_inicial) || (preg_match("[.]",$capital_inicial) && preg_match("[,]",$capital_inicial))){
			$errores['capital_inicial']='Los decimales del capital inicial deben ir separados por puntos.';
		}else{
    		$errores['capital_inicial']='El capital inicial tiene que ser numérico y decimal (ej. 105000.55)';
    	}
    }elseif ($capital_inicial < 0) {
		$errores['capital_inicial'] = "El capital inicial no puede ser negativo";
	}
}

$anyos=trim($anyos);	
if ($anyos == '') {
	$errores['anyos'] = "Debe introducir un número de años del préstamo";
}elseif (!validate_number($anyos)) {
	$errores['anyos'] = "El número de años debe ser un valor numérico entero";
}else{
		$decimal_punto_anyos = strpos($anyos, '.');
		$decimal_coma_anyos = strpos($anyos, ',');
		if (($decimal_punto_anyos + $decimal_coma_anyos) > 0) {
			$errores['anyos'] = "El número de años tiene que ser un valor entero";
		}else{
			$anyos = abs($anyos);
			if ($anyos <=0 || $anyos > 50) {
				$errores['anyos'] = "El número de años no puede ser superior a 50";
			}
		}
}

$meses=trim($meses);
if ($meses != '') {
	if (!validate_number($meses)) {
		$errores['meses'] = "El número de meses debe ser un valor numérico entero";
	}else{
		$decimal_punto_meses = strpos($meses, '.');
		$decimal_coma_meses = strpos($meses, ',');
		if (($decimal_punto_meses + $decimal_coma_meses) > 0) {
			$errores['meses'] = "El número de meses tiene que ser un valor entero";
		}else{
			$meses = abs($meses);
			if ($meses <=0 || $meses > 12) {
				$errores['meses'] = "El número de meses no puede ser superior a 12";
			}
		}
	}
}

$tae=trim($tae);
if ($tae == '') {
	$errores['tae'] = "Debe introducir el tipo de interés del préstamo";
}else{
	if(!preg_match("/^-?[0-9]+([.][0-9]*)?$/", $tae)){
		if(preg_match("/^-?[0-9]+([,][0-9]*)?$/", $tae) || (preg_match("[.]",$tae) && preg_match("[,]",$tae))){
			$errores['tae']='Los decimales del tipo de interés deben ir separados por puntos.';
		}else{
    		$errores['tae']='El tipo de interés tiene que ser numérico y decimal (ej. 1.25)';
    	}
    }elseif ($tae < 0) {
		$errores['tae'] = "El tipo de interés no puede ser negativo";
	}elseif ($tae > 10) {
		$errores['tae'] = "El tipo de interés no puede ser superior a 10";
	}
}


// Compruebo si tras la validación hay errores
$num_errores = count($errores);

if ($num_errores > 0) {
    $_SESSION['errores'] = $errores;
    $_SESSION['valores'] = $_POST;
    redirectTo('../simulador-hipotecario.php?action=form&result=off');
	
} else {

	// Efectuamos el cálculo

	//Meses de la hipoteca
    $total_meses_prestamo = ($anyos*12) + $meses;


	//Inicializamos los valores de capital
	$capital_C=$capital_S=$capital_inicial;


	$contador = 13; //Para que entre la primera vez

	$importe_pagado_demas=0;
	$tipo_variable=($tae/100);

	
	$cuota_C=$interes_C=$amortizacion_C=$cuota_S=$interes_S=$amortizacion_S=0;
	//Calculamos cuota, interés, capital vivo y acumulamos diferencia de cuotas por cada mes
	$_SESSION["tabla_datos_meses"] = array();
	$arrTabla = array();

	$date = new DateTime('now');
	for($i = 0; $i < $total_meses_prestamo; $i++){ 

		addMonths($date, +1);

		$mes_tabla = $date->format('m');
		$anio_tabla = $date->format('Y');

		$cuota_C=$capital_inicial/((1-pow((1+$tipo_variable/12),-$total_meses_prestamo))/($tipo_variable/12));
		$cuota_C=round($cuota_C, 2);

		$interes_C=($capital_C*$tipo_variable)/12;
		$interes_C=round($interes_C, 2);
		$amortizacion_C=$cuota_C-$interes_C;
		$capital_C=$capital_C-$amortizacion_C;

		$arrTabla[$anio_tabla][$mes_tabla]["cuota_C"] = $cuota_C;
		$arrTabla[$anio_tabla][$mes_tabla]["interes_C"] = $interes_C;
		$arrTabla[$anio_tabla][$mes_tabla]["amortizacion_C"] = $amortizacion_C;
		$arrTabla[$anio_tabla][$mes_tabla]["capital_C"] = $capital_C;
		
		if($arrTabla[$anio_tabla][$mes_tabla]["cuota_C"]<0){
			$arrTabla[$anio_tabla][$mes_tabla]["cuota_C"] = "0.00";
		}else{
			$arrTabla[$anio_tabla][$mes_tabla]["cuota_C"] = round($arrTabla[$anio_tabla][$mes_tabla]["cuota_C"], 2);
		}
		
		if($arrTabla[$anio_tabla][$mes_tabla]["interes_C"]<0){
			$arrTabla[$anio_tabla][$mes_tabla]["interes_C"] = "0.00";
		}else{
			$arrTabla[$anio_tabla][$mes_tabla]["interes_C"] = round($arrTabla[$anio_tabla][$mes_tabla]["interes_C"], 2);
		}
		
		if($arrTabla[$anio_tabla][$mes_tabla]["amortizacion_C"]<0){
			$arrTabla[$anio_tabla][$mes_tabla]["amortizacion_C"] = "0.00";
		}else{
			$arrTabla[$anio_tabla][$mes_tabla]["amortizacion_C"] = round($arrTabla[$anio_tabla][$mes_tabla]["amortizacion_C"], 2);
		}
		
		if($arrTabla[$anio_tabla][$mes_tabla]["capital_C"]<0){
			$arrTabla[$anio_tabla][$mes_tabla]["capital_C"] = "0.00";
		}else{
			$arrTabla[$anio_tabla][$mes_tabla]["capital_C"] = round($arrTabla[$anio_tabla][$mes_tabla]["capital_C"], 2);
		}


	}

	$_SESSION["tabla_datos_meses"] = $arrTabla;

	$_POST['cuota'] = $cuota_C;
    $_SESSION['valores'] = $_POST;

	
	redirectTo('../simulador-hipotecario.php?action=list&result=on');
}

function addMonths($date, $months) {
    $years = floor(abs($months / 12));
    $leap = 29 <= $date->format('d');
    $m = 12 * (0 <= $months?1:-1);
    for ($a = 1;$a < $years;++$a) {
        $date = addMonths($date, $m);
    }
    $months -= ($a - 1) * $m;
    
    $init = clone $date;
    if (0 != $months) {
        $modifier = $months . ' months';
        
        $date->modify($modifier);
        if ($date->format('m') % 12 != (12 + $months + $init->format('m')) % 12) {
            $day = $date->format('d');
            $init->modify("-{$day} days");
        }
        $init->modify($modifier);
    }
    
    $y = $init->format('Y');
    if ($leap && ($y % 4) == 0 && ($y % 100) != 0 && 28 == $init->format('d')) {
        $init->modify('+1 day');
    }
    return $init;
}


/*
 * Función para validar números
 */
function validate_number($number) {
	$regex = '/^[0-9]+$/';
    if(preg_match($regex, $number)) return true;
    else return false;
}

/*
 * Función para validar fechas
 */
function comparar_fechas($primera, $segunda)
 {
  $valoresPrimera = explode ("/", $primera);
  $valoresSegunda = explode ("/", $segunda);
  $diaPrimera    = $valoresPrimera[0];
  $mesPrimera  = $valoresPrimera[1];
  $anyoPrimera   = $valoresPrimera[2];
  $diaSegunda   = $valoresSegunda[0];
  $mesSegunda = $valoresSegunda[1];
  $anyoSegunda  = $valoresSegunda[2];
  $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);
  $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);
  if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
    // "La fecha ".$primera." no es válida";
    return false;
  }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
    // "La fecha ".$segunda." no es válida";
    return false;
  }else{
    return  $diasPrimeraJuliano - $diasSegundaJuliano;
  }
}


?>
