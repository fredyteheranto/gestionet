﻿<?php 
	session_start();
	error_reporting(E_ALL & ~E_NOTICE);
	require_once 'inc/new_header.php';	
	
	$labels_revision=array("A" => "anual", "S" => "semestral");
	$labels_euribor=array("0" => "El mismo mes", "1" => "Un mes antes", "2" => "Dos meses antes", "3" => "Tres meses antes");
	$name_month = array('Todos', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
	$year_now = date("Y");
	$year_limit = 1999;

	//Valor por defecto
		$euribor_revision=1;
		$revision="A";
		$capital_inicial="";
		$dia_fecha_firma="";
		$mes_fecha_firma="";
		$ano_fecha_firma="";
		$anyos="";
		$meses="";
		$diferencial="";
		$dia_fecha_1rev="";
		$mes_fecha_1rev="";
		$ano_fecha_1rev="";
		$tipo_fijo_1p="";
		$clausula_suelo="";
		$capital_no_amortizado="";
		$importe_pagado_demas="";
		$total_perjuicio="";

	if(isset($_SESSION['valores'])):
	    foreach ($_SESSION['valores'] as $key => $valor):
	        $$key = $valor;
	    endforeach;
	endif;
?>	
	<div class="simulador">
		<div class="subheader">
			<h3>ADICAE GANA LA MACRODEMANDA DE CLÁUSULAS SUELO </h3>
			<span>Simulador de cuánto nos tienen que devolver.</span>
		</div>

		<form method="post" action="simulador-datos-pruebas.php?action=form&result=off&opcion=1">
			<input type="submit" value="DESDE EL PRINCIPIO DE LA HIPOTECA"/>
		</form>
	
		<form method="post" action="simulador-datos-pruebas.php?action=form&result=off&opcion=2">
			<input type="submit" value="DESDE EL 9 DE MAYO DE 2013"/>
		</form>
	</div>
<?php
	require_once 'inc/new_footer.php';
	
	if(isset($_SESSION['errores'])):
		unset($_SESSION['errores']);
	endif;
	if(isset($_SESSION['valores'])):
		unset($_SESSION['valores']);
	endif;
?>

<!--script type="text/javascript">
	<?php if($_GET['result']=="on"):?>
		setTimeout(function () {
			$('html, body').animate({scrollTop: $("select#euribor_revision").offset().top }, 1500);
		}, 500);
	<?php endif; ?>
	<?php if (isset($_SESSION['errores'])): ?>
		setTimeout(function () {
			$('html, body').animate({scrollTop: $("span.byline").offset().top }, 1500);
		}, 500);
	<?php endif;?>
	
	$(document).ready(function(){
		$('#btn_clean').bind('click', function() {
			$(':text').val('');
			$( "select" ).val('');
			$( "select#revision" ).val('A');
			$( "select#euribor_revision" ).val('1');
			$('span.alert').remove();
			$('.result').remove();
			$('html, body').animate({scrollTop:0 }, 1500);
		});
	});
</script-->
