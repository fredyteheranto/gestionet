﻿<?php 
session_start();
error_reporting(E_ALL & ~E_NOTICE);
require_once 'inc/new_header.php';	
?>	
<div class="mobile_noticias">
	<section>
			<h2>ADICAE GANA LA MACRODEMANDA DE CLÁUSULAS SUELO</h2>
			<span class="byline">Últimas noticias</span>

			<p><strong>14-07-2016</strong>&nbsp;&nbsp;Hoy tenemos un motivo más para reivindicar justicia y el cumplimiento de la legislación europea sobre cláusulas suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3255" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>07-07-2016</strong>&nbsp;&nbsp;13 de julio: Europa, ¿a favor de la devolución total de la cláusula suelo? <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3251" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>19-05-2016</strong>&nbsp;&nbsp;ADICAE recurre para ampliar la devolución de la cláusula suelo al principio de la hipoteca <a href="http://adicae.net/nota-de-prensa/1644.html" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>29-04-2016</strong>&nbsp;&nbsp;Nuevo tirón de orejas de Europa a España por las cláusulas suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3211" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>27-04-2016</strong>&nbsp;&nbsp;ADICAE moviliza a los afectados por las cláusulas suelo y exige en 20 ciudades la devolución de todo el dinero cobrado de más <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3206" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>26-04-2016</strong>&nbsp;&nbsp;La banca y el Gobierno dan la nota en Europa litigando contra el consumidor español en la batalla perdida de las cláusulas suelo <a href="http://adicae.net/nota-de-prensa/1593.html" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>25-04-2016</strong>&nbsp;&nbsp;ADICAE denuncia a 11 bancos por engañar a sus clientes ofreciendo acuerdos para perpetuar los ingresos ilegales de sus cláusulas suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3203" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>15-04-2016</strong>&nbsp;&nbsp;La devolución de todo el dinero pagado de más por las cláusulas suelo comienza a ser una opción real para el Tribunal Supremo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3194" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>14-04-2016</strong>&nbsp;&nbsp;Usted puede ser uno de los 2 millones de afectados por clásulas suelo: Infórmese sobre cómo eliminarla en el 900 80 10 76 <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3193" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>08-04-2016</strong>&nbsp;&nbsp;¿Qué hacer si tengo cláusula suelo? <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3188" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>07-04-2016</strong>&nbsp;&nbsp;ADICAE vence en la macrodemanda contra las cláusulas suelo: los bancos, condenados a eliminarlas <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3186" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>28-10-2015</strong>&nbsp;&nbsp;ADICAE recopila las propuestas más tramposas de la banca para negociar por la cláusula suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3031" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>13-10-2015</strong>&nbsp;&nbsp;La inminente sentencia de la macrodemanda de ADICAE extenderá la nulidad a todas las cláusulas suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=3009" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>24-06-2015</strong>&nbsp;&nbsp;La macrodemanda de ADICAE contra las cláusulas suelo queda vista para sentencia <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2917" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>18-06-2015</strong>&nbsp;&nbsp;ADICAE llama a los afectados por cláusulas suelo a acudir a la vista previa de la macrodemanda del 24 al 26 de junio <a href="http://laeconomiadelosconsumidores.adicae.net/?articulo=2908" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>29-05-2015</strong>&nbsp;&nbsp;La macrodemanda de cláusulas suelo ya tiene fecha para la vista previa: 24, 25 y 26 de junio <a href="http://usuariosdebancayseguros.adicae.net/?articulo=2890" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>15-04-2015</strong>&nbsp;&nbsp;La banca miente en sede judicial y asegura que los afectados por las cláusulas suelo sabían lo que firmaban <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2822" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>06-04-2015</strong>&nbsp;&nbsp;ADICAE llama a todos los afectados por cláusulas suelo a acudir al juicio de la demanda colectiva, del 15 al 17 de abril <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2802" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>01-03-2015</strong>&nbsp;&nbsp;El Supremo respalda la suspensión cautelar de los 'suelos' en la macrodemanda de ADICAE <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2746" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>15-11-2014</strong>&nbsp;&nbsp;ADICAE logra que un juzgado de Cáceres anule las cláusulas suelo que Caixa Catalunya pactó con la Junta de Extremadura en VPO <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2569" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>15-11-2014</strong>&nbsp;&nbsp;ADICAE resuelve las trabas de la banca y la burocracia judicial en la macrodemanda por cláusulas suelo, que sigue adelante <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2568" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>01-08-2014</strong>&nbsp;&nbsp;La macrodemanda de ADICAE contra las abusivas cláusulas suelo sigue adelante con una nueva jueza <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2344" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>01-06-2014</strong>&nbsp;&nbsp;ADICAE consigue que se condene a UNICAJA a devolver más de 30.000 euros cobrados a través de cláusulas suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2211" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>15-05-2014</strong>&nbsp;&nbsp;Un juez dictamina por primera vez que las cláusulas suelo son abusivas y condena a BBVA a devolver lo cobrado de más <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2193" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>01-05-2014</strong>&nbsp;&nbsp;ADICAE consigue que Banco Popular sea condenado a anular 54 cláusulas suelo en una demanda colectiva <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2148" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>15-03-2014</strong>&nbsp;&nbsp;55 demandas agrupadas de ADICAE contra las abusivas cláusulas suelo avanzan en los juzgados <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=2057" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>14-01-2014</strong>&nbsp;&nbsp;Engañosa campaña masiva de la banca para rebajar cláusulas suelo a cambio de retirar demandas <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1953" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>17-09-2013</strong>&nbsp;&nbsp;Los afectados por cláusulas suelo pisan el acelerador de la presión social y política con nuevas movilizaciones y propuestas <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1714" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>12-12-2013</strong>&nbsp;&nbsp;Nueva victoria de ADICAE: Ipar Kutxa, condenada a devolver el dinero de las cláusulas suelo a decenas de hipotecados <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1896" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>08-11-2013</strong>&nbsp;&nbsp;Ratificadas tres sentencias que obligan a Caja Extremadura, CajaMar y BBK Bank CajaSur a anular 48 cláusulas suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1841" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>22-10-2013</strong>&nbsp;&nbsp;Los afectados por cláusulas suelo salen a la calle a recordar al Supremo que sus derechos están por delante de los intereses de la banca <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1806" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>11-10-2013</strong>&nbsp;&nbsp;El Santander reconoce la abusividad de las cláusulas suelo de Banesto y las elimina pero ADICAE peleará para que los afectados recuperen lo que han pagado de más  <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1776" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>06-09-2013</strong>&nbsp;&nbsp;ADICAE logra que se devuelva el dinero cobrado de más por las cláusulas suelo en una sentencia que va más allá que el Supremo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1693" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>26-08-2013</strong>&nbsp;&nbsp;Los bancos reconocen menos de un tercio del coste que deberían asumir por retirar las cláusulas suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1675" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>23-08-2013</strong>&nbsp;&nbsp;Una sentencia ganada por ADICAE condena a Unicaja a anular 28 cláusulas suelo y a devolver a los afectados más de 225.000 euros <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1673" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>19-07-2013</strong>&nbsp;&nbsp;La presión social de los afectados por cláusulas suelo da sus frutos: la macrodemanda de ADICAE ya tiene nueva jueza <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1608" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>28-06-2013</strong>&nbsp;&nbsp;ADICAE logra que un juez de Bilbao condene a Ipar Kutxa a devolver lo cobrado de más por un grupo de cláusulas suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1567" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>17-06-2013</strong>&nbsp;&nbsp;Cajasur, condenada a eliminar varias cláusulas suelo a un grupo de consumidores defendidos por ADICAE <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1543" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>13-06-2013</strong>&nbsp;&nbsp;La aclaración del Supremo pone de manifiesto en sí misma la abusividad de la cláusula suelo al margen de la información facilitada, y expresa graves contradicciones en las consecuencias del fallo del Alto Tribunal <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1536" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>06-06-2013</strong>&nbsp;&nbsp;Una demanda de ADICAE a CajaMar logra la nulidad de la cláusula suelo y la devolución de las cantidades indebidamente pagadas por los consumidores <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1522" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>23-05-2013</strong>&nbsp;&nbsp;La lucha judicial de ADICAE y sus afectados contra las abusivas cláusulas suelo continúa firme en los juzgados le pese a quien le pese <a href="http://laeconomiadelosconsumidores.adicae.net/?articulo=1478" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>09-05-2013</strong>&nbsp;&nbsp;El Tribunal Supremo ha publicado la sentencia de cláusulas suelo. Esta sentencia se corresponde con el fallo que el Tribunal hizo público mediante comunicado de prensa el día 19 de marzo de 2013 pero sobre el cual todavía no se había dictado sentencia. <a href="http://adicae.net/nota-de-prensa/762.html" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>34-04-2013</strong>&nbsp;&nbsp;Las familias con cláusula suelo han pagado, de media, 11.200 euros más por su hipoteca desde 2009 <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1421" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>03-01-2013</strong>&nbsp;&nbsp;El juez condena a Banco Mare Nostrum a devolver con intereses lo que ganó de más con un colectivo de afectados por cláusulas suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1154" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>21-11-2012</strong>&nbsp;&nbsp;Ratificada la sentencia que obliga a Caja de Ahorros de Extremadura a eliminar ocho cláusulas suelo y devolver a los afectados lo cobrado de más <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1094" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>13-11-2012</strong>&nbsp;&nbsp;ADICAE consigue que Banco de Castilla La Mancha sea condenado a eliminar 27 cláusulas suelo y devolver el dinero cobrado de más <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1083" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>19-10-2012</strong>&nbsp;&nbsp;ADICAE consigue 2 sentencias en las que los jueces declaran abusivas más de 20 cláusulas suelo y exigen su eliminación inmediata <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=1050" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>19-10-2011</strong>&nbsp;&nbsp;ADICAE escenifica la lucha contra las cláusulas suelo <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=558" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>05-09-2011</strong>&nbsp;&nbsp;La macrodemanda colectiva de ADICAE contra las cláusulas suelo avanza y se amplía a un total de 101 bancos y cajas <a href="http://laeconomiadelosconsumidores.adicae.net/index.php?articulo=497" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>07-07-2011</strong>&nbsp;&nbsp;La verdadera cara de Banca Cívica: 182.441 cláusulas suelo a un tipo medio del 3.75% <a href="http://usuariosdebancayseguros.adicae.net/index.php?articulo=438" target="_blank">[ver&nbsp;+]</a></p>

			<p><strong>11-04-2011</strong>&nbsp;&nbsp;Una propuesta de ADICAE para la regulación de las cláusulas suelo llega al Congreso de los Diputados <a href="http://adicae.org:4040/noticia/705.html" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>27-02-2011</strong>&nbsp;&nbsp;La demanda colectiva de ADICAE contra las cláusulas suelo origina una revolución en las hipotecas en España <a href="http://adicae.org:4040/nota-de-prensa/445.html" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>25-02-2011</strong>&nbsp;&nbsp;ADICAE lleva el caso suelo hasta Consumers International (organismo mundial para la defensa del consumidor)<a href="http://es.consumersinternational.org/" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>24-02-2011</strong>&nbsp;&nbsp;La afluencia de consumidores para sumarse a la demanda colectiva de ADICAE destapa cómo se “colocaron” las cláusulas suelo en cientos de miles de hipotecas<a href="http://adicae.org:4040/nota-de-prensa/444.html" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>15-02-2011</strong>&nbsp;&nbsp;ADICAE amplía la demanda colectiva por cláusulas suelo a 53 bancos y cajas<a href="http://adicae.org:4040/nota-de-prensa/441.html" target="_blank">[ver&nbsp;+]</a></p>
			<p><strong>11-02-2011</strong>&nbsp;&nbsp;ADICAE sienta en el banquillo a 45 bancos y cajas por abusar de 4 millones de consumidores<a href="http://adicae.org:4040/nota-de-prensa/440.html" target="_blank">[ver&nbsp;+]</a></p>
	</section>
</div>
	<?php require_once 'inc/new_footer.php'; ?>