﻿<?php 
	session_start();
	error_reporting(E_ALL & ~E_NOTICE);
	
	if(empty($_GET['opcion'])){
		//redirecciono
	}
	
	require_once 'inc/new_header.php';	
	
	$labels_revision=array("A" => "anual", "S" => "semestral");
	$labels_euribor=array("0" => "El mismo mes", "1" => "Un mes antes", "2" => "Dos meses antes", "3" => "Tres meses antes");
	$name_month = array('Todos', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
	$year_now = date("Y");
	$year_limit = 1999;

	//Valor por defecto
		$euribor_revision=1;
		$revision="A";
		$capital_inicial="";
		$dia_fecha_firma="";
		$mes_fecha_firma="";
		$ano_fecha_firma="";
		$indice_referencia="euribor";
		$anyos="";
		$meses="";
		$diferencial="";
		$dia_fecha_1rev="";
		$mes_fecha_1rev="";
		$ano_fecha_1rev="";
		$tipo_fijo_1p="";
		$clausula_suelo="";
		$capital_no_amortizado="";
		$importe_pagado_demas="";
		$total_perjuicio="";

	if(isset($_SESSION['valores'])){
	    foreach ($_SESSION['valores'] as $key => $valor):
	        $$key = $valor;
	    endforeach;
	}
?>	
		<div class="simulador">
			<div class="subheader">
				<h2>ADICAE GANA LA MACRODEMANDA DE CLÁUSULAS SUELO</h2>
				<span>Simulador de cuánto nos tienen que devolver</span>
			</div>
                        
			<?php if ($_GET['opcion'] == '1') { ?>
				<h3>CALCULA CUÁNTO DEBES RECUPERAR DESDE EL PRINCIPIO DE LA HIPOTECA</h3>
				<p>Calcula con este sencillo simulador cuánto dinero te debe devolver tu banco por todo lo que has pagado por la cláusula suelo desde el principio de la hipoteca, tal como ha recurrido ADICAE y previsiblemente apoye la justicia europea. Introduce para ello los datos de tu hipoteca. El resultado puede ser aproximado. Reclama lo que es tuyo con ADICAE.</p>
			<?php }elseif($_GET['opcion'] == '2'){ ?>
				<h3>CALCULA CUÁNTO DEBES RECUPERAR DESDE EL 9 DE MAYO DE 2013</h3>
				<p>Calcula con este sencillo simulador cuánto dinero te debe devolver tu banco por todo lo que has pagado por la cláusula suelo desde el 9 de mayo de 2013, fecha que te ha reconocido la justicia, aunque ADICAE está trabajando para que también recuperes lo pagado anteriormente, es decir, desde el principio de la hipoteca. El resultado puede ser aproximado. Reclama lo que es tuyo con ADICAE.</p>
			<?php } ?>

			<form method="post" action="process/simulador-calcular-pruebas.php">
				<input type="hidden" name="opcion" id="opcion" value="<?php echo $_GET['opcion']; ?>">
				
				<?php if (isset($_SESSION['errores'])): ?>
					<span class="alert corner-all">
						<?php foreach ($_SESSION['errores'] as $id => $error): ?>
							<p><?php echo $error ?></p>
						<?php endforeach; ?>
					</span>
				<?php endif;?>

				<div class="form_block">
					<div class="form_label">
						<label for="capital_inicial">Importe de la hipoteca (€)</label>
					</div>
					<div class="form_input">
						<input type="text" name="capital_inicial" id="capital_inicial" value="<?php echo $capital_inicial; ?>"/> 
					</div>
				</div>

				<div class="form_block">
					<div class="form_label"><label for="fecha_firma">Fecha de la firma</label></div>

					<select name="dia_fecha_firma" id="dia_fecha_firma" class="fecha">
						<option value ="" >Día</option>
						<?php for($i = 1; $i <= 31; $i++): ?>
							<option value="<?php echo $i ?>" <?php echo $dia_fecha_firma == $i ? 'selected="selected"' : ''; ?>>
								<?php echo $i ?>
							</option>
						<?php endfor; ?>
					</select>
					
					<select name="mes_fecha_firma" id="mes_fecha_firma" class="fecha">
						<option value ="" >Mes</option>
						<?php for($i = 1; $i <= 12; $i++): ?>
							<option value="<?php echo $i ?>" <?php echo $mes_fecha_firma == $i ? 'selected="selected"' : ''; ?>>
								<?php echo $name_month[$i]; ?>
							</option>
						<?php endfor; ?>
					</select>
					
					<select name="ano_fecha_firma" id="ano_fecha_firma" class="fecha">
						<option value ="" >Año</option>
						<?php for($i = $year_now; $i >= $year_limit; $i--): ?>
							<option value="<?php echo $i ?>" <?php echo $ano_fecha_firma == $i ? 'selected="selected"' : ''; ?>>
								<?php echo $i ?>
							</option>
						<?php endfor; ?>
					</select>
				</div>

				<div class="form_block" style="clear:both;">
					<div class="form_label"><label for="duracion">Duración del préstamo</label></div>
					
					<div class="form_label_esp"><label for="anyos">Años:</label></div>
					<div class="form_input_esp">
						<input type="text" name="anyos" id="anyos" value="<?php echo $anyos; ?>"/> 
					</div>

					<div class="form_label_esp"><label for="meses">y meses:</label></div>
					<div class="form_input_esp">
						<input type="text" name="meses" id="meses" value="<?php echo $meses; ?>"/> 
					</div>
				</div>
				
				<div class="form_block">
					<div class="form_label"><label for="indice_referencia">&Iacute;ndice de referencia</label></div>
					<div class="form_input">
						<select name="indice_referencia" id="indice_referencia">
							<option value="euribor" <?php echo "euribor" == $indice_referencia ? 'selected="selected"' : '' ?>>Euribor</option>
							<option value="irph" <?php echo "irph" == $indice_referencia ? 'selected="selected"' : '' ?>>IRPH</option>
						</select>
					</div>
				</div>

				<div class="form_block">
					<div class="form_label"><label for="revision">Revisión</label></div>
					<div class="form_input">
						<select name="revision" id="revision">
							<?php foreach ($labels_revision as $key => $value): ?>
								<option value="<?php echo $key ?>" <?php echo $key == $revision ? 'selected="selected"' : '' ?>>
									<?php echo $value ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="form_block">
					<div class="form_label"><label for="diferencial">Diferencial (%)</label></div>
					<div class="form_input">
						<input type="text" name="diferencial" id="diferencial" value="<?php echo $diferencial; ?>"/> 
					</div>
				</div>

				<div class="form_block">
					<div class="form_label"><label for="tipo_fijo_1p">Tipo fijo del primer periodo (%)</label></div>
					<div class="form_input">
						<input type="text" name="tipo_fijo_1p" id="tipo_fijo_1p" value="<?php echo $tipo_fijo_1p; ?>"/> 
					</div>
				</div>

				<div class="form_block">
					<div class="form_label"><label for="fecha_1rev">Fecha fin del periodo a tipo fijo</label></div>

					<select name="dia_fecha_1rev" id="dia_fecha_1rev" class="fecha">
						<option value ="" >Día</option>
						<?php for($i = 1; $i <= 31; $i++): ?>
							<option value="<?php echo $i ?>" <?php echo $dia_fecha_1rev == $i ? 'selected="selected"' : ''; ?>><?php echo $i ?></option>
						<?php endfor; ?>
					</select>
					
					<select name="mes_fecha_1rev" id="mes_fecha_1rev" class="fecha">
						<option value ="" >Mes</option>
						<?php for($i = 1; $i <= 12; $i++): ?>
							<option value="<?php echo $i ?>" <?php echo $mes_fecha_1rev == $i ? 'selected="selected"' : ''; ?>><?php echo $name_month[$i]; ?></option>
						<?php endfor; ?>
					</select>
					
					<select name="ano_fecha_1rev" id="ano_fecha_1rev" class="fecha">
						<option value ="" >Año</option>
						<?php for($i = $year_now; $i >= $year_limit; $i--): ?>
							<option value="<?php echo $i ?>" <?php echo $ano_fecha_1rev == $i ? 'selected="selected"' : ''; ?>><?php echo $i ?></option>
						<?php endfor; ?>
					</select>
				</div>

				<div class="form_block" style="clear:both;">
					<div class="form_label"><label for="euribor_revision">Revisión del Euribor / IRPH</label></div>
					<div class="form_input">
						<select name="euribor_revision" id="euribor_revision">
							<?php foreach ($labels_euribor as $key => $value): ?>
								<option value="<?php echo $key ?>" <?php echo $key == $euribor_revision ? 'selected="selected"' : '' ?>><?php echo $value ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="form_block">
					<div class="form_label"><label for="clausula_suelo">Cláusula suelo (%)</label></div>
					<div class="form_input">
						<input type="text" name="clausula_suelo" id="clausula_suelo" value="<?php echo $clausula_suelo; ?>"/> 
					</div>
				</div>

				<input type="button" id="btn_clean" value=" Limpiar "/>
				<input type="submit" value="Calcular"/>
            </form>

			<?php if($_GET['result']=="on"){?>
			
				<?php if ($_GET['opcion'] == '1') { ?>
					<div class="form_block result">
						<div class="form_label">&nbsp;
						</div>
						<div class="form_input">
							<h3>DESDE EL PRINCIPIO DE LA HIPOTECA</h3> 
						</div>
					</div>
				<?php } ?>
				
				<?php if ($_GET['opcion'] == '2') { ?>
					<div class="form_block result">
						<div class="form_label">&nbsp;
						</div>
						<div class="form_input">
							<h3>DESDE EL 9 DE MAYO DE 2013</h3>
						</div>
					</div>
				<?php } ?>

				<div class="form_block result">
					<div class="form_label">
						<label for="capital_no_amortizado">Capital no amortizado (€)</label>
					</div>
					<div class="form_input">
						<input type="text" class="orange" name="capital_no_amortizado" id="capital_no_amortizado" size="18"
						value="<?php echo $capital_no_amortizado; ?>"/> 
					</div>
				</div>

				<div class="form_block result">
					<div class="form_label">
						<label for="importe_pagado_demas">Importe pagado de más<br> en las cuotas (€)</label>
					</div>
					<div class="form_input">
						<input type="text" class="orange" name="importe_pagado_demas" id="importe_pagado_demas" size="18"
						value="<?php echo $importe_pagado_demas; ?>"/> 
					</div>
				</div>

				<div class="form_block result">
					<div class="form_label">
						<label for="total_perjuicio">Total perjuicio (€)</label>
					</div>
					<div class="form_input">
						<input type="text" class="green" name="total_perjuicio" id="total_perjuicio" size="18"
						value="<?php echo $total_perjuicio; ?>"/> 
					</div>
				</div>
				
				<table style="text-align:center;" class="result">
					<tr>
						<th style="text-align:center;">A&ntilde;o</th>
						<th style="text-align:center;">Mes</th>
						<th style="text-align:center;">Cuota</th>
						<th style="text-align:center;">Interés</th>
						<th style="text-align:center;">Amortización</th>
						<th style="text-align:center;">Pendiente</th>
					<tr>
					<?php
						$arrTabla = $_SESSION["tabla_datos_meses"];
						if(is_array($arrTabla)){
							foreach($arrTabla as $anio=>$meses){
								if(is_array($meses)){
									foreach($meses as $mes=>$datos){
										?>
										<tr>
											<td style="text-align:center;"><?php echo($anio); ?></td>
											<td style="text-align:center;"><?php echo($mes); ?></td>
											<td style="text-align:center;"><?php echo($datos["cuota_C"]); ?></td>
											<td style="text-align:center;"><?php echo($datos["interes_C"]); ?></td>
											<td style="text-align:center;"><?php echo($datos["amortizacion_C"]); ?></td>
											<td style="text-align:center;"><?php echo($datos["capital_C"]); ?></td>
										<tr>
										<?php
									}
								}
							}
						}
					?>
				</table>

			<?php }?>
                    
			<div class="ayuda" style="clear:both;">
				<h4>¿Cómo funciona el simulador de cláusula suelo?</h4>
				<span>

					El simulador incluye casilleros para reflejar los parámetros clave de cualquier préstamo hipotecario*. Los datos que se han de incluir en la calculadora son:<br>
					
					<ul>
						<li><b>Importe de la hipoteca (€):</b> Capital inicial solicitado a la entidad financiera.</li>
						<li><b>Fecha de la firma:</b> Día en el que se formalizó el préstamo hipotecario.</li>
						<li><b>Diferencial (%):</b> Tipo de interés fijo que se suma al Euribor / IRPH **.</li>
						<li><b>Duración del préstamo:</b> Plazo de amortización pactado con la entidad financiera.</li>
						<li><b>Revisión (anual/semestral):</b> Periodicidad en la que se revisa el tipo de interés asignado al préstamo hipotecario.</li>
						<li><b>Tipo fijo del primer periodo:</b> En la mayoría de las hipotecas, se establece un tipo fijo durante el primer periodo del préstamo. En caso de no existir, deje en blanco esta casilla.</li>
						<li><b>Fecha fin del periodo a tipo fijo:</b> Fecha en la que finaliza el tipo fijo del primer periodo. Debe cumplimentar esta casilla en el caso de haber existido un tipo de interés fijo inicial. En caso contrario, déjela en blanco.</li>
						<li><b>Revisión del Euribor / IRPH:</b> (el mismo mes, un mes antes, dos meses antes, tres meses antes).</li>
						<li><b>Cláusula suelo:</b> Tipo de interés mínimo (fijo) marcado por su entidad financiera.
					</ul>
					
					<span class="mini">
						<br>Cálculos válidos a partir del 1 de enero de 1999<br>
						*     El método que se ha utlizado para la simulación es el sistema francés, aplicado al 90 % de las hipotecas en España.<br>
						**   Los cálculos se realizan en base al tipo de referencia del Euribor 12 meses o el IRPH Entidades<br>
					</span>
					
					<?php if ($_GET['result'] == 'on') { ?>
					
					<p>&nbsp;</p>
					<h4>Resultados</h4>
					<ul>
						<li><b>Importe pagado de más en las cuotas:</b> Cantidad total que se habría ahorrado en sus cuotas si su préstamo no tuviese cláusula suelo.</li>
						<li><b>Capital no amortizado:</b> Importe que habría podido amortizar si no le hubiesen aplicado la cláusula suelo.</li>
						<li><b>Perjuicio total:</b> Cuantía total que debería recuperar si eliminasen la cláusula suelo de su préstamo hipotecario***.</li>				
					</ul>

					</span>
					<span class="mini">
						***  A  esta cantidad debe sumar el tipo de interés legal del dinero (entre el 3% y el 4%).<br>
						La cantidad resultante es una estimación y solo una orientación general para calcular el perjuicio sufrido por la cláusula suelo. ADICAE no puede garantizar que el resultado sea exacto.
					</span>
				
					<?php } ?>
				
			</div>
		</div>
<?php	
	require_once 'inc/new_footer.php';
	
	if(isset($_SESSION['errores'])){
		unset($_SESSION['errores']);
	}
	if(isset($_SESSION['valores'])){
		unset($_SESSION['valores']);
	}	
	if(isset($_SESSION["tabla_datos_meses"])){
		unset($_SESSION["tabla_datos_meses"]);
	}
?>

<script type="text/javascript">
	<?php if($_GET['result']=="on"){?>
		setTimeout(function () {
			$('html, body').animate({scrollTop: $("select#euribor_revision").offset().top }, 1500);
		}, 500);
	<?php }?>
	
	<?php if (isset($_SESSION['errores'])): ?>
		setTimeout(function () {
			$('html, body').animate({scrollTop: $("span.byline").offset().top }, 1500);
		}, 500);
	<?php endif;?>
	
	$(document).ready(function(){

		$('#btn_clean').bind('click', function() {
			$(':text').val('');
			$( "select" ).val('');
			$( "select#revision" ).val('A');
			$( "select#euribor_revision" ).val('1');
			$('span.alert').remove();
			$('.result').remove();
			$('html, body').animate({scrollTop:0 }, 1500);
		});

	});
</script>
