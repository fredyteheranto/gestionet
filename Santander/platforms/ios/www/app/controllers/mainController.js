santanderApp.controller('mainController', ['$scope', '$rootScope', 'dataService','$location','$translate', '$route', function ($scope, $rootScope, dataService, $location, $translate, $route, $filter, datefilter) {
    
	var CONST_TIEMPO_CUENTAATRAS_PJ = 120;
	$rootScope.mostrarDiezMinAlerta = false;
	var mask = document.createElement("div");
    var activeNav;
    mask.className = "mask";
    mask.addEventListener("click", function(){
        $('body').removeClass(activeNav);
        activeNav = "";
        document.body.removeChild(mask);
        if($('.mostrarConstelacion').length){
            $('.constelacionAbierto').removeClass( "mostrarConstelacion" );
        }
    });
    $scope.openConst = function(){
        document.body.appendChild(mask);
        activeNav = "smr-open";
        $('.constelacionAbierto').addClass( "mostrarConstelacion" );
    };
    $scope.closeConst = function(){
        document.body.removeChild(mask);
        $('.constelacionAbierto').removeClass( "mostrarConstelacion" );

    };
	$rootScope.hideSidebar = function () {
		$(".smr-open").css('position', '');
		$('body').removeClass( "smr-open" );
		/*$('header').css('position', 'fixed');
		$('#sb-site').css('padding-top', '40px');*/
		document.body.removeChild(mask);
		activeNav = "";	
	};
	$rootScope.showSidebar = function () {
		$('body').addClass( "smr-open" );
		/*$('header').css('position', 'static');
		$('#sb-site').css('padding-top', '0px');*/
		$(".smr-open").css('position', 'relative');
		document.body.appendChild(mask);
		activeNav = "smr-open";	
	};
	$rootScope.isSidebarOpen = function () {
		return $("body").hasClass('smr-open');
	};
	
    $scope.toggleSidebar = function() {
        if ( $rootScope.isSidebarOpen() ){
			$rootScope.hideSidebar();
        }else{
			$rootScope.showSidebar();
			//console.log("mainController toggleSidebar " + $rootScope.tipoPartida);
			if ($rootScope.tipoPartida != 1)
			{
				$rootScope.getInfoMercadoMulti();
			}
        }

    };
    $scope.closeSidebar = function(bConcurso){
        if ($("body").hasClass('smr-open')){
            $(".smr-open").css('position', '');
            $('body').removeClass( "smr-open" );
            document.body.removeChild(mask);
            activeNav = "";
        }
		/*
		if (bConcurso)
			$rootScope.volverEmprende();
		*/
    };

    $scope.pasarJornada= function(){
        $scope.closeSidebar();
        dataService.getInfoMantenimiento($rootScope.ConfigOk).then(function (result){
            if(result.data.status[0].estado == 1){
                $location.path("/mantenimiento");
            }
        });
        swal({
                title: $translate.instant('Alertas.TituloPasoJornada'),
                text: $translate.instant('Alertas.PasoJornada'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#889641",
                confirmButtonText: $translate.instant('comunes.siMin'),
                cancelButtonText: $translate.instant('comunes.noMin'),
                closeOnConfirm: false
            },
            function(isConfirm)
            {
                if (isConfirm)
                {
					//window.plugins.socialsharing.share("compartir");
                    dataService.pasarJornada($rootScope.idMundoGeneral, $rootScope.momentoGeneral).then(function (result){
                        // $rootScope.momentoGeneral = result.data.momento;
                        // versión original con numero var numEmpresa = parseInt($rootScope.idEmpresaGeneral);
                        /*$rootScope.gameOverSingle = result.data.gameover[numEmpresa];
						if ($rootScope.gameOverSingle == true)
							$scope.puntuaciones = result.data.puntuaciones[numEmpresa];
						*/
						$rootScope.gameOverSingle = result.data.gameover[ $rootScope.idEmpresaGeneral ];
						if ($rootScope.gameOverSingle == true)
							$scope.puntuaciones = result.data.puntuaciones[ $rootScope.idEmpresaGeneral ];							
						$scope.jornadaPasada();
                    });

                }

            });
    };

	
	$scope.jornadaPasada = function(){
		
		window.localStorage.setItem("pasoenProgreso", false);
		$rootScope.horasAsignadasProyecto = false;
		$rootScope.prestamoUnoPorJornada = true;
	
		if ($location.path() == "/miEmpresa")
			$route.reload();
		else
			$location.path("/miEmpresa");
		console.log("jornada pasada: tipopartida " + $rootScope.tipoPartida + " gameover? " + $rootScope.gameOverSingle + " / " + $rootScope.gameOverMulti + " / " + $rootScope.gameOverConcurso);
		if ( (($rootScope.gameOverSingle == true) && ($rootScope.tipoPartida == 1) ) || (($rootScope.gameOverMulti == true) && ($rootScope.tipoPartida == 2) ) 
				|| (($rootScope.gameOverConcurso == true) && ($rootScope.tipoPartida == 3) )
				|| (($rootScope.gameOverConcurso == true) && ($rootScope.tipoPartida == 4) )
				){
			if($rootScope.tipoPartida == 1){
				window.localStorage.setItem("gameOverSingle", true)
				$rootScope.partidaTerminadaSingle = window.localStorage.getItem("gameOverSingle");
			} else if($rootScope.tipoPartida == 2){
				window.localStorage.setItem("gameOverMulti", true)
				$rootScope.partidaTerminadaMulti = window.localStorage.getItem("gameOverMulti");
			} else if($rootScope.tipoPartida == 3){
				window.localStorage.setItem("gameOverConcurso", true)
				$rootScope.partidaTerminadaConcurso = window.localStorage.getItem("gameOverConcurso");
			} else if($rootScope.tipoPartida == 4){
				window.localStorage.setItem("gameOverConcurso", true)
				$rootScope.partidaTerminadaConcurso = window.localStorage.getItem("gameOverConcurso");
			}
			
			 $scope.closeSidebar();
			// si ha terminado la partida ¿no mostramos la puntuación? está guardada en $scope.puntuaciones
			//social share
			//<button onclick="window.plugins.socialsharing.share('Message only')">message only</button>
			swal({
                title: $translate.instant('Alertas.TituloPartidaFinalizada'),
                text: $translate.instant('Alertas.PartidaFinalizada') + $translate.instant('Alertas.PartidaCompartirRRSS'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#889641",
                confirmButtonText: $translate.instant('comunes.siMin'),
                cancelButtonText: $translate.instant('comunes.noMin'),
                closeOnConfirm: true
            },
            function(isConfirm)
            {
                if (isConfirm)
                {
					compartirRRSS();
                }
				else
				{
					$location.path("/menuPrincipal");
					$scope.$apply();
				}
				
            });
		}
		else
		{
			swal($translate.instant('Alertas.PasoMesCorrectamente'), "", "success");
			//$rootScope.get_MomentoActualApp(); ya está la llamada en el controller de miEmpresa
		}
	};
	
	var compartirRRSS = function( ){
		var rutaImagen;
		if($rootScope.idioma== 'es'){
			rutaImagen = 'https://www.emprende-santander.com/front/images/mensaje_SimulacionEmprende.jpg'
		}else if($rootScope.idioma== 'en'){
			rutaImagen = 'https://www.emprende-santander.com/front/images/mensaje_SimulacionEmprende_2.jpg'
		}else if($rootScope.idioma== 'pt'){
			rutaImagen = 'https://www.emprende-santander.com/front/images/mensaje_SimulacionEmprende_3.jpg'
		}
		window.plugins.socialsharing.share(null, null, rutaImagen, null, compartirSuccessCallback, compartirErrorCallback);
	
	}
	
	var compartirSuccessCallback = function(){
		console.log ("compartido correctamente");
		$location.path("/menuPrincipal");
		$scope.$apply();
	}
	
	var compartirErrorCallback = function(){
		console.log ("Error compartiendo el mensaje");
		$location.path("/menuPrincipal");
		$scope.$apply();
	}
	
    $rootScope.devolverMesGrafica = function(momento, idEscenario){

        var meses = [""];
        var j;
        var i;
        if(idEscenario == '21' || idEscenario == '22' || idEscenario == '23') {
            j = 5;
            i=5;
        }
        else if(idEscenario == '31' || idEscenario == '32' || idEscenario == '33'){
            j = 11;
            i=11;
        }
        else {       
            j=0;
            i=0;
        }
        for(i; i<=momento; i++ ) {
            //fecha = $translate.instant('Fechas.Mes'+j);
			if ( ($rootScope.tipoPartida == 2) || ($rootScope.tipoPartida == 3) )
			{
				mes = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral + j;					
			}
			else
				mes = j;
			//fecha = $translate.instant('Fechas.Mes'+momento);
			fecha = $rootScope.devolverMes(mes);
			
            meses.push(fecha);
           /* if(fecha == "Diciembre") {
                j = 1;
            } else {
                j++;
            }*/
            j++;
        }
        return meses;

    };
	//filtros http://www.angularjshub.com/examples/collections/formattingfilters/
	// Utility functions
    $scope.formatDate1 = function (date, format) {
      return $filter("date")(date, format);
    };

    $scope.formatDate2 = function (date, format) {
      return dateFilter(date, format);
    };


    $rootScope.irBecas = function(){

		if (window.plugins.launcher)
		{
			console.log("window.plugins.launcher available");
			/* SOLUCION PARA ANDROID 
			//para lanzar la aplicación launch
			// para saber si esta o no instalada, canLaunch */
			if(device.platform == 'Android')
			{
				var successCallback = function() {
					// alert("Success!");
					window.plugins.launcher.launch({packageName:'com.cws.becassantander'});
				};
				var errorCallback = function() {
					//alert("Error! " + errMsg);
					window.location.href="market://details?id=com.cws.becassantander";

				};
				window.plugins.launcher.canLaunch({packageName:'com.cws.becassantander'}, successCallback, errorCallback);

			}
			else 
			{
				var successCallback = function() {
					window.location.href="becas://";
				};
				var errorCallback = function() {
					window.location.href="itms-apps://itunes.apple.com/app/id885218432";

				};
				window.plugins.launcher.canLaunch({uri:'becas://'}, successCallback, errorCallback);

			}		
		}
		else
		{
			console.log("window.plugins.launcher NOT available");
			/* SOLUCION PARA IOS */
			var scheme;
			var url;
        
			// Don't forget to add the org.apache.cordova.device plugin!
			if(device.platform === 'iOS') {
				scheme = 'becas://';
				url = "itms-apps://itunes.apple.com/app/id885218432";
			}
			else if(device.platform === 'Android') {
				scheme = 'com.cws.becassantander';
				url = "market://details?id=com.cws.becassantander";
			}
			
			appAvailability.check(
				scheme, // URI Scheme
				function() {  // Success callback
					console.log(scheme + ' is available :)');
					//window.location.href=scheme;
					window.location.href=url;
				},
				function() {  // Error callback
					console.log(scheme + ' is not available :(');				
					window.location.href=url;
				}
			);
		}

        

    };
    $rootScope.irFormacion = function(){
		
		if (window.plugins.launcher)
		{
			/* SOLUCION PARA ANDROID **/
			
			if(device.platform == 'Android'){
				var successCallback = function() {
					// alert("Success!");
					window.plugins.launcher.launch({packageName:'com.santander.appcourses'});
				};
				var errorCallback = function() {
					//alert("Error! " + errMsg);
					window.location.href="market://details?id=com.santander.appcourses";

				};
				window.plugins.launcher.canLaunch({packageName:'com.santander.appcourses'}, successCallback, errorCallback);

			}else {
				var successCallback = function() {
					window.location.href="appcourses://";
				};
				var errorCallback = function() {
				  window.location.href="itms-apps://itunes.apple.com/app/id969562859";
				};
				window.plugins.launcher.canLaunch({uri:'appcourses://'}, successCallback, errorCallback);
			}
		}
		else
		{
			/* SOLUCION PARA IOS */
		
			// AppAvailability Demo Code (Alert)
			var scheme;
			var url;
			
			// Don't forget to add the org.apache.cordova.device plugin!
			if(device.platform === 'iOS') {
				scheme = 'appcourses://';
				url="itms-apps://itunes.apple.com/app/id969562859";
				//window.location.href="appcourses://"
				//window.open('appcourses://', 'system');
			}
			else if(device.platform === 'Android') {
				scheme = 'com.santander.appcourses';
				url="market://details?id=com.santander.appcourses";
				
			}
			
			appAvailability.check(
				scheme, // URI Scheme
				function() {  // Success callback
					console.log(scheme + ' is available :)');
					//window.open('appcourses://', '_system');
					//window.location.href=scheme;
					window.location.href=url;
				},
				function() {  // Error callback
					console.log(scheme + ' is not available :(');
					window.location.href = url;					
					/*
					if(device.platform === 'iOS') 
					{
						window.location.href="itms-apps://itunes.apple.com/app/id969562859";
					}
					else
					{
						window.open('market://details?id=com.santander.appcourses', '_blank');
					}
					*/
				}
			);
		}
		
	};
	
	//solicitar info de mercado si partida multi y abrimos el menú lateral 
	 $rootScope.getInfoMercadoMulti = function(){
		console.log("mainController tipoPartida " + $rootScope.tipoPartida);
		dataService.getInfoMantenimiento().then(function (result){
            if(result.data.status[0].estado == 1){
                $location.path("/mantenimiento");
            }
			else
			{
				dataService.getInfoMercadoMulti($rootScope.idMundoGeneral, $rootScope.idEmpresaGeneral).then(function (result) {
					if (result.data.status == "success")
					{	
						if ($rootScope.tipoPartida == 2)
						{
							//multi
							gestionarLogicaMercadoMulti( result.data.datos );
						}
						else if (($rootScope.tipoPartida == 3) || ($rootScope.tipoPartida == 4))
						{
							//concurso
							gestionarLogicaMercadoConcurso( result.data.datos );
						}
					}
					else
						return false;
				});
			}
        });				
	}
	
	var gestionarLogicaMercadoMulti = function( datosServidor ) {
		
		//var datosServidor = result.data.datos;
		console.log (datosServidor);
		
		if ($rootScope.mundoMultiData == null)
		{
			// inicializar valores si estaban vacios
			$rootScope.mundoMultiData = datosServidor;
		}
		// hace falta guardar el estadoEmpresa, jornadaEmpresa, estadoMundo y jornadaMundo en el móvil/rootscope
		//para compararlo con los datos que vengan del servidor
		
		// COMPROBAR EL ESTADO DEL MERCADO				
		if ( datosServidor.mundo['Estado'] == 2)
		{
			 //ES TERMINADO --> HA ACABADO LA PARTIDA, AVISAR, MOSTRAR PUNTUACIÓN Y VOLVER AL MENU PRINCIPAL
			$rootScope.gameOverMulti = true;
			console.log("mainController getInfoMercadoMulti: HA ACABADO LA PARTIDA/MUNDO, AVISAR, MOSTRAR PUNTUACIÓN Y VOLVER AL MENU PRINCIPAL " + $rootScope.gameOverMulti);
			$scope.puntuaciones = datosServidor.empresa['puntuacion'];
			$scope.jornadaPasada();
		}
		// COMPROBAR SI EL ESTADO DE LA EMPRESA ES TERMINADO --> HA ACABADO LA PARTIDA AUNQUE CONTINUE EL MUNDO, AVISAR, MOSTRAR PUNTUACIÓN Y VOLVER AL MENU PRINCIPAL
		else if ( datosServidor.empresa['estado'] == 2)
		{
			 //ES TERMINADO --> HA ACABADO LA PARTIDA, AVISAR, MOSTRAR PUNTUACIÓN Y VOLVER AL MENU PRINCIPAL					
			$rootScope.gameOverMulti = true;
			console.log("mainController getInfoMercadoMulti: HA ACABADO LA PARTIDA/EMPRESA, AVISAR, MOSTRAR PUNTUACIÓN Y VOLVER AL MENU PRINCIPAL " + $rootScope.gameOverMulti);
			$scope.puntuacion = datosServidor.empresa['puntuacion'];
			$scope.jornadaPasada();
		}
		else if ( datosServidor.empresa['estado'] == 0 && window.localStorage.getItem("pasoenProgreso") != 'true')
		{
			// EL ESTADO DE LA EMPRESA ES INACTIVO --> AÚN NO HA EMPEZADO LA PARTIDA, AVISAR DE TIEMPO RESTANTE PARA PJ Y PREGUNTAR AL USUARIO 
			//		SI QUIERE COMENZAR YA, ACTIVAR EMPRESA EN EL MUNDO Y REDIRECCIONAR A PANTALLA "MI EMPRESA"
			//		SI PREFIERE ESPERAR, BLOQUEAR ACTIVIDAD Y REDIRECCIONAR A PANTALLA DE ESPERA CON TEXTO "ESPERANDO A PJ"
			console.log("mainController getInfoMercadoMulti: EL ESTADO DE LA EMPRESA ES INACTIVO --> AÚN NO HA EMPEZADO LA PARTIDA, AVISAR DE TIEMPO RESTANTE PARA PJ Y PREGUNTAR AL USUARIO ");
			var minutosPJ = parseInt (datosServidor.mundo['tiempoRestante']/ 60);
			var texto_A = $translate.instant('Alertas.multiAvisoInactivaPJInminente_01')  + minutosPJ + $translate.instant('Alertas.multiAvisoInactivaPJInminente_02');
			var texto_B = $translate.instant('Alertas.multiAvisoBienvenidaSinCompetencia');
			var texto_C = $translate.instant('Alertas.multiAvisoBienvenidaConCompetencia_01') + datosServidor.totalEmpresas + $translate.instant('Alertas.multiAvisoBienvenidaConCompetencia_02');
			var title_A = $translate.instant('Alertas.multiAvisoInactivaTitulo');
			var title_B = $translate.instant('Alertas.multiAvisoInactivaComienzaTitulo');
			var mostrarCancelar = false;
			var texto = texto_C;
			var titulo = title_B;
			var texto_aceptar = $translate.instant('comunes.aceptar');
			var texto_no =  $translate.instant('comunes.noMin');
			var texto_esperar = $translate.instant('Alertas.multiAvisosEsperar');
			var texto_empezar = $translate.instant('Alertas.multiAvisosEmpezar');
			var textoConfirmAlert = texto_aceptar;
			var textoNoAlert = texto_no;
			if ( CONST_TIEMPO_CUENTAATRAS_PJ < datosServidor.mundo['tiempoRestante'])
			{
				//activamos la empresa ya que falta tiempo suficiente para el pj
				console.log("activamos la empresa ya que falta tiempo suficiente para el pj");
				dataService.activarEmpresa(datosServidor.empresa['idEmpresa'], $rootScope.tipoPartida, datosServidor.mundo['idMundo']).then(function (result) {
					if (result.data.status == "success")
					{
						console.log("empresa activada, vamos a mi empresa");
						$location.path("/miEmpresa");
					}
				});
			}
			else
			{
				if ( CONST_TIEMPO_CUENTAATRAS_PJ > datosServidor.mundo['tiempoRestante'] && 0 < datosServidor.mundo['tiempoRestante'] )
				{
					mostrarCancelar = true;
					texto = texto_A;
					titulo = title_A;
					textoConfirmAlert = texto_empezar;
					textoNoAlert = texto_esperar;
				}
				else if (datosServidor.totalEmpresas == 0)
				{
					texto = texto_B;
					textoConfirmAlert = texto_aceptar;
					textoNoAlert = texto_no;
				}
				else
				{
					texto = texto_C;
					textoConfirmAlert = texto_aceptar;
					textoNoAlert = texto_no;
				}
				swal({
						title: titulo,
						text: texto,
						type: "warning",
						confirmButtonColor: "#889641",
						confirmButtonText: textoConfirmAlert,
						showCancelButton: mostrarCancelar,
						cancelButtonText: textoNoAlert
					},
					function(isConfirm)
					{
						// actualizar valores
						$rootScope.mundoMultiData = datosServidor;
						console.log(isConfirm);
						if ( $rootScope.isSidebarOpen() ){
							$rootScope.hideSidebar();
						}
						if (isConfirm)
						{
							//llamar al servidor para activar la empresa y redireccionar a menuprincipal
							dataService.activarEmpresa(datosServidor.empresa['idEmpresa'], $rootScope.tipoPartida, datosServidor.mundo['idMundo']).then(function (result) {
								if (result.data.status == "success")
								{
									console.log("empresa activada, vamos a mi empresa");
									$location.path("/miEmpresa");
								}
							});
						}
						else
						{
							console.log("a paso jornada");								
							//vamos a pantalla de pasoJornada o la recargamos
							$location.path("/pasoJornada/inactiva" );								
							$route.reload();
						}
					}
				);
			}
		}
		else if ( datosServidor.mundo['jornadaActual'] > $rootScope.mundoMultiData.mundo['jornadaActual']  )
		{
			// SI TIEMPO < 0 Y JORNADA DEL MUNDO ES > A LA GUARDADA EN LA APP --> AVISAR DE QUE SE HA REALIZADO EL PJ, ACTUALIZAR VARIABLES Y REDIRECCIONAR A PANTALLA "MI EMPRESA"
			console.log("jornada pasada");
			$scope.jornadaPasada();			
		}
		//Cuando el usuario entra a un mercado y quedan menos decisiones que las que quiere jugar para que se reinicie el mercado
		else if ((window.localStorage.getItem("menosDecionesMundoQueDecEmpresa") == 'true') && ((datosServidor.mundo['maxDecisionesMundo'] - datosServidor.mundo['jornadaActual']) < (datosServidor.mundo['maxDecisionesEmpresa']))
			&& window.localStorage.getItem("pasoenProgreso") != 'true'){
			console.log("menosDecionesMundoQueDecEmpresa");
				// actualizar valores
				$rootScope.mundoMultiData = datosServidor;
				var resultado = datosServidor.mundo['maxDecisionesMundo'] - datosServidor.mundo['jornadaActual'];
				var texto = $translate.instant('Alertas.multiMercadoReiniciarUno') + resultado + $translate.instant('Alertas.multiMercadoReiniciarDos');
				swal({
					title: '',
					text: texto,
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#889641",
					confirmButtonText: $translate.instant('comunes.siMin'),
					cancelButtonText: $translate.instant('comunes.noMin'),
				},
				function(isConfirm)
				{								
					if ( $rootScope.isSidebarOpen() ){
						$rootScope.hideSidebar();
					}
					if (isConfirm)
					{
						$location.path('/miEmpresa');
						$scope.$apply();
					}
					else
					{
						$location.path("/nuevaPartidaMulti");
						$scope.$apply();
					}
				}
				);
			window.localStorage.setItem("menosDecionesMundoQueDecEmpresa", false);
		}
		//Cuando queda una única decisión para que se reinicie el mercado
		else if ((window.localStorage.getItem("unaDecisionReinicieMundo") == 'true') && ((datosServidor.mundo['maxDecisionesMundo'] - datosServidor.mundo['jornadaActual']) == 1)
			&& window.localStorage.getItem("pasoenProgreso") != 'true'){
			console.log("unaDecisionReinicieMundo");
					// actualizar valores
					$rootScope.mundoMultiData = datosServidor;
					swal({
						title: '',
						text: $translate.instant('Alertas.multiUnaDecisionFinMercado'),
						type: "warning",
						confirmButtonColor: "#889641",
						confirmButtonText: $translate.instant('comunes.aceptar'),
					},
					function(isConfirm)
					{
													
						if ( $rootScope.isSidebarOpen() ){
							$rootScope.hideSidebar();
						}
						if (isConfirm)
						{
							$location.path('/miEmpresa');
							$scope.$apply();
						}
					}
					);
			window.localStorage.setItem("unaDecisionReinicieMundo", false);
		}
		else if ( CONST_TIEMPO_CUENTAATRAS_PJ > datosServidor.mundo['tiempoRestante'] )
		{
			// COMPROBAR TIEMPO RESTANTE PARA PASO JORNADA 
			// SI 0 < TIEMPO < 2 MINUTOS --> AVISAR DE QUE SE VA A PROCEDER AL PJ Y SE BLOQUEA LA ACTIVIDAD --> REDIRECCIONAR A PANTALLA DE ESPERA CON TEXTO "ESPERANDO A PJ"
			// SI TIEMPO < 0 Y JORNADA DEL MUNDO ES IGUAL A LA GUARDADA EN LA APP --> AVISAR PJ EN PROCESO, BLOQUEAR ACTIVIDAD Y REDIRECCIONAR A PANTALLA DE ESPERA CON TEXTO "PJ EN PROCESO"
			console.log("mainController getInfoMercadoMulti: COMPROBAR TIEMPO RESTANTE PARA PASO JORNADA "); 
			console.log("pasoenProgreso: " + window.localStorage.getItem("pasoenProgreso"));
			// actualizar valores
			$rootScope.mundoMultiData = datosServidor;
			if ( $rootScope.isSidebarOpen() ){
				$rootScope.hideSidebar();
			}
			if (window.localStorage.getItem("pasoenProgreso") != 'true' )
			{
				// Si aceptamos vamos al menu principal, sino, vamos a paso de jornada
				swal({
						title: '',
						text: $translate.instant('Alertas.multiAvisoDosMin'),
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#889641",
						confirmButtonText: $translate.instant('comunes.aceptar'),
						cancelButtonText: $translate.instant('comunes.noMin'),

					},
					function(isConfirm)
					{
						// actualizar valores
						$rootScope.mundoMultiData = datosServidor;
						console.log(isConfirm);
						if ( $rootScope.isSidebarOpen() ){
							$rootScope.hideSidebar();
						}
						if (isConfirm)
						{
							$location.path('/menuPrincipal');
							$scope.$apply();
							
						}
						else
						{
							$location.path("/pasoJornada");
							$scope.$apply();
						}
					}
				);
			}
			else
			{
				//recargamos la página de pasojornada en progreso
				$location.path("/pasoJornada");
				$route.reload();
			}
			
		}				
		else if(($rootScope.hayPartidaMulti == true) && (window.localStorage.getItem("entroPorMenu") == 'true')){
			$location.path("/miEmpresa");
			//$scope.$apply();
			window.localStorage.setItem("entroPorMenu", false);
		}
		//Cuando el usuario entra a cualquier mercado y quedan 10 minutos o menos de tiempo para tomar la decisión
		else if(datosServidor.mundo['tiempoRestante'] < 600)
		{
			// Solo lo muestro una vez la alerta durante el juego, o si sales y vuelves a entrar
			if ($rootScope.mostrarDiezMinAlerta == false) 
			{
				$rootScope.mostrarDiezMinAlerta = true;
				swal($translate.instant('Alertas.multiAvisoDiezMin'));
			}
		}
		// actualizar valores
		$rootScope.mundoMultiData = datosServidor;				
	}
	
	var gestionarLogicaMercadoConcurso = function( datosServidor ) {
		
		console.log (datosServidor);
		
		if ($rootScope.mundoMultiData == null)
		{
			// inicializar valores si estaban vacios
			$rootScope.mundoMultiData = datosServidor;
		}
		// hace falta guardar el estadoEmpresa, jornadaEmpresa, estadoMundo y jornadaMundo en el móvil/rootscope
		//para compararlo con los datos que vengan del servidor
		
		// COMPROBAR EL ESTADO DEL MERCADO				
		if ( datosServidor.mundo['Estado'] == 2)
		{
			 //ES TERMINADO --> HA ACABADO LA PARTIDA, AVISAR, MOSTRAR PUNTUACIÓN Y VOLVER AL MENU PRINCIPAL
			$rootScope.gameOverConcurso = true;
			console.log("mainController getInfoMercadoMulti: HA ACABADO LA PARTIDA/MUNDO, AVISAR, MOSTRAR PUNTUACIÓN Y VOLVER AL MENU PRINCIPAL " + $rootScope.gameOverConcurso);
			$scope.puntuaciones = datosServidor.empresa['puntuacion'];
			$scope.jornadaPasada();
		}
		// COMPROBAR SI EL ESTADO DE LA EMPRESA ES TERMINADO --> HA ACABADO LA PARTIDA AUNQUE CONTINUE EL MUNDO, AVISAR, MOSTRAR PUNTUACIÓN Y VOLVER AL MENU PRINCIPAL
		else if ( datosServidor.empresa['estado'] == 2)
		{
			 //ES TERMINADO --> HA ACABADO LA PARTIDA, AVISAR, MOSTRAR PUNTUACIÓN Y VOLVER AL MENU PRINCIPAL					
			$rootScope.gameOverConcurso = true;
			console.log("mainController getInfoMercadoMulti: HA ACABADO LA PARTIDA/EMPRESA, AVISAR, MOSTRAR PUNTUACIÓN Y VOLVER AL MENU PRINCIPAL " + $rootScope.gameOverConcurso);
			$scope.puntuacion = datosServidor.empresa['puntuacion'];
			$scope.jornadaPasada();
		}
		else if ( datosServidor.empresa['estado'] == 0 && window.localStorage.getItem("pasoenProgreso") != 'true')
		{
			// EL ESTADO DE LA EMPRESA ES INACTIVO --> AÚN NO HA EMPEZADO LA PARTIDA, AVISAR DE TIEMPO RESTANTE PARA PJ Y PREGUNTAR AL USUARIO 
			//		SI QUIERE COMENZAR YA, ACTIVAR EMPRESA EN EL MUNDO Y REDIRECCIONAR A PANTALLA "MI EMPRESA"
			//		SI PREFIERE ESPERAR, BLOQUEAR ACTIVIDAD Y REDIRECCIONAR A PANTALLA DE ESPERA CON TEXTO "ESPERANDO A PJ"
			console.log("mainController getInfoMercadoMulti: EL ESTADO DE LA EMPRESA ES INACTIVO --> AÚN NO HA EMPEZADO LA PARTIDA, AVISAR DE TIEMPO RESTANTE PARA PJ Y PREGUNTAR AL USUARIO ");
			var minutosPJ = parseInt (datosServidor.mundo['tiempoRestante']/ 60);
			var texto_A = $translate.instant('Alertas.multiAvisoInactivaPJInminente_01')  + minutosPJ + $translate.instant('Alertas.multiAvisoInactivaPJInminente_02');
			var texto_B = $translate.instant('Alertas.multiAvisoBienvenidaSinCompetencia');
			var texto_C = $translate.instant('Alertas.multiAvisoBienvenidaConCompetencia_01') + datosServidor.totalEmpresas + $translate.instant('Alertas.multiAvisoBienvenidaConCompetencia_02');
			var title_A = $translate.instant('Alertas.multiAvisoInactivaTitulo');
			var title_B = $translate.instant('Alertas.multiAvisoInactivaComienzaTitulo');
			var mostrarCancelar = false;
			var texto = texto_C;
			var titulo = title_B;
			var texto_aceptar = $translate.instant('comunes.aceptar');
			var texto_no =  $translate.instant('comunes.noMin');
			var texto_esperar = $translate.instant('Alertas.multiAvisosEsperar');
			var texto_empezar = $translate.instant('Alertas.multiAvisosEmpezar');
			var textoConfirmAlert = texto_aceptar;
			var textoNoAlert = texto_no;
			if ( CONST_TIEMPO_CUENTAATRAS_PJ < datosServidor.mundo['tiempoRestante'])
			{
				//activamos la empresa ya que falta tiempo suficiente para el pj
				console.log("activamos la empresa ya que falta tiempo suficiente para el pj");
				dataService.activarEmpresa(datosServidor.empresa['idEmpresa'], $rootScope.tipoPartida, datosServidor.mundo['idMundo']).then(function (result) {
					if (result.data.status == "success")
					{
						console.log("empresa activada, vamos a mi empresa");
						$location.path("/miEmpresa");
					}
				});
			}
			else
			{
				if ( CONST_TIEMPO_CUENTAATRAS_PJ > datosServidor.mundo['tiempoRestante'] && 0 < datosServidor.mundo['tiempoRestante'] )
				{
					mostrarCancelar = true;
					texto = texto_A;
					titulo = title_A;
					textoConfirmAlert = texto_empezar;
					textoNoAlert = texto_esperar;
				}
				else if (datosServidor.totalEmpresas == 0)
				{
					texto = texto_B;
					textoConfirmAlert = texto_aceptar;
					textoNoAlert = texto_no;
				}
				else
				{
					texto = texto_C;
					textoConfirmAlert = texto_aceptar;
					textoNoAlert = texto_no;
				}
				swal({
						title: titulo,
						text: texto,
						type: "warning",
						confirmButtonColor: "#889641",
						confirmButtonText: textoConfirmAlert,
						showCancelButton: mostrarCancelar,
						cancelButtonText: textoNoAlert
					},
					function(isConfirm)
					{
						// actualizar valores
						$rootScope.mundoMultiData = datosServidor;
						console.log(isConfirm);
						if ( $rootScope.isSidebarOpen() ){
							$rootScope.hideSidebar();
						}
						if (isConfirm)
						{
							//llamar al servidor para activar la empresa y redireccionar a menuprincipal
							dataService.activarEmpresa(datosServidor.empresa['idEmpresa'], $rootScope.tipoPartida, datosServidor.mundo['idMundo']).then(function (result) {
								if (result.data.status == "success")
								{
									console.log("empresa activada, vamos a mi empresa");
									$location.path("/miEmpresa");
								}
							});
						}
						else
						{
							console.log("a paso jornada");								
							//vamos a pantalla de pasoJornada o la recargamos
							$location.path("/pasoJornada/inactiva" );								
							$route.reload();
						}
					}
				);
			}
		}
		else if ( datosServidor.mundo['jornadaActual'] > $rootScope.mundoMultiData.mundo['jornadaActual']  )
		{
			// SI TIEMPO < 0 Y JORNADA DEL MUNDO ES > A LA GUARDADA EN LA APP --> AVISAR DE QUE SE HA REALIZADO EL PJ, ACTUALIZAR VARIABLES Y REDIRECCIONAR A PANTALLA "MI EMPRESA"
			console.log("jornada pasada");
			$scope.jornadaPasada();			
		}
		
		//Cuando queda una única decisión para que se reinicie el mercado
		else if ((window.localStorage.getItem("unaDecisionReinicieMundo") == 'true') && ((datosServidor.mundo['maxDecisionesMundo'] - datosServidor.mundo['jornadaActual']) == 1)
			&& window.localStorage.getItem("pasoenProgreso") != 'true'){
			console.log("unaDecisionReinicieMundo");
					// actualizar valores
					$rootScope.mundoMultiData = datosServidor;
					swal({
						title: '',
						text: $translate.instant('Alertas.multiUnaDecisionFinMercado'),
						type: "warning",
						confirmButtonColor: "#889641",
						confirmButtonText: $translate.instant('comunes.aceptar'),
					},
					function(isConfirm)
					{
						if ( $rootScope.isSidebarOpen() ){
							$rootScope.hideSidebar();
						}
						if (isConfirm)
						{
							$location.path('/miEmpresa');
							$scope.$apply();
						}
					}
					);
			window.localStorage.setItem("unaDecisionReinicieMundo", false);
		}
		else if ( CONST_TIEMPO_CUENTAATRAS_PJ > datosServidor.mundo['tiempoRestante'] )
		{
			// COMPROBAR TIEMPO RESTANTE PARA PASO JORNADA 
			// SI 0 < TIEMPO < 2 MINUTOS --> AVISAR DE QUE SE VA A PROCEDER AL PJ Y SE BLOQUEA LA ACTIVIDAD --> REDIRECCIONAR A PANTALLA DE ESPERA CON TEXTO "ESPERANDO A PJ"
			// SI TIEMPO < 0 Y JORNADA DEL MUNDO ES IGUAL A LA GUARDADA EN LA APP --> AVISAR PJ EN PROCESO, BLOQUEAR ACTIVIDAD Y REDIRECCIONAR A PANTALLA DE ESPERA CON TEXTO "PJ EN PROCESO"
			console.log("mainController getInfoMercadoMulti: COMPROBAR TIEMPO RESTANTE PARA PASO JORNADA "); 
			console.log("pasoenProgreso: " + window.localStorage.getItem("pasoenProgreso"));
			// actualizar valores
			$rootScope.mundoMultiData = datosServidor;
			if ( $rootScope.isSidebarOpen() ){
				$rootScope.hideSidebar();
			}
			if (window.localStorage.getItem("pasoenProgreso") != 'true' )
			{
				// Si aceptamos vamos al menu principal, sino, vamos a paso de jornada
				swal({
						title: '',
						text: $translate.instant('Alertas.multiAvisoDosMin'),
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#889641",
						confirmButtonText: $translate.instant('comunes.aceptar'),
						cancelButtonText: $translate.instant('comunes.noMin'),

					},
					function(isConfirm)
					{
						// actualizar valores
						$rootScope.mundoMultiData = datosServidor;
						console.log(isConfirm);
						if ( $rootScope.isSidebarOpen() ){
							$rootScope.hideSidebar();
						}
						if (isConfirm)
						{
							$location.path('/menuPrincipal');
							$scope.$apply();
							
						}
						else
						{
							$location.path("/pasoJornada");
							$scope.$apply();
						}
					}
				);
			}
			else
			{
				//recargamos la página de pasojornada en progreso
				$location.path("/pasoJornada");
				$route.reload();
			}
		}				
		else if(($rootScope.hayPartidaConcurso == true) && (window.localStorage.getItem("entroPorMenu") == 'true')){
			$location.path("/miEmpresa");
			window.localStorage.setItem("entroPorMenu", false);
		}
		//Cuando el usuario entra a cualquier mercado y quedan 10 minutos o menos de tiempo para tomar la decisión
		else if(datosServidor.mundo['tiempoRestante'] < 600)
		{
			// Solo lo muestro una vez la alerta durante el juego, o si sales y vuelves a entrar
			if ($rootScope.mostrarDiezMinAlerta == false) 
			{
				$rootScope.mostrarDiezMinAlerta = true;
				swal($translate.instant('Alertas.multiAvisoDiezMin'));
			}
		}
		// actualizar valores
		$rootScope.mundoMultiData = datosServidor;
	}
	
	
}]);