santanderApp.controller('mercadoController', ['$scope', 'dataService','$location','$rootScope', function ($scope, dataService,$location, $rootScope) {

	$scope.historicoVacio = false;

    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombreInfoMer/parrafo_unoInfoMer/parrafo_dosInfoMer');
    };
	
	var competenciaIPC = [];
	var competenciaMot = [];
	var competenciaRend = [];
	var competenciaDis = [];
	var competenciaPro = [];
	var competenciaProyectos =[];
	var competenciaPrecMed =[];
	var competenciaCalidad =[];
	var competenciaSolvencia =[];
	var competenciaLiquidez =[];
	
    dataService.getHistoricoMercado($rootScope.idEmpresaGeneral).then(function(result){
        $scope.infomercadoGraf = result.data.datos;
		console.log("getHistoricoMercado");
		console.log("motivacion");
		console.log(result.data.datos.motivacion);
		competenciaIPC = result.data.datos.ipc
		console.log("competenciaIPC");
		console.log(competenciaIPC);
		$scope.valorCompeIPC = competenciaIPC[$rootScope.momentoGeneral];
    });
	
	
	
		
	if ($rootScope.tipoPartida == 1)
	{
		//monojugador
		$scope.mercadoInfo = null;
		//competenciaMot = [7,7.5,7.5,7.5,8,7.75,7.5,8,8.25,8,8,8,8.25,8,8.25,8,7.5,7.75,7.25,7.75,8,8.25,8,7.5,7.75,7.75];
        competenciaMot = [7,8,8,8,8.5,9,9.5,9,9,9.25,9.5,9.5,9.5,9.5,9.5,9.25,9.25,9,8.75,8.75,8.5,8.5,8.75,8.75,8.25,7];
		$scope.valorCompeMot = competenciaMot[$rootScope.momentoGeneral];
		competenciaRend = [7,8,8,8,8.5,9,9.5,9,9,9.25,9.5,9.5,9.5,9.5,9.5,9.25,9.25,9,8.75,8.75,8.5,8.5,8.75,8.75,8.25,7];
		$scope.valorCompeRen = competenciaRend[$rootScope.momentoGeneral];
		
		competenciaDis =[6,5,5,5,4,5,5,5,5,4,4,4,4,5,5,5,5,6,6,6,6,6,6,6,6,6];
		$scope.varloComDisenio = competenciaDis[$rootScope.momentoGeneral];

		competenciaPro =[3,4,4,4,5,4,4,4,4,5,5,5,5,4,4,4,4,3,3,3,3,3,3,3,3,3];
		$scope.varloComPro = competenciaPro[$rootScope.momentoGeneral];

		competenciaProyectos =[10,11,11,11,14,14,16,16,16,19,19,20,21,21,21,25,25,25,25,25,28,28,28,28,28,29];
		$scope.varloComProyectos = competenciaProyectos[$rootScope.momentoGeneral];

		competenciaPrecMed =[6500,6750,6750,6750,6750,6750,7500,7500,7500,8750,8750,8750,8500,8500,8500,9000,9000,9000,9000,9000,9500,9500,9500,9500,9500,9250];
		$scope.varloComPrecMed = competenciaPrecMed[$rootScope.momentoGeneral];

		competenciaCalidad =[5,6,5,5.5,6,7,5.5,6.5,7.5,9.5,8.5,10,11,13,14,15,14,14.5,15.5,15.5,17,18,18,19,20.5,20.5];
		$scope.varloComCalidad = competenciaCalidad[$rootScope.momentoGeneral];

		competenciaSolvencia =[1.8,1.8,2,2,1.75,1.75,1.5,1.5,1.75,1.75,1.75,1.75,2,2,1.75,1.75,1.75,2,2,2,1.75,1.75,1.75,1.5,1.75,1.5];
		$scope.varloComSolvencia = competenciaSolvencia[$rootScope.momentoGeneral];

		competenciaLiquidez =[0.75,0.75,1,1,0.8,0.75,0.6,0.6,0.8,0.8,0.8,1,1,1,0.85,0.85,1,1,1,1,0.75,0.75,0.75,0.6,1,0.75];
		$scope.varloComLiquidez= competenciaLiquidez[$rootScope.momentoGeneral];
	}
	else 
	{
		dataService.getHistoricoCompetidores( $rootScope.idMundoGeneral, $rootScope.idEmpresaGeneral, $rootScope.momentoGeneral).then(function(result){
			if (result.data.status == "success")
			{
				console.log("datos competidores cargado " + $rootScope.mundoMultiData.mundo.jornadaActual );
				competenciaMot = result.data.datos['motivacion'];
                if($rootScope.mundoMultiData.mundo.jornadaActual < 2){
                    momento = 0;
                }else{
                    momento = $rootScope.mundoMultiData.mundo.jornadaActual-1; //2;    
                }
				$scope.valorCompeMot = competenciaMot[momento];
				
				competenciaRend = result.data.datos['rendimiento']; 
				$scope.valorCompeRen = competenciaRend[momento]; 
				
				competenciaDis = result.data.datos['disenyadores'];
				$scope.varloComDisenio = competenciaDis[momento]; 
				
				competenciaPro = result.data.datos['programadores'];
				$scope.varloComPro = competenciaPro[momento]; 
				
				competenciaProyectos = result.data.datos['proyectosEntregados'];
				$scope.varloComProyectos = competenciaProyectos[momento]; 
				
				competenciaPrecMed = result.data.datos['proyectosPrecioMedio'];
				$scope.varloComPrecMed = competenciaPrecMed[momento]; 
				
				competenciaCalidad = result.data.datos['calidad'];
				$scope.varloComCalidad = competenciaCalidad[momento]; 
				
				competenciaSolvencia = result.data.datos['solvencia'];
				$scope.varloComSolvencia = competenciaSolvencia[momento]; 
				
				competenciaLiquidez = result.data.datos['liquidez']
				$scope.varloComLiquidez= competenciaLiquidez[momento]; 
			}
		});
	}
	
	
	
	
    $scope.cargarGrafica= function(parametro, open){

        var labels;
		
		if (open)
			return;
        if($rootScope.idEscenario == '31' || $rootScope.idEscenario == '32' || $rootScope.idEscenario == '33'){
            labels = $rootScope.devolverMesGrafica($rootScope.momentoGeneral, $rootScope.idEscenario);
        }else{
            labels = $rootScope.devolverMesGrafica($rootScope.momentoGeneral-1, $rootScope.idEscenario);
        }
		//comprobar si hay datos
		//console.log("datos historicos: " + $scope.infomercadoGraf.motivacion.length);
		if ($scope.infomercadoGraf.motivacion.length == 0)
		{
			$scope.historicoVacio = true;
			return;
		}
		//console.log("mostrar gráficas: ");

        if (parametro == 'motivacion'){
			//var datosGraficaMot = angular.copy($scope.infomercadoGraf.motivacion);
			var datosGraficaMot = [];
			angular.forEach($scope.infomercadoGraf.motivacion, function(element) {
			  datosGraficaMot.push(element);
			});
            datosGraficaMot = $scope.convertirGraficaBase10(datosGraficaMot,  $scope.mercadoInfo.maxNivelMotivacion,  $scope.mercadoInfo.minNivelMotivacion);
			var datosCompetenciaMot = $scope.adecuarDatosCompetencia(datosGraficaMot, competenciaMot);
			datosCompetenciaMot = $scope.convertirGraficaBase10(datosCompetenciaMot,  $scope.mercadoInfo.maxNivelMotivacion,  $scope.mercadoInfo.minNivelMotivacion);
            if(datosGraficaMot.length != labels.length) {
                datosGraficaMot.unshift(0);
				datosCompetenciaMot.unshift(0);
            }
            grafica(datosGraficaMot, labels, datosCompetenciaMot, parametro)
        } else if (parametro == 'rendimiento') {
            //var datosGraficaRend = angular.copy($scope.infomercadoGraf.rendimiento);
			var datosGraficaRend = [];
			angular.forEach($scope.infomercadoGraf.rendimiento, function(element) {
			  datosGraficaRend.push(element);
			});
            datosGraficaRend = $scope.convertirGraficaBase10(datosGraficaRend,  $scope.mercadoInfo.maxNivelRendimiento,  $scope.mercadoInfo.minNivelRendimiento);
			competenciaRend = $scope.adecuarDatosCompetencia(datosGraficaRend, competenciaRend);
			competenciaRend = $scope.convertirGraficaBase10(competenciaRend,  $scope.mercadoInfo.maxNivelRendimiento,  $scope.mercadoInfo.minNivelRendimiento);
            if(datosGraficaRend.length != labels.length) {
                datosGraficaRend.unshift(0);
				competenciaRend.unshift(0);
            }
			grafica(datosGraficaRend, labels, competenciaRend, parametro)
        }else if (parametro == 'ipc') {
            //var datosGraficaIpc = $scope.infomercadoGraf.ipc;
			var datosGraficaIpc = [];
			competenciaIPC = [];
			angular.forEach($scope.infomercadoGraf.ipc, function(element) {
				competenciaIPC.push(parseFloat(element));
				datosGraficaIpc.push(parseFloat(element));
				//datosGraficaIpc.push(parseFloat(element) + Math.floor((Math.random() * 10) + 1));
			});
			//var labels = $rootScope.devolverMesGrafica($rootScope.momentoGeneral-1, $rootScope.idEscenario);
			competenciaIPC = $scope.adecuarDatosCompetencia(datosGraficaIpc, competenciaIPC);
            if(datosGraficaIpc.length != labels.length) {
                datosGraficaIpc.unshift(0);
				competenciaIPC.unshift(0);
            }
            grafica(datosGraficaIpc, labels, competenciaIPC, parametro)
        }
    }
    dataService.getInfoMercado($rootScope.idEmpresaGeneral,$rootScope.momentoGeneral).then(function (result) {
		console.log("getInfoMercado");
        $scope.mercadoInfo = result.data.datos;

    });
	$scope.adecuarDatosCompetencia = function (datosEmpresa, datosCompetencia) {
        var datosCompetenciaAdecuados = angular.copy(datosEmpresa);
		var index = 0;
		var sizeCompetencia = datosCompetencia.length;
		var sizeEmpresa = datosEmpresa.length;
        angular.forEach(datosEmpresa, function(dato)
        {
            datosCompetenciaAdecuados[index] = datosCompetencia[sizeCompetencia-sizeEmpresa+index];
            index++;
        });
        //console.log("Valor datos");
        return datosCompetenciaAdecuados;
    };
    $scope.convertirGraficaBase10 = function (datos, max, min) {
        var index = 0;
        angular.forEach(datos, function(dato)
        {
            datos[index] = $scope.convertirBase10(dato, max, min);
            index++;
        });
        //console.log("Valor datos");
        return datos;
    };
	$scope.convertirBase10 = function (datoBase, max, min) {
		var valorAdecuado = 0;
		if (datoBase != null)
		{
			longitud = max - min;
			//console.log("longitud : " + longitud);
			//console.log("datoBase : " + datoBase);
			if (datoBase < min)
				datoBase = min;
			valor = datoBase - min;
			//console.log("valor : " + valor);
			valorAdecuado = valor * 10 / longitud;
			valorAdecuado = parseFloat(valorAdecuado).toFixed(2);
			//console.log("valorAdecuado : " + valorAdecuado);			
		}
		return valorAdecuado;		
    };

    var cargaGrafica = function(datos, labels, competencia, tipo){
        var ctx;
        if(tipo == 'motivacion') {
            ctx = document.getElementById("chartLine").getContext("2d");
        } else if(tipo == 'rendimiento') {
            ctx = document.getElementById("chartLine1").getContext("2d");
        }else if(tipo == 'ipc') {
            ctx = document.getElementById("chartLine2").getContext("2d");
        }

        var data = {
            labels:  labels,
            datasets: [
                {
                    label: "mixta",
                    fillColor: "rgba(182,0,184,0)",
                    strokeColor: "#6C354E",
                    pointColor: "#6C354E",
                    pointStrokeColor: "#6C354E",
                    pointHighlightFill: "#6C354E",
                    pointHighlightStroke: "#6C354E",
                    data: datos
                },
                {
                    label: "variable",
                    fillColor: "rgba(3,151,20,0)",
                    strokeColor: "#D0A426",
                    pointColor: "#D0A426",
                    pointStrokeColor: "#D0A426",
                    pointHighlightFill: "#D0A426",
                    pointHighlightStroke: "#D0A426",
                    data: competencia
                }
            ]
        };
        new Chart(ctx).Line(data, {
            showTooltips: true,
			animation: true,
			scaleFontSize: 10,
			
        });

    }
    var grafica = function(datos, labels, competencia, tipo){
        if(tipo == 'motivacion') {
            $("#chartLine").remove();
            $(".capaCanvas").append("<canvas id='chartLine' class='grafifasAncho' ></canvas>");
        } else if(tipo == 'rendimiento') {
            $("#chartLine1").remove();
            $(".capaCanvas1").append("<canvas id='chartLine1' class='grafifasAncho'></canvas>");
        }else if(tipo == 'ipc') {
            $("#chartLine2").remove();
            $(".capaCanvas2").append("<canvas id='chartLine2' class='grafifasAncho'></canvas>");
			
        }
        cargaGrafica(datos, labels, competencia, tipo);
    }
	//$scope.convertirMotivacionBase10();

}]);
