
santanderApp.controller('registroController', ['$scope','dataService','storageService','$location','$rootScope', '$translate', function ($scope,dataService,storageService,$location,$rootScope, $translate) {
    //var passOK;

    $scope.formSubmitted = false;
    $scope.formRegistro;
    $scope.nombre;
    $scope.apellidos;
    $scope.email;
    $scope.emailRepite;
    $scope.pais;
    $scope.passwordRe;
    $scope.confirmPassword;
    $scope.nick;
	$scope.paises = [];
    

    /* comprobar si existe sesi�n en local */
	//console.log("registroController localstorage /" + window.localStorage.getItem("loggedInUserId")+"/");

    $scope.registro = function () {
        $scope.formSubmitted = true;
        $scope.emailOK = ($scope.email == $scope.emailRepite);
        $scope.passOK = ($scope.passwordRe == $scope.confirmPassword);

        if($scope.formRegistro.$valid && $scope.passOK && $scope.emailOK) {
            dataService.registro($scope.nick, $scope.nombre, $scope.apellidos, $scope.email, $scope.pais, $scope.passwordRe, $rootScope.idConcurso).then(function (result){
            var userId = result.data.userId;
			var reserva = result.data.reserva;
            console.log("REGISTRO RETURN");
			console.log(result);
			
            if(result.data.mensaje == 'MSG_REGISTRY_NICK_DUPLICATED') {
                swal({
                    title:'',
                    text: $translate.instant('Login.nickRepetido'),
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#889641",
                    confirmButtonText: $translate.instant('comunes.aceptar'),
                });
            }else if(result.data.mensaje == 'MSG_REGISTRY_EMAIL_DUPLICATED'){
                swal({
                    title:'',
                    text: $translate.instant('Login.emailRepetido'),
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#889641",
                    confirmButtonText: $translate.instant('comunes.aceptar'),
                });
            }
            else{
				var texto = $translate.instant('Login.registroCorrecto');
				/*if (reserva == true)
					texto += '.' + $translate.instant('Alertas.concursoInscripcionMaxParticipantesAlcanzado');*/
                  swal({
                    title:'',
                    text: texto,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#889641",
                    confirmButtonText: $translate.instant('comunes.aceptar'),
                },
                function(isConfirm)
                {
                    if (isConfirm)
                    {
						storageService.setData("loggedInUserIdConcurso", userId); //window.localStorage.setItem("loggedInUserIdConcurso", userId);
						$rootScope.loggedInUserIdConcurso = userId;
						console.log("REGISTRO realizado con éxito" + $rootScope.loggedInUserIdConcurso);
                        $location.path("/concurso");
                        $scope.$apply();
                     }
                 });
            }

            
          
           
         });
            
        }

       
    };

    var getPaisesConcurso = function(){
        dataService.getPaisesConcurso().then(function (result){
			//$scope.paises = result.data.datos;
			var pais = null;
			/*
			for (var i=0; i < result.data.datos.length; i++)
			{
				pais = result.data.datos[i];
				if (pais.habilitado == '1')
					$scope.paises.push(pais);
			}
			*/
			 angular.forEach(result.data.datos, function(pais) {
				
				//console.log(pais.habilitado);
				if (pais.habilitado == '1')
					$scope.paises.push(pais);
			});
			//console.log($scope.paises);
        });
    }
    getPaisesConcurso();
}]);