cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "cordova-plugin-secure-storage.SecureStorage",
        "file": "plugins/cordova-plugin-secure-storage/www/securestorage.js",
        "pluginId": "cordova-plugin-secure-storage",
        "clobbers": [
            "SecureStorage"
        ]
    },
    {
        "id": "cordova-plugin-secure-storage.sjcl_ss",
        "file": "plugins/cordova-plugin-secure-storage/www/sjcl_ss.js",
        "pluginId": "cordova-plugin-secure-storage",
        "runs": true
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-secure-storage": "2.1.0"
};
// BOTTOM OF METADATA
});