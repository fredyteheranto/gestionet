cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-secure-storage/www/securestorage.js",
        "id": "cordova-plugin-secure-storage.SecureStorage",
        "pluginId": "cordova-plugin-secure-storage",
        "clobbers": [
            "SecureStorage"
        ]
    },
    {
        "file": "plugins/cordova-plugin-secure-storage/www/sjcl_ss.js",
        "id": "cordova-plugin-secure-storage.sjcl_ss",
        "pluginId": "cordova-plugin-secure-storage",
        "runs": true
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-secure-storage": "2.1.0"
}
// BOTTOM OF METADATA
});