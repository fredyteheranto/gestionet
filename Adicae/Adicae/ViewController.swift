//
//  ViewController.swift
//  Adicaw
//
//  Created by Fredy Saul Teheran Tovar on 29/11/16.
//  Copyright © 2016 Fredy Saul Teheran Tovar. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate {
    let  wv = WKWebView(frame: UIScreen.main.bounds)
    let viewportScriptString = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); meta.setAttribute('initial-scale', '1.0'); meta.setAttribute('maximum-scale', '1.0'); meta.setAttribute('minimum-scale', '1.0'); meta.setAttribute('user-scalable', 'no'); document.getElementsByTagName('head')[0].appendChild(meta);"

    override func viewDidLoad() {
        super.viewDidLoad()
        _ = WKUserScript(source: viewportScriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        guard let url =  NSURL(string: "http://afectadosclausulasuelo.org/mobile/") else { return }
        wv.navigationDelegate = self
        self.navigationController?.hidesBarsOnSwipe = true
        wv.load(NSURLRequest(url: url as URL) as URLRequest)
        view.addSubview(wv)
        wv.scrollView.isScrollEnabled = true
        wv.allowsBackForwardNavigationGestures = false
        wv.contentMode = .scaleToFill
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return scrollView
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated  {
            if let newURL = navigationAction.request.url,
                // let host = newURL.host , !host.hasPrefix //host.contains
                let host = newURL.host, !host.hasPrefix("afectadosclausulasuelo.org") &&
                UIApplication.shared.canOpenURL(newURL) &&
                UIApplication.shared.openURL(newURL) {
                print(newURL)
               // print("Redirected to browser. No need to open it locally")
                decisionHandler(.cancel)
            } else {
               // print("Open it locally")
                decisionHandler(.allow)
            }
        } else {
            //print("not a user click")
            decisionHandler(.allow)
        }
    }
}
