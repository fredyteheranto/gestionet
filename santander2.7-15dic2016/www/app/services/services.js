'use strict';
/*
angular.module('services', ['ngResource'], function ($provide) {

    $provide.factory('Notes', function ($resource) {
        return $resource('/rest/notes', {}, {
            get: { method: 'GET', isArray: false, crypt: true},
            save: { method: 'POST', crypt: true}            
        });
    });
});
*/

angular.module('services', ['ngResource'], function ($provide) {
    $provide.factory('Data', function ($resource) {
        return $resource('/assets/config', {}, {
            query: {method: 'GET', isArray: false, crypt: true},
            queryNoCrypt: {method: 'GET'},
            save: {method: 'POST', params: {}, crypt: true},
            saveNoCrypt: {method: 'POST', params: {}}
        });
    });
});