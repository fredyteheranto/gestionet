santanderApp.controller('publicidadController', ['$scope', 'dataService','$location','$rootScope','$translate', function ($scope, dataService,$location,$rootScope,$translate) {

    $scope.idInversion;
    $scope.cobertura;

    $scope.pantallaInformacion = function(){
        $location.path('informacionPublicidad');
    };

    $scope.obtenerCoberturas = function() {
        dataService.obtenerCoberturas($rootScope.idEmpresaGeneral, $rootScope.momentoGeneral).then(function (result) {
            $scope.publicidadInfoLocalEspecializado = result.data.datos.medios[0];
            $scope.publicidadInfoLocalMedioTv = result.data.datos.medios[1];
            $scope.publicidadInfoLocalMedioRadio = result.data.datos.medios[2];
            $scope.publicidadInfoLocalMedioInternet = result.data.datos.medios[3];
            $scope.publicidadInfoNacionalEspecializado = result.data.datos.medios[4];
            $scope.publicidadInfoNacionalMedioTv = result.data.datos.medios[5];
            $scope.publicidadInfoNacionalMedioInternet = result.data.datos.medios[6];
            $scope.publicidadInfoInterNacionalMedioInternet = result.data.datos.medios[7];

            $scope.mesp = parseFloat(result.data.datos.medios[0].cobertura);
			$scope.mespConvertido = $rootScope.convertirMonedaBaseActual($scope.mesp);
            $scope.tv = parseFloat(result.data.datos.medios[1].cobertura);
			$scope.tvConvertido = $rootScope.convertirMonedaBaseActual($scope.tv);
            $scope.radioL = parseFloat(result.data.datos.medios[2].cobertura);
            $scope.radioLConvertido = $rootScope.convertirMonedaBaseActual($scope.radioL);
			$scope.internet = parseFloat(result.data.datos.medios[3].cobertura);
            $scope.internetConvertido = $rootScope.convertirMonedaBaseActual($scope.internet);
			$scope.mespNac = parseFloat(result.data.datos.medios[4].cobertura);
            $scope.mespNacConvertido = $rootScope.convertirMonedaBaseActual($scope.mespNac);
			$scope.internetNac = parseFloat(result.data.datos.medios[6].cobertura);
			$scope.internetNacConvertido = $rootScope.convertirMonedaBaseActual($scope.internetNac);
            $scope.tvNac = parseFloat(result.data.datos.medios[5].cobertura);
			$scope.tvNacConvertido = $rootScope.convertirMonedaBaseActual($scope.tvNac);
            $scope.internetInter = parseFloat(result.data.datos.medios[7].cobertura);
			$scope.internetInterConvertido = $rootScope.convertirMonedaBaseActual($scope.internetInter);
			
			$scope.coberturaTotal = $scope.mesp + $scope.internet + $scope.radioL + $scope.tv + $scope.mespNac + $scope.internetNac + $scope.tvNac + $scope.internetInter;
			
			
			//copiado de cargarGraficaPublicidad, que se repet�a la llamada
			var graficaLocal = result.data.datos.mercados[1].coberturaEmpresa;
            var graficaNacional = result.data.datos.mercados[2].coberturaEmpresa;
            var graficaInternacional = result.data.datos.mercados[3].coberturaEmpresa;

            $("#chart").remove();
            $(".capaCanvas1").append("<canvas id='chart'' width='100'' height='100'></canvas>");

            $("#chart1").remove();
            $(".capaCanvas2").append("<canvas id='chart1'' width='100'' height='100'></canvas>");

            $("#chart2").remove();
            $(".capaCanvas3").append("<canvas id='chart2'' width='100'' height='100'></canvas>");

            cargarGrafica(graficaLocal, graficaNacional, graficaInternacional);
			
        });
    };

    $scope.cargarGraficaPublicidad = function(){
        dataService.obtenerCoberturas($rootScope.idEmpresaGeneral, $rootScope.momentoGeneral).then(function (result) {
            var graficaLocal = result.data.datos.mercados[1].coberturaEmpresa;
            var graficaNacional = result.data.datos.mercados[2].coberturaEmpresa;
            var graficaInternacional = result.data.datos.mercados[3].coberturaEmpresa;

            $("#chart").remove();
            $(".capaCanvas1").append("<canvas id='chart'' width='100'' height='100'></canvas>");

            $("#chart1").remove();
            $(".capaCanvas2").append("<canvas id='chart1'' width='100'' height='100'></canvas>");

            $("#chart2").remove();
            $(".capaCanvas3").append("<canvas id='chart2'' width='100'' height='100'></canvas>");

            cargarGrafica(graficaLocal, graficaNacional, graficaInternacional);
        });
    };

    $scope.actualizarCoberturaLocal = function(mesp, radioL,internet, tv, IdVersionLocalEsp, IdVersionLocalRad,  IdVersionLocalInter,IdVersionLocalTv) {
		
		bDatosCorrectos = true;
        $scope.errorPublicidad="";
		
		if ( (mesp < 0) || (radioL < 0) || (internet < 0) || (tv < 0) )
		{
			bdatosCorrectos = false;
			$scope.errorPublicidad = $translate.instant('Ups.errorPublicidadHorasNeg');
			$rootScope.mostrarUps = true;
		}
		else
		{
             //var data = {id:0, nombre:""};
              $scope.medios = [
                {
                    "idInversion": IdVersionLocalEsp,
                    "cobertura": mesp
                },
                {
                   "idInversion": IdVersionLocalRad,
                    "cobertura": radioL
                },
                {
                   "idInversion": IdVersionLocalInter,
                    "cobertura": internet
                },
                {
                   "idInversion": IdVersionLocalTv,
                    "cobertura": tv
                }
            ];
			$scope.datos = JSON.stringify($scope.medios);
			actualizarCobertura($scope.datos);
			//actualizarCobertura(IdVersionLocalInter, internet);
			//actualizarCobertura(IdVersionLocalRad, radioL);
			//actualizarCobertura(IdVersionLocalTv, tv);
			swal($translate.instant('Alertas.operacionCorrecta'));
		}
    };

    $scope.actualizarCoberturaNacional = function(mesp, internet, tv, IdVersionNacEsp, IdVersionNacInter, IdVersionNacTv) {
		
		bDatosCorrectos = true;
        $scope.errorPublicidad="";
		if ( (mesp < 0) || (internet < 0) || (tv < 0) )
		{
			bdatosCorrectos = false;
			$scope.errorPublicidad = $translate.instant('Ups.errorPublicidadHorasNeg');
			$rootScope.mostrarUps = true;
		}
		else
		{
			//actualizarCobertura(IdVersionNacEsp, mesp);
			//actualizarCobertura(IdVersionNacInter, internet);
			//actualizarCobertura(IdVersionNacTv,tv );
			$scope.medios = [
                {
                    "idInversion": IdVersionNacEsp,
                    "cobertura": mesp
                },
                {
                   "idInversion": IdVersionNacInter,
                    "cobertura": internet
                },
                {
                   "idInversion": IdVersionNacTv,
                    "cobertura": tv
                }
            ];
			$scope.datos = JSON.stringify($scope.medios);
			actualizarCobertura($scope.datos);
		}
    };

    $scope.actualizarCoberturaInternacional = function(internet, IdVersionInterNacInter) {
		
		bDatosCorrectos = true;
        $scope.errorPublicidad="";
		if ( (internet < 0) )
		{
			bdatosCorrectos = false;
			$scope.errorPublicidad = $translate.instant('Ups.errorPublicidadHorasNeg');
			$rootScope.mostrarUps = true;
		}
		else
		{
			//actualizarCobertura(IdVersionInterNacInter, internet);
			$scope.medios = [
                {
                   "idInversion": IdVersionInterNacInter,
                    "cobertura": internet
                }
            ];
			$scope.datos = JSON.stringify($scope.medios);
			actualizarCobertura($scope.datos);
		}
    };

    var actualizarCobertura= function (medios){
        dataService.actualizarCobertura(medios).then(function (result) {
            if(result.data.status == "success"){
                $scope.obtenerCoberturas();
                //$scope.cargarGraficaPublicidad();
            }
        });
    };

    $scope.obtenerCoberturas();
    //$scope.cargarGraficaPublicidad();

    var cargarGrafica = function(graficaLocal, graficaNacional, graficaInternacional){
        var restaLocal = 100 - graficaLocal;
        var restaNacional = 100 - graficaNacional;
        var restaInternacional = 100 - graficaInternacional;

        //Chart1
        var ctx = document.getElementById("chart").getContext("2d");

        var data = [
            {
                value: parseInt(graficaLocal),
                color: "#2B7C96",
                label: "COBERTURA LOCAL"
            },
            {
                value: parseInt(restaLocal),
                color: "#D8D8D8",
                label: "Porcentaje"
            }
        ]

        new Chart(ctx).Pie(data, {
            showTooltips: false
        });


        //Chart2
        var ctx1 = document.getElementById("chart1").getContext("2d");

        var data = [
            {
                value: parseInt(graficaNacional),
                color:"#D0A426",
                label: "COBERTURA NACIONAL"
            },
            {
                value: parseInt(restaNacional),
                color: "#D8D8D8",
                label: "Porcentaje"
            }
        ]

        new Chart(ctx1).Pie(data, {
            showTooltips: false
        });

        //Chart3
        var ctx2 = document.getElementById("chart2").getContext("2d");

        var data = [
            {
                value: parseInt(graficaInternacional),
                color:"#6C354E",
                label: "COBERTURA INTERNACIONAL"
            },
            {
                value: parseInt(restaInternacional),
                color: "#D8D8D8",
                label: "Porcentaje"
            }
        ]

        new Chart(ctx2).Pie(data, {
            showTooltips: false
        });

    };



}]);
