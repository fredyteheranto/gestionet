santanderApp.controller('contratacionController', ['$scope','$q', 'dataService','$rootScope','$location','$translate', function ($scope,$q, dataService, $rootScope,$location,$translate) {
    $scope.perfil = 2; // diseñador
    $scope.estado = 0; // disponible
    $scope.vacaciones = 0
    $scope.horarios = 8;
    $scope.multi = 1.35;
    $scope.costeDespidoDias = 1.66;
    $scope.mensualidades = 12;
    $scope.diasAnio= 365;
    $scope.checkDiseno = true;
    $scope.checkProgramacion = false;
    $scope.checkContDisenador = false;
    $scope.checkContProgramador = false;
	$scope.salarioMinimo = 600;
	
	$scope.salarioOfr = 0;
	$scope.salarioOfrConvertido = 0;
	$scope.salarioBruto = 0;
	
	$scope.poblacionInfo = [];
	$scope.contratadosInfo = [];

    var maximoContratadosDis;
    var maximoContratadosPro;

    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombreContratacion/parrafo_unoContratacion/parrafo_dosContratacion');
    };
    dataService.getPoblacion($rootScope.idEmpresaGeneral,$scope.perfil,$scope.estado).then(function (result) {
        $scope.poblacionInfoObject = result.data.datos;
		$scope.poblacionInfo = objetcToArray($scope.poblacionInfo, $scope.poblacionInfoObject);
		$scope.salarioMinimo = result.data.salarioMinimo;
    });


    $scope.perfiles = function(perfil, estado){
        $scope.perfil = perfil;
        $scope.estado = estado;
        dataService.getPoblacion($rootScope.idEmpresaGeneral,$scope.perfil,$scope.estado).then(function (result) {
            $scope.poblacionInfoObject = result.data.datos;
			$scope.poblacionInfo = objetcToArray($scope.poblacionInfo, $scope.poblacionInfoObject);
        });

        if($scope.perfil == 2) {
            $scope.checkDiseno = true;
            $scope.checkProgramacion = false;
        } else if ($scope.perfil == 1) {
            $scope.checkDiseno = false;
            $scope.checkProgramacion = true;
        }
    };

    $scope.contratados = function(perfil){
        $scope.perfil = perfil;
        $scope.estado = 1;
        dataService.getContratados($rootScope.idEmpresaGeneral,$scope.perfil,$scope.estado, $rootScope.momentoGeneral).then(function (result) {
            $scope.contratadosInfoObject = result.data.datos;
			$scope.contratadosInfo = objetcToArray($scope.contratadosInfo, $scope.contratadosInfoObject);
            $scope.maximoContratados = $scope.contratadosInfo.length;
            calcularDespido();
        });
        if($scope.perfil == 2) {
            $scope.checkContDisenador = true;
            $scope.checkContProgramador = false;
        } else if ($scope.perfil == 1) {
            $scope.checkContDisenador = false;
            $scope.checkContProgramador = true;
        }
    };

    $scope.contratar = function(idPoblacion, tipoContrato, salario, perfil, estado){
        $scope.idPoblacion = idPoblacion;
        $scope.tipoContrato = tipoContrato;
        $scope.salario = salario;
        //$scope.perfil = perfil;
        //$scope.estado = estado;
        $scope.errorProveedoresDatosVacios = "";
        $scope.errorContSalarioMin = "";
        $scope.errorConLimitePersonal = "";
        bDatosCorrectos = true;

        var contratados = [ dataService.getPoblacion($rootScope.idEmpresaGeneral,1,1), dataService.getPoblacion($rootScope.idEmpresaGeneral,2,1)];
        $q.all(contratados).then(function(data) {
            maximoContratadosDis = data[0].data.datos.length;
            maximoContratadosPro = data[1].data.datos.length;
            $scope.maximoContratados = maximoContratadosDis + maximoContratadosPro;
            if($scope.maximoContratados == 9){
                $scope.errorConLimitePersonal = $translate.instant('Ups.errorConLimitePersonal');
                $rootScope.mostrarUps = true;
                bDatosCorrectos = false;
            }else if(salario == null || tipoContrato == null){
                $scope.errorProveedoresDatosVacios = $translate.instant('Ups.errorProveedoresDatosVacios');
                $rootScope.mostrarUps = true;
                bDatosCorrectos = false;
            }else if ($scope.salario < $scope.salarioMinimo) 
            {
                $scope.errorContSalarioMin = $translate.instant('Ups.errorContSalarioMin');
                $rootScope.mostrarUps = true;
                bDatosCorrectos = false;
                //swal("Aviso", "No se puede contratar por un salario menor al mínimo establecido por ley.", "warning");
            } else if (bDatosCorrectos)
            {
                dataService.contratar($rootScope.idEmpresaGeneral, $scope.idPoblacion,$scope.tipoContrato, $scope.salario, $scope.vacaciones, $scope.horarios, $rootScope.momentoGeneral).then(function (result) {
                    // $scope.perfiles( $scope.perfil, $scope.estado );
                    var indexToDelete = indexFromUser($scope.poblacionInfo, $scope.idPoblacion);
                    $scope.poblacionInfo.splice(indexToDelete, 1);
                    if (result.data.alertas == true) {
                        dataService.getTotalAlertasNuevas($rootScope.idEmpresaGeneral).then(function (result) {
                            $rootScope.totalAlertas = result.data.datos;
                        });
                    }
                    swal({
                        title: $translate.instant('Alertas.contratacion'),
                        text: $translate.instant('Alertas.contratacionImport'),
                        type: "success",
                        confirmButtonColor: "#889641",
                        confirmButtonText: "OK"
                    });

                });
            }

        });

    };



    $scope.despedir = function(idPoblacion, perfil, antiguedad){
        $scope.idPoblacion = idPoblacion;
        $scope.perfil = perfil;
		$scope.antiguedad = antiguedad;
        $scope.errorDespidoAntiguedadCero = "";
        bDatosCorrectos = true;
		if ($scope.antiguedad == 0)
		{
            $scope.errorDespidoAntiguedadCero = $translate.instant('Ups.errorDespidoAntiguedadCero');
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;
        }
        else
		{
			 swal({
                    title: $translate.instant('Alertas.avisoDespidoTitulo'),
                    text: $translate.instant('Alertas.avisoDespidoTexto'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#889641",
                    confirmButtonText: $translate.instant('Alertas.si'),
                    cancelButtonText: $translate.instant('Alertas.no'),
                    closeOnConfirm: false
                },
                function(isConfirm)
                {
                    if (isConfirm)
                    {
                        dataService.despedir($rootScope.idEmpresaGeneral, $scope.idPoblacion, $rootScope.momentoGeneral).then(function (result) {
							//$scope.contratados($scope.perfil);
							var indexToDelete = indexFromUser($scope.contratadosInfo, $scope.idPoblacion);
							$scope.contratadosInfo.splice(indexToDelete, 1);
                            if (result.data.alertas == true) {
                                dataService.getTotalAlertasNuevas($rootScope.idEmpresaGeneral).then(function (result) {
                                    $rootScope.totalAlertas = result.data.datos;
                                });
                            }
							swal($translate.instant('Alertas.operacionCorrecta'));
                            if($rootScope.horasAsignadasProyecto == true){
                                swal($translate.instant('Alertas.orgCambios'));

                            }
                            $rootScope.horasAsignadasProyecto = false;
						});
                    }
			});
			
		}
    };

    var calcularDespido = function(){
        angular.forEach($scope.contratadosInfo, function(persona) {
            persona.despido = ($scope.costeDespidoDias * parseFloat(persona.antiguedad)) * ((persona.salarioActualInt * $scope.multi * 12)/ $scope.diasAnio);
        });
    };


    var indexFromUser = function (array, id) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].idPoblacion == id) {
                return i;
            }
        }
        return false;
    }
	
	var objetcToArray = function ($array, $object) {
		//reset
		$array = [];
		angular.forEach($object, function(element) {
			$array.push(element);
		});
		return $array;
	};

}]);