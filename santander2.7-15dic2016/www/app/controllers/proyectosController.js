santanderApp.controller('proyectosController', ['$scope', 'dataService','$location','$rootScope','$translate', function ($scope, dataService,$location, $rootScope,$translate) {
   
	$scope.parseInt = parseInt;
   $scope.origenProyecto = ["LOCAL", "NACIONAL", "INTERNACIONAL"];
   // $scope.fechaEntrega = ["Enero - Año 1", "Febrero - Año 1", "Marzo - Año 1", "Abril - Año 1", "Mayo - Año 1", "Junio - Año 1","Julio - Año 1", "Agosto - Año 1", "Septiembre - Año 1", "Octubre - Año 1", "Noviembre - Año 1", "Diciembre - Año 1", "Enero - Año 2", "Febrero - Año 2", "Marzo - Año 2"];
    $scope.fechaEntrega = [];
	$scope.momentoMundo = $rootScope.momentoGeneral;
	//variable de referencia para ajustar las fechas
	if ($rootScope.tipoPartida == 1)
	{
		//singleplayer, momento de la empresa, al ser igual queda 0
		$scope.momentoReferencia = 0;
	}
	else
	{
		// multi, momento del mercado
		$scope.momentoReferencia = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral; 
		$scope.momentoMundo = $rootScope.mundoMultiData.mundo.jornadaActual;
	}
	
	$scope.proyectosInfoProyectos =[];
	
    //$scope.horasTotalesIniciales = {Dise:0, Prog:0};
    $scope.horasTotalesInicialesDise = 0;
    $scope.horasTotalesInicialesProg = 0;
    for (var i = 0; i < 24; i++) {
        $scope.fechaEntrega.push($translate.instant('Subvenciones.FechaConAnio.' + i))
    };
    $scope.pantallaInformacion = function(){
        $location.path('/informacionGestion');
    };
    $scope.horas = false;
    $scope.getProyectosEnCurso = function(){
        dataService.getProyectosEnCurso($rootScope.idEmpresaGeneral,$rootScope.momentoGeneral, $scope.momentoMundo).then(function (result) {
            $scope.proyectosInfo = result.data.datos;
			$scope.proyectosInfoProyectos = objetcToArray($scope.proyectosInfoProyectos, $scope.proyectosInfo.proyectos);
            $scope.horasTotalesInicialesDise = result.data.datos.horasDisponibles.Dise;
            $scope.horasTotalesInicialesProg = result.data.datos.horasDisponibles.Prog;
            $scope.horasTotales = result.data.datos.horasDisponibles;
            $scope.ningunProyecto = $scope.proyectosInfoProyectos.length == 0;

            if ($scope.horasTotales != null &&  ($scope.horasTotales.Dise > 0 || $scope.horasTotales.Prog > 0))
            {

                angular.forEach(result.data.datos.proyectos, function(proyecto) {
                    $scope.actualizarHorasDisponibles(proyecto.horasDiseAsigJornada, proyecto.horasProgAsigJornada);

                });
            }

        });
    };
    $scope.cancelarProyecto = function(idProyecto){
        $scope.idProyecto = idProyecto;
            swal({
                    title: $translate.instant('Alertas.errorProyectosSegCan'),
                    text: $translate.instant('Alertas.errorProyectosSegCanPer'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#889641",
                    confirmButtonText: $translate.instant('Alertas.si'),
                    cancelButtonText: $translate.instant('Alertas.no'),
                    closeOnConfirm: false
                },
                function(isConfirm)
                {
                    if (isConfirm)
                    {
                        dataService.cancelarProyecto($rootScope.idEmpresaGeneral,$scope.idProyecto,$rootScope.momentoGeneral).then(function (result) {
                            swal($translate.instant('Alertas.errorProyectoCancelado'), $translate.instant('Alertas.errorProyectoHaSidoCan'), "success");
                            $scope.getProyectosEnCurso();
                        });
                    }


        });
    };

    var getHorasAsignadas = function (){
        var horasAsignadas = {prog:0, dise:0};
        angular.forEach($scope.proyectosInfoProyectos, function(proyecto) {
            horasAsignadas.prog += proyecto.horasProgAsigJornada;
            horasAsignadas.dise += proyecto.horasDiseAsigJornada;
        });
        return horasAsignadas;
    }

    $scope.asignarHorasProyecto = function(idProyecto,horasDise, horasProg) {//, horasTotalesDise, horasTotalesProg){
        $scope.idProyecto = idProyecto;
        $scope.horasDise = horasDise;
        $scope.horasProg = horasProg;

        bDatosCorrectos = true;
        $scope.errorProyectosHorasSup="";

        horasAsignadas = getHorasAsignadas();

        if( horasAsignadas.dise >  $scope.horasTotalesInicialesDise){
            bdatosCorrectos = false;
            $scope.errorProyectosHorasSup = $translate.instant('Ups.errorProyectosHorasSup');
            $rootScope.mostrarUps = true;

        } else if ( horasAsignadas.prog >  $scope.horasTotalesInicialesProg){
            bdatosCorrectos = false;
            $scope.errorProyectosHorasSup = $translate.instant('Ups.errorProyectosHorasSup');
            $rootScope.mostrarUps = true;

        }
		else if ( ( parseInt(horasAsignadas.prog) < 0) || ( parseInt(horasAsignadas.dise) < 0) ){
            $scope.errorProyectosHorasSup = $translate.instant('Ups.errorProyectosHorasNegativas');
            $rootScope.mostrarUps = true;
            bdatosCorrectos = false;
        }
		else if (bDatosCorrectos) {

            dataService.asignarHorasProyecto($rootScope.idEmpresaGeneral,$scope.idProyecto, $scope.horasDise, $scope.horasProg).then(function (result) {
                $scope.getProyectosEnCurso();
                $rootScope.horasAsignadasProyecto = true;
                swal($translate.instant('Alertas.operacionCorrecta'));
            });
        }
    };
	
	$scope.actualizarHorasDisponibles = function(horasDise, horasProg){
        $scope.horasTotales.Dise -= horasDise;
		$scope.horasTotales.Prog -= horasProg;

    };
	
	$scope.parseData = function(proyecto){
		proyecto.horasDiseAsigJornada = parseInt(proyecto.horasDiseAsigJornada);
		proyecto.horasProgAsigJornada = parseInt(proyecto.horasProgAsigJornada);

	}

	var objetcToArray = function ($array, $object) {
		//reset
		$array = [];
		angular.forEach($object, function(element) {
			$array.push(element);
		});
		return $array;
	};
	
	
    $scope.getProyectosEnCurso();
}]);
