//
//  ViewController.swift
//  edicae
//
//  Created by Fredy Saul Teheran Tovar on 22/11/16.
//  Copyright © 2016 Fredy Saul Teheran Tovar. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet var webView: UIWebView!
        
    let url = "http://www.afectadosclausulasuelo.org/mobile"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let requestURL = NSURL(string:url)
        let request = NSURLRequest(url: requestURL! as URL)
        
        webView.loadRequest(request as URLRequest)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

