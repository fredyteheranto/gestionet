//
//  ViewController.swift
//  Adicaw
//
//  Created by Fredy Saul Teheran Tovar on 29/11/16.
//  Copyright © 2016 Fredy Saul Teheran Tovar. All rights reserved.
//
import os.log
import JavaScriptCore
import UIKit
import WebKit
//import context

//import SQLite

class ViewController: UIViewController, WKNavigationDelegate,UIWebViewDelegate {
	let  wv = WKWebView(frame: UIScreen.main.bounds)
	let viewportScriptString = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); meta.setAttribute('initial-scale', '1.0'); meta.setAttribute('maximum-scale', '1.0'); meta.setAttribute('minimum-scale', '1.0'); meta.setAttribute('user-scalable', 'no'); document.getElementsByTagName('head')[0].appendChild(meta);"
	
	override func viewDidLoad() {
		super.viewDidLoad()
		_ = WKUserScript(source: viewportScriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
		guard let url =  NSURL(string: "http://unilever.gestionetsimulators.com") else { return }
		wv.navigationDelegate = self
		self.navigationController?.hidesBarsOnSwipe = true
		wv.load(NSURLRequest(url: url as URL) as URLRequest)
		view.addSubview(wv)
		wv.scrollView.isScrollEnabled = true
		wv.allowsBackForwardNavigationGestures = false
		wv.contentMode = .scaleToFill

	}
    // new function
    func webViewDidFinishLoad(webView: UIWebView) {
        var storage = HTTPCookieStorage.shared
        var cookiesArray = storage.cookies
        var array2 = [Any]()
        var isFound = false
        for cookie: HTTPCookie in cookiesArray! {
            if (cookie.name == "FedAuth") {
                
            }
        }
        
        let storagea = HTTPCookieStorage.shared
        for cookie in (storagea.cookies as! [HTTPCookie]?)!{
            print(cookie)
        }
        UserDefaults.standard.synchronize()

    }
   
    // fin fuction
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	func viewForZooming(in scrollView: UIScrollView) -> UIView? {
		return scrollView
	}
	
	func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
		if navigationAction.navigationType == .linkActivated  {
			if let newURL = navigationAction.request.url,
				// let host = newURL.host , !host.hasPrefix //host.contains
				let host = newURL.host, !host.hasPrefix("unilever.gestionetsimulators.com") &&
				UIApplication.shared.canOpenURL(newURL) &&
				UIApplication.shared.openURL(newURL) {
				print(newURL)
				// print("Redirected to browser. No need to open it locally")
				decisionHandler(.cancel)
			} else {
				// print("Open it locally")
				decisionHandler(.allow)
			}
		} else {
			//print("not a user click")
			decisionHandler(.allow)
		}
	}
}
