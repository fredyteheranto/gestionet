package com.gestionet.fredyteheranto.unilever;
/**
 * Created by fredy.teheran.gestionet@gmail.com on 22/11/16.
 */
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;

public class initunilever extends Activity  {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.init_unilever);

        Thread timerTread = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(initunilever.this, unileverActivity.class);
                    startActivity(intent);
                }
            }
        };
        timerTread.start();
    }
    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }
}
