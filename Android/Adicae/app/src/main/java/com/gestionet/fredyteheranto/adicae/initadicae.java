/**
 * Created by fredy.teheran.gestionet@gmail.com on 22/11/16.
 */
package com.gestionet.fredyteheranto.adicae;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;


public class initadicae extends Activity  {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.init_adicae);

        Thread timerTread = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(initadicae.this, AdicadActivity.class);
                    startActivity(intent);
                }
            }
        };
        timerTread.start();
    }
    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }

}
