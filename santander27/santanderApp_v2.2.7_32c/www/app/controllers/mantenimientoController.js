santanderApp.controller('mantenimientoController', ['$scope', 'dataService','$rootScope','$translate','$location', function ($scope, dataService, $rootScope, $translate, $location) {

	$scope.showMessage = function (data) {
		if ($rootScope.idioma == 'en'){
			$scope.getInfoMantenimiento = data.avisoEN;
		}
		else if($rootScope.idioma == 'pt'){
			$scope.getInfoMantenimiento = data.avisoPT;
		}
		else if($rootScope.idioma == 'de'){
			$scope.getInfoMantenimiento = data.avisoDE;
		}
		else{
			$scope.getInfoMantenimiento = data.avisoES;
		}	
	}

	 $scope.salir = function () {
        navigator.app.exitApp();
        //window.localStorage.setItem("video", false);
        //$location.path('/');
    }
	
	if ($rootScope.ConfigOk)
	{
		dataService.getInfoMantenimiento().then(function (result){
			$scope.showMessage(result.data.status[0]);
		});
	}
	else
	{
		console.log("mantenimiento");
		console.log($rootScope.ConfigKoMessage);
		$scope.showMessage($rootScope.ConfigKoMessage.status[0]);
	}
	
	
}]);
