santanderApp.controller('necesidadesController', ['$scope', 'dataService','$rootScope','$location','$translate', function ($scope, dataService, $rootScope, $location,$translate) {
    $scope.nombresProDispoMaterial = ["Equipos informáticos", "Licencia de software", "Hosting / Dominios", "Mobiliario", "Aire acondicionado"];
    if($rootScope.idioma == 'en'){
        $scope.nombresProDispoMaterial = ["Computer equipment", "Software licences", "Hosting / Domain services", "Office furniture", "Air conditioning"];
    }
    if($rootScope.idioma == 'pt'){
        $scope.nombresProDispoMaterial = ["Equipamentos informáticos", "Licenças de software", "Alojamento / Domínios", "Mobiliário", "Ar condicionado"];
    }

    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombreNecesidades/parrafo_unoNecesidades/parrafo_dosNecesidades');
    };
	var pantalla = 'necesidades';
    dataService.getDisponibilidadMaterial($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $rootScope.momentoGeneral, pantalla ).then(function (result) {
        $scope.proveedoresMaterialInfo = result.data.datos;

    });


}]);
