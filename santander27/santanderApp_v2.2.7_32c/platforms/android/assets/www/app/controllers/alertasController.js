santanderApp.controller('alertasController', ['$scope', 'dataService','$rootScope', function ($scope, dataService, $rootScope) {
    $scope.alertas;

    dataService.getNotificaciones($rootScope.idEmpresaGeneral).then(function (result) {
        //primero obtener las alertas nuevas y anteriores
		var mensajes = result.data.datos;
		if ($rootScope.idioma == 'de')
		{
			$.each( mensajes, function( i, value ){
				mensajes[i].titulo = mensajes[i].titulo.replace(/&#228;/g, "ä");
				mensajes[i].contenido = mensajes[i].contenido.replace(/&#228;/g, "ä");
				
				mensajes[i].titulo = mensajes[i].titulo.replace(/&#233;/g, "é");
				mensajes[i].contenido = mensajes[i].contenido.replace(/&#233;/g, "é");
				
				mensajes[i].titulo = mensajes[i].titulo.replace(/&#246;/g, "ö");
				mensajes[i].contenido = mensajes[i].contenido.replace(/&#246;/g, "ö");
				
				mensajes[i].titulo = mensajes[i].titulo.replace(/&#252;/g, "ü");
				mensajes[i].contenido = mensajes[i].contenido.replace(/&#252;/g, "ü");
				
				mensajes[i].titulo = mensajes[i].titulo.replace(/&#196;/g, "Ä");
				mensajes[i].contenido = mensajes[i].contenido.replace(/&#196;/g, "Ä");
				
				mensajes[i].titulo = mensajes[i].titulo.replace(/&#201;/g, "É");
				mensajes[i].contenido = mensajes[i].contenido.replace(/&#201;/g, "É");
				
				mensajes[i].titulo = mensajes[i].titulo.replace(/&#214;/g, "Ö");
				mensajes[i].contenido = mensajes[i].contenido.replace(/&#214;/g, "Ö");
				
				mensajes[i].titulo = mensajes[i].titulo.replace(/&#220;/g, "Ü");
				mensajes[i].contenido = mensajes[i].contenido.replace(/&#220;/g, "Ü");
				
				mensajes[i].titulo = mensajes[i].titulo.replace(/&#223;/g, "ß");
				mensajes[i].contenido = mensajes[i].contenido.replace(/&#223;/g, "ß");
			});
		}
		$scope.alertas = mensajes;
		//despurs marcarlas como leidas
		dataService.marcarAlertasLeidas($rootScope.idEmpresaGeneral).then(function () {
			$rootScope.totalAlertas = 0;
		});
    });


}]);
