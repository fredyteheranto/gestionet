santanderApp.controller('proveedoresController', ['$scope', '$q', 'dataService','$rootScope', '$filter','$translate','$location', function ($scope, $q, dataService, $rootScope, $filter, $translate, $location) {
    var grafica;

    $scope.proveedoresInfo;
    //$scope.nombresProDispoMaterial = ["Equipos informáticos", "Licencia de software", "Mobiliario", "Hosting / Dominios", "Aire acondicionado"];
    $scope.nombresProDispoMaterial = [$translate.instant('Proovedores.equiposInformaticos'), $translate.instant('Proovedores.licenciaSoft'), $translate.instant('Proovedores.mobiliario'), $translate.instant('Proovedores.hosting'), $translate.instant('Proovedores.aireAcondicionado')];

    $scope.costeU = [0,0,0];
    $scope.Precio;
    //$scope.costeT;
	$scope.costeT = [0,0,0];
    $scope.cant;
    $scope.proveedoresMaterialInfo;
    $scope.pago;

    $scope.precioInfotech = 0;
    $scope.precioSilver = 0;
    $scope.precioWei = 0;

    $scope.porcientoAveriasInfotech;
    $scope.porcientoAveriasSilver;
    $scope.porcientoAveriasWei;

    $scope.recargoInfotech;
    $scope.recargoSilver;
    $scope.recargoWei;

    $scope.cant = undefined;
    $scope.idProductoOptions = [
        {
            "value": 0,
            "nombre": $translate.instant('comunes.selecciona')
        },
        {
            "value": 1,
            "nombre": $translate.instant('Proovedores.equiposInformaticos')
        },
        {
            "value": 2,
            "nombre": $translate.instant('Proovedores.licenciaSoft')
        },
        {
            "value": 3,
            "nombre": $translate.instant('Proovedores.hosting')
        },
        {
            "value": 4,
            "nombre": $translate.instant('Proovedores.mobiliario')
        },
        {
            "value": 5,
            "nombre": $translate.instant('Proovedores.aireAcondicionado')
        }
    ];

    $scope.formaPagoOptions = [
        {
            "value": 0,
            "nombre": $translate.instant('comunes.selecciona')
        },
        {
            "value": 1,
            "nombre": $translate.instant('Proovedores.contado')
        },
        {
            "value": 2,
            "nombre": $translate.instant('Proovedores.aplazado')
        }
    ];

    $scope.idProducto;

    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombreProveedores/parrafo_unoProveedores/parrafo_dosProveedores');
    };

	var pantalla = 'proveedores';
    var urlCalls = [dataService.getProveedores(), dataService.getDisponibilidadMaterial($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $rootScope.momentoGeneral, pantalla)];
    $q.all(urlCalls).then(function(data) {
        $scope.proveedoresInfo = data[0].data.datos;
        $scope.proveedoresMaterialInfo = data[1].data.datos;

        calcularPorcientoAverias();

        $scope.dataAct = data;
        cargarGrafica();
    });
    var disponibilidad = function(){
        dataService.getDisponibilidadMaterial($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $rootScope.momentoGeneral, pantalla).then(function (result){
            $scope.proveedoresMaterialInfo = result.data.datos;
        });
    }
    $scope.comprar = function(idProducto, cant, idProv, pago, costeT){
        bDatosCorrectos = true;

        var numElementos;
        $scope.errorProveedoresMaxEle="";
        $scope.errorProveedoresDatosVacios="";
        var maximo = $scope.proveedoresMaterialInfo[idProducto - 1].MaximoDisponible;
        var maximoAdquirido = $scope.proveedoresMaterialInfo[idProducto - 1].adquirido;
        $scope.maximoPermitidoAdquirido = parseFloat(cant) + parseFloat(maximoAdquirido);
        if(idProducto == null || cant == null || cant == 0 || pago == null || cant < 0 || idProducto == 0 || pago == 0){
            $scope.errorProveedoresDatosVacios = $translate.instant('Ups.errorProveedoresDatosVacios');
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;
        }
        else if (cant > maximo && maximo != 0) {
            $scope.errorProveedoresMaxEle = $translate.instant('Ups.errorProveedoresMaxEle');
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;
            //alert("No puede comprar más de " + maximo + " artículos")
        }else if($scope.maximoPermitidoAdquirido > maximo && maximo != 0){
            $scope.errorProveedoresMaxEle = $translate.instant('Ups.errorProveedoresMaxEle');
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;
        }
        else if (bDatosCorrectos) {

            $scope.datos = JSON.stringify({
                "Cnt": +cant,
                "idProducto": +idProducto,
                "idProv": +idProv,
                "idPago": +pago,
                "Cst": +costeT
            });
            dataService.comprarProveedores($rootScope.idEmpresaGeneral, $rootScope.momentoGeneral, $rootScope.idMundoGeneral, $scope.datos).then(function (result) {
                if (result.data.alertas == true) {
                    dataService.getTotalAlertasNuevas($rootScope.idEmpresaGeneral).then(function (result) {
                        $rootScope.totalAlertas = result.data.datos;
                    });
                }
                disponibilidad();

                swal($translate.instant('Alertas.operacionCorrecta'));
            });

        }

    };

    $scope.actualizarCosteUnitario = function(idProducto, idProveedor, cantidad, pago) {
        //$scope.costeU = parseFloat($scope.proveedoresMaterialInfo[idProducto - 1].Precio) * parseFloat($scope.proveedoresInfo[idProveedor - 1].CB);

        switch(idProveedor - 1) {
            case 0:
                $scope.costeU[0] = parseFloat((parseFloat($scope.proveedoresMaterialInfo[idProducto - 1].Precio) * parseFloat($scope.proveedoresInfo[idProveedor - 1].CB)).toFixed());
                $scope.precioInfotech = $scope.costeU[0];
                break;
            case 1:
                $scope.costeU[1] = parseFloat((parseFloat($scope.proveedoresMaterialInfo[idProducto - 1].Precio) * parseFloat($scope.proveedoresInfo[idProveedor - 1].CB)).toFixed());
                $scope.precioSilver = $scope.costeU[1];
                break;
            case 2:
                $scope.costeU[2] = parseFloat((parseFloat($scope.proveedoresMaterialInfo[idProducto - 1].Precio) * parseFloat($scope.proveedoresInfo[idProveedor - 1].CB)).toFixed());
                $scope.precioWei = $scope.costeU[2];
                break;
        }

        $scope.actualizarCosteTotal(pago, idProveedor, cantidad);
       // updateTable();
    };

    $scope.actualizarCosteTotal = function(idFormaPago, idProveedor, cantidad) {
        if(idFormaPago == 1) {
            //$scope.costeT =  $scope.costeU[idProveedor - 1] * cantidad; 
			$scope.costeT[idProveedor - 1] =  $scope.costeU[idProveedor - 1] * cantidad;
        } else if(idFormaPago == 2) {
            //$scope.costeT =  $scope.costeU[idProveedor - 1] * parseFloat($scope.proveedoresInfo[idProveedor - 1].Recargo) * cantidad;
			$scope.costeT[idProveedor - 1] =  $scope.costeU[idProveedor - 1] * parseFloat($scope.proveedoresInfo[idProveedor - 1].Recargo) * cantidad;
        }
    };


    var cargarGrafica = function(){
        //Chart1
        var ctx = document.getElementById("chart").getContext("2d");
        $scope.proveedoresInfo
        var data = {
            labels: [$translate.instant('Proovedores.precio'), $translate.instant('Proovedores.averias'), $translate.instant('Proovedores.recargo')],
            datasets: [
                {
                    label: "My First dataset",
                    fillColor: "#2B7C96",
                    strokeColor: "#2B7C96",
                    pointColor: "#2B7C96",
                    pointStrokeColor: "#2B7C96",
                    pointHighlightFill: "#2B7C96",
                    pointHighlightStroke: "#2B7C96",
                    data: [10 , $scope.porcientoAveriasInfotech, parseFloat($scope.proveedoresInfo[0].Recargo)]
                },
                {
                    label: "My Second dataset",
                    fillColor: "#D0A426",
                    strokeColor: "#D0A426",
                    pointColor: "#D0A426",
                    pointStrokeColor: "#D0A426",
                    pointHighlightFill: "#D0A426",
                    pointHighlightStroke: "#D0A426",
                    data: [8.5, $scope.porcientoAveriasSilver, parseFloat($scope.proveedoresInfo[1].Recargo)]
                },
                {
                    label: "My Third dataset",
                    fillColor: "#6C354E",
                    strokeColor: "#6C354E",
                    pointColor: "#6C354E",
                    pointStrokeColor: "#6C354E",
                    pointHighlightFill: "#6C354E",
                    pointHighlightStroke: "#6C354E",
                    data: [6, $scope.porcientoAveriasWei, parseFloat($scope.proveedoresInfo[2].Recargo)]
                }
            ]
        };

        grafica = new Chart(ctx).Bar(data, {
            barShowStroke: false,
            responsive: true
        });

    };

    var updateTable = function() {
        grafica.clear();
        cargarGrafica();
    }

    var calcularPorcientoAverias = function () {
        $scope.porcientoAveriasInfotech = parseFloat(100 - $scope.proveedoresInfo[0].Calidad);
        $scope.porcientoAveriasSilver = parseFloat(100 - $scope.proveedoresInfo[1].Calidad);
        $scope.porcientoAveriasWei = parseFloat(100 - $scope.proveedoresInfo[2].Calidad);
    };
}]);
