santanderApp.controller('loginController', ['$scope','dataService','storageService','$location','$rootScope', '$translate', function ($scope,dataService,storageService,$location,$rootScope,$translate) {
    
 	$scope.formSubmitted = false;
 	$scope.loginForm;
    $scope.user = $rootScope.loggedInUserNameConcurso; //window.localStorage.getItem("loggedInUserNameConcurso");
    $scope.password = $rootScope.loggedInPasswordConcurso; //window.localStorage.getItem("loggedInPasswordConcurso");
	$scope.loggedInUserNameConcurso = $rootScope.loggedInUserNameConcurso; //window.localStorage.getItem("loggedInUserNameConcurso");
    $scope.login = function () {
    	$scope.formSubmitted = true;
    	if($scope.loginForm.$valid) {
             dataService.validateUser( $scope.user, $scope.password).then(function (result) {
           
				$rootScope.isLogged = true;

				if (result.data.status == "success")
				{
					$rootScope.loggedInUserIdConcurso = result.data.userId;
					$rootScope.loggedInUserNameConcurso = $scope.user;
					$rootScope.loggedInPasswordConcurso = $scope.password;
	                //window.localStorage.setItem("loggedInUserNameConcurso", $scope.user);
                    //window.localStorage.setItem("loggedInPasswordConcurso", $scope.password);
					//window.localStorage.setItem("loggedInUserIdConcurso", $rootScope.loggedInUserIdConcurso);
					$location.path("/concurso");

				}
				else if(result.data.status == "fail")
				{	
					if (result.data.campos.username == false){
						swal({
		                    title:'',
		                    text: $translate.instant('Login.usuarioIncorrecto'),
		                    type: "warning",
		                    showCancelButton: false,
		                    confirmButtonColor: "#889641",
		                    confirmButtonText: $translate.instant('comunes.aceptar'),
               			 });
					}
					else if (result.data.campos.password == false){
						swal({
		                    title:'',
		                    text: $translate.instant('Login.passwordIncorrecto'),
		                    type: "warning",
		                    showCancelButton: false,
		                    confirmButtonColor: "#889641",
		                    confirmButtonText: $translate.instant('comunes.aceptar'),
               			 });
					}
					
				}
        	});
    	}
       
    };

var cargar = function(){
	$("#login").val("EN");	
}	
cargar();
	
$scope.recuperarPwd = function(){
	dataService.solicitarResetPwd($scope.recuperarEmail).then(function (result){
    	if (result.data.status=="fail"){
    		 swal({
                    title: "",
                    text: $translate.instant('Login.recuperaraPwdNoExisteEmail'),
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#889641",
                    confirmButtonText: $translate.instant('comunes.aceptar'),
                    cancelButtonText: $translate.instant('Alertas.no'),
                    closeOnConfirm: true
                
			});
    	}
    	if(result.data.status=="ok"){
    		 swal({
                    title: "",
                    text: $translate.instant('Login.recuperarPwdOk'),
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#889641",
                    confirmButtonText: $translate.instant('comunes.aceptar'),
                    cancelButtonText: $translate.instant('Alertas.no'),
                    closeOnConfirm: true
                },
                function(isConfirm)
                {
                    if (isConfirm)
                    {
                      $location.path("/recuperarPassword");
                       $scope.$apply();
                    }
			});
    	}


	});
}
	
}]);