santanderApp.controller('menuPrincipalController', ['$scope','dataService','storageService','$location','$rootScope', '$translate', function ($scope,dataService,storageService,$location,$rootScope,$translate) {

	$scope.nuevaPartida = function ($event) {
        if ($rootScope.loggedInUserId == null) //if(window.localStorage.getItem("loggedInUserId") == null) 
		{
			//no user --> crear
			$scope.crearUsuario();
		}
		else
		{
			// user --> comprobar existe
			//console.log("menuprincipal.js  comprobar existe user " + $rootScope.loggedInUserId);
			dataService.checkUserExistente($rootScope.loggedInUserId).then(function (result){ //dataService.checkUserExistente(window.localStorage.getItem("loggedInUserId")).then(function (result){
				if (result.data.status != "success" || result.data.userid == null)
				{
					// no existe, creamos uno nuevo
					$scope.crearUsuario();
				}
				else
				{
					//existe ok, seguimos
					$location.path("/nuevaPartida");
				}
			});
		}
	};
	
	$scope.crearUsuario = function () {
		dataService.createUser().then(function(result){
			$rootScope.loggedInUserId = result.data.userid;
			$rootScope.loggedInUserName = result.data.username;
			$rootScope.loggedInUserPass = result.data.password;
		
			//window.localStorage.setItem("loggedInUserId", result.data.userid);
			//window.localStorage.setItem("loggedInUserName", result.data.username);
			//window.localStorage.setItem("loggedInUserPass", result.data.password);
			
			//console.log("menuprincipal.js  crearUsuario " + $rootScope.loggedInUserId);
			
			$location.path("/nuevaPartida");
		});	
	}

	$scope.continuarPartida = function ($event) {
        //$rootScope.partidaTerminada = false;	
		$location.path("/continuarPartida");
		
	};	
	
    //continuar viene de la pantalla ayudaConfigurar
    $scope.continuar = function(){

		/*if(window.localStorage.getItem("noMostrarAyuda") !=  false){
			$location.path("/nuevaPartida");
		}*/
        if($scope.noMostrar){
			$rootScope.showAyuda = true;
			window.localStorage.setItem("noMostrarAyuda", false);
			$location.path("/nuevaPartidaSingle");

        }else{
            $location.path("/nuevaPartidaSingle");
        }

	}

	//continuar viene de la pantalla ayudaConfigurarMulti
    $scope.continuarMulti = function(){

        if($scope.noMostrarMulti){
			$rootScope.showAyudaMulti = true;
			window.localStorage.setItem("noMostrarAyudaMulti", false);
			$location.path("/nuevaPartidaMulti");

        }else{
            $location.path("/nuevaPartidaMulti");
        }

	}

	$rootScope.checkMantenimiento = function () {
		$scope.formSubmitted = true;
		$scope.idEmpresa;
		$scope.idMundo;
		$scope.puntuacion;
		$scope.hayPartida = false;
		if($rootScope.loggedInUserId) {

			dataService.getInfoMantenimiento($rootScope.ConfigOk).then(function (result){
				//console.log("resultado");
				//console.log(result);
				if(result.data.status[0].estado == 1){
					$location.path("/mantenimiento");
				}
				else {
					$rootScope.getConcursosActivos();
				}
			});
		}
	};
	//checkMantenimiento();
	
	$scope.salir = function () {
		storageService.storeData();
		//navigator.app.exitApp();
		
	}
	$scope.logout = function () {
        resetCredentials();
        $location.path("/");
       

	};

	var resetCredentials = function () {
		//console.log("resetCredentials");
		$rootScope.loggedInUserId = null;
		$rootScope.loggedInUserName = null;
		$rootScope.loggedInUserIdConcurso = null;
		$rootScope.isLogged = false;

		//window.localStorage.clear("loggedInUserId");
		//window.localStorage.clear("loggedInUserName");
		//window.localStorage.clear("loggedInUserPass");
		//window.localStorage.clear("loggedInUserIdConcurso");
	};

	//Video
	if(window.localStorage.getItem("video") == "undefined" || window.localStorage.getItem("video") == null || window.localStorage.getItem("video") == "false") {
		window.localStorage.setItem("video", true);
	}


	$rootScope.android = window.localStorage.getItem("android");

	$rootScope.getConcursosActivos = function () {
		dataService.getConcursosActivos().then(function(result){
			
			if(result.data.status == "fail" || result.data.datos == null || result.data.datos.length == 0){
				$scope.verConcurso = false;
				$rootScope.dentroEpocaConcurso = false;
			}else{
				$scope.verConcurso = true;

				$rootScope.idConcurso = result.data.datos[0].idConcurso;

				var fechaActual = result.data.datos[0].current.substring(0, 10);
				fechaActual = fechaActual.replace(/-/gi,'');
				var fechaComienzo = result.data.datos[0].fechaComienzo.replace(/-/gi,'');
				var fechaFin = result.data.datos[0].fechaFin.replace(/-/gi,'');
				var fechaInscripcionInicio = result.data.datos[0].fechaInscripcionInicio.replace(/-/gi,'');
				var fechaInscripcionFin = result.data.datos[0].fechaInscripcionFin.replace(/-/gi,'');
				
				$rootScope.maxConcursantes = result.data.datos[0].maxConcursantes;
				$rootScope.fechaComienzo = result.data.datos[0].fechaComienzo;
				$rootScope.fechaFin = result.data.datos[0].fechaFin;
				$rootScope.fechaInicioFaseFinal = result.data.datos[0].fechaInicioFaseFinal;
				$rootScope.fechaFinFaseFinal = result.data.datos[0].fechaFinFaseFinal;
				$rootScope.fechaInscripcionInicio = result.data.datos[0].fechaInscripcionInicio;
				$rootScope.fechaInscripcionFin = result.data.datos[0].fechaInscripcionFin;
				
				if(fechaActual >= fechaComienzo && fechaActual <= fechaFin ){
					//console.log("Dentro de fechas del concurso");
					$rootScope.dentroEpocaConcurso = true;
				}else{
					//console.log("Fuera de fechas del concurso")
					$rootScope.dentroEpocaConcurso = false;
				}

				if(fechaActual >= fechaInscripcionInicio && fechaActual <= fechaInscripcionFin ){
					//console.log("Dentro de fechas del REGISTRO concurso");
					$rootScope.dentroEpocaRegistro = true;
				}else{
					//console.log("Fuera de fechas del REGISTRO concurso")
					$rootScope.dentroEpocaRegistro = false;
				}
			}
		
		});	
	}
	//getConcursosActivos();

	
	$scope.epocaConcurso = function(){
		/*
		if($rootScope.dentroEpocaConcurso == true){
			$location.path("/login");
		}
		if($rootScope.dentroEpocaRegistro == true){
			$location.path("/concurso");
		}
		*/
		$location.path("/concurso");
	}
	/* check idioma */
	if ($rootScope.idioma == 'de')
	{
		$("#constelacionFormacion").hide();
		$(".constImg").addClass("mini");
		$("#constelacionBecas img").attr('src','img/ico_APP_Becas_72x72_de.png');
		$(".logo img").attr('src','img/Logo_Incio_de.png');
		$(".logoSanConteslacion img").attr('src','img/LogoSantanderUniv_Transparente_de.png'); 
		$(".cerrarLogoAyudas img").attr('src','img/LogoSantanderUniv_Transparente_de.png');
	}
	else
	{
		$("#constelacionFormacion").show();
		$(".constImg").removeClass("mini");
		$("#constelacionBecas img").attr('src','img/ico_APP_Becas_72x72.png');
		$(".logo img").attr('src','img/Logo_Incio.png');
		$(".logoSanConteslacion img").attr('src','img/LogoSantanderUniv_Transparente.png');
		$(".cerrarLogoAyudas img").attr('src','img/LogoSantanderUniv_Transparente.png'); 
	}
	
	$rootScope.showMenu= false;
	
	
	
	
}]);
