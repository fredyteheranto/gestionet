santanderApp.controller('calendarioController', ['$scope','dataService','$location','$rootScope','$translate', function ($scope,dataService,$location,$rootScope,$translate) {
  $scope.parseInt = parseInt;
  $scope.fechas = new Array();
  
	dataService.getFasesConcurso($rootScope.idConcurso).then(function(result){
		$scope.fasesInfoObject = result.data.datos;
		$scope.fasesInfo = objetcToArray($scope.fasesInfo, $scope.fasesInfoObject);
		var longitud = $scope.fasesInfo.length;
		if ($scope.fasesInfo != null)
		{
			for(var i=0; i < longitud; i++)
			{
				console.log("fecha inicio " + $scope.fasesInfo[i].mundofechaInicio);
				$scope.fechas.push(getNumber($scope.fasesInfo[i].duracion, $scope.fasesInfo[i].mundofechaInicio));
				$scope.fasesInfo[i].open = false;
			}
			console.log($scope.fechas);
		}
	});
	
	var getNumber = function(duracion, fechaInicio) {
		var total = duracion/24;
		fechaInicio = new Date(fechaInicio.substr(0,10));
		var fechasTemp = new Array();
		var fecha = null;
		//console.log("fechaInicio");
		//console.log(fechaInicio);
		
		var vacios = total % 3;
		console.log("vacios " + vacios);
		
		for (var i = 0; i < total; i++)
		{	
			//var fecha = new Date(fechaInicio.getDate()+i);
			var fecha = new Date(fechaInicio);
			//console.log("fecha  " + fecha);
			//console.log("fecha inicio " + fechaInicio);
			fecha.setDate(fechaInicio.getDate() + i);
			//console.log("fecha nueva " + fecha);
			
			fechasTemp.push(fecha); 
		}
		//for (var i = 0; i <= vacios; i++)
		for (var i = 0; i < vacios; i++)
		{
			fechasTemp.push('');
		}
		
		return fechasTemp;
	}
	
	 /**
	 * Open Close panel method
	 * @param idx {Number} - Array index
	 */
	$scope.openClosePanel = function (idx) {
		$scope.groups[idx].open = !$scope.groups[idx].open;
	};
	
	$scope.parseDate = function (fecha) {
		var fechi = new Date(fecha.substr(0,10));
		console.log(fechi);
		return fechi;
	};

	var objetcToArray = function ($array, $object) {
		//reset
		$array = [];
		angular.forEach($object, function(element) {
			$array.push(element);
		});
		return $array;
	};
	
 
$scope.oneAtATime = true;
 
}]);
