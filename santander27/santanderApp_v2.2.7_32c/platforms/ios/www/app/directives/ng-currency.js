/*
 * ng-currency
 * http://alaguirre.com/

 * Version: 0.7.10 - 2014-11-26
 * License: MIT
 */

angular.module('ng-currency', ['tmh.dynamicLocale'])
    .directive('ngCurrency', ['$filter', '$locale','$rootScope','tmhDynamicLocale','$compile', function ($filter, $locale, $rootScope, tmhDynamicLocale, $compile) {
        return {
            require: 'ngModel',
			//transclude: true,
			restrict: 'AE',
            scope: {
                min: '=min',
                max: '=max',
                currencySymbol: '@',
                ngRequired: '=ngRequired',
				campoEditable: '@',
				multip: '=',
				customerInfo: '=',
				sinDecimales: '@'
            },
            link: function (scope, element, attrs, ngModel) {

                function decimalRex(dChar) {
                    return RegExp("\\d|\\-|\\" + dChar, 'g');
                }

                function clearRex(dChar) {
                    return RegExp("\\-{0,1}((\\" + dChar + ")|([0-9]{1,}\\" + dChar + "?))&?[0-9]{0,2}", 'g');
                }
				
				function convertirMonedaBaseActual(valor) {                    
					//valor model es siempre en euros 					
					var cambio = 1;
					var multi = 1;
					if ($rootScope.MonedaActual != 0)
						cambio = $rootScope.cambiosMoneda[$rootScope.MonedaActual];
					else
						cambio = 1;
					//para multiplicadores
					if (angular.isDefined(scope.multip))
						multi = scope.multip;
					else
						multi = 1;
					var resultado = ngModel.$modelValue * cambio * multi;
					return resultado;
					
                }
				
				function convertirMonedaActualBase() {
                    //valor model es siempre en euros 					
					var cambio = 1;
					if ($rootScope.MonedaActual != 0)
						cambio = $rootScope.cambiosMoneda[$rootScope.MonedaActual];
					else
						cambio = 1;
					var monedaTemp = ngModel.$modelValue;					
					//ngModel.$modelValue = ngModel.$modelValue  / cambio;
					//scope.customerInfo = monedaTemp; 
					//scope.customerInfo = ngModel.$modelValue * cambio;
					scope.customerInfo = monedaTemp / cambio;					
					//para multiplicadores
					if (angular.isDefined(scope.multip))
					{
						scope.customerInfo = scope.customerInfo * scope.multip;
					}
                }

                function clearValue(value) {
                    value = String(value);
					var dSeparator = $locale.NUMBER_FORMATS.DECIMAL_SEP;
                    var cleared = null;

                    if(RegExp("^-[\\s]*$", 'g').test(value)) {
                        value = "-0";
                    }

                    if(decimalRex(dSeparator).test(value))
                    {
                        cleared = value.match(decimalRex(dSeparator))
                            .join("").match(clearRex(dSeparator));
                        cleared = cleared ? cleared[0].replace(dSeparator, ".") : null;
                    }
                    else
                    {
                        cleaned = null;
                    }

                    return cleared;
                }

                function currencySymbol() {
                    if (angular.isDefined(scope.currencySymbol)) {
                        return scope.currencySymbol;
                    } else {
                        return $locale.NUMBER_FORMATS.CURRENCY_SYM;
                    }
                }

                ngModel.$parsers.push(function (viewValue) {					
                    cVal = clearValue(viewValue);
					//console.log("ngmodelparser " + cVal);						
                    return parseFloat(cVal);
                });
				
				//if (scope.editable == "undefined")
				//if (attrs.campoEditable) 
				element.on("click", function () {	
					if (angular.isDefined(scope.campoEditable))
					{
						//console.log("click en input ");
						//console.log("-----------------------actualizar campo editable click ");
						element.val("");
					}
					else
					{	
						//console.log("click en span ");
					}
				});
				
				element.on("blur", function () {	
					if (angular.isDefined(scope.campoEditable))
					{
						//console.log("blur en input ");						
						////console.log("-----------------------actualizar campo editable ");
						////console.log("-----------------------ngModel.$modelValue antes de conversi�n " + ngModel.$modelValue);
						////console.log("-----------------------ngModel.$viewValue antes de conversi�n " + ngModel.$viewValue);
						
						//var valor = ngModel.$modelValue;
						//ngModel.$viewValue = ngModel.$modelValue;
						//valor = convertirMonedaActualBase(ngModel.$modelValue);
						//ngModel.$modelValue = valor;
						convertirMonedaActualBase();
						
						////console.log("-----------------------ngModel.$modelValue despu�s de conversi�n " + ngModel.$modelValue);
						////console.log("-----------------------ngModel.$viewValue despu�s de conversi�n " + ngModel.$viewValue);
						////console.log("-----------------------valor visualizado de conversi�n " + ngModel.$viewValue);				
						if (angular.isDefined(scope.sinDecimales))
							element.val($filter('mycurrency')(ngModel.$modelValue));
						else
							element.val($filter('currency')(ngModel.$modelValue));

						////console.log("-----------------------ngModel.$modelValue despu�s de formateo " + ngModel.$modelValue);
						////console.log("-----------------------ngModel.$viewValue despu�s de formateo " + ngModel.$viewValue);
						//console.log("-----------------------scope.valorBase despu�s de formateo " + scope.customerInfo);
						
						//element.val($filter('currency')(scope.customerInfo));
					}
					else
					{
						//console.log("blur en span ");
					}
				});
				
				if (angular.isDefined(scope.campoEditable))
				{
					//campo editable
					//console.log("currency input ");
				}
				else
				{
					/*$rootScope.$on("loadedDataEvent", function () {
						console.log("loadedDataEvent " + ngModel.$modelValue);
						element.html($filter('currency')(ngModel.$modelValue, currencySymbol()));
					 });*/
					 
					/*console.log("currency span");
					console.log(element.html());
					console.log("currency span2" + ngModel.$modelValue);
					console.log($filter('currency')(ngModel.$modelValue, currencySymbol()));
					//element.html($filter('currency')(ngModel.$modelValue, currencySymbol()));
					//$compile(element.contents())(scope); 
					console.log(element.html());*/
				}

                ngModel.$formatters.unshift(function (value) {
                    //return $filter('currency')(value, currencySymbol());
					if (angular.isDefined(scope.sinDecimales))
						return $filter('mycurrency')(value);
					else
						return $filter('currency')(value);
                });

                scope.$watch(function () {
					//console.log("watcher " + ngModel.$modelValue);
					if (angular.isDefined(scope.campoEditable))
					{
						convertirMonedaActualBase();
					}
                    return ngModel.$modelValue
                }, function (newValue, oldValue) {
                    runValidations(newValue);
					//console.log("watch runValidations newValue " + newValue);
					//console.log("--------------------scope.editable: /" + scope.editable + "/");
					//if ( scope.editable == "undefined")
					//if (attrs.campoEditable)
					if (angular.isDefined(scope.campoEditable))
					{
						//console.log("campo editable");
					}
					else
					{
						//console.log("locale" + $rootScope.$locale);
						//console.log("campo no editable");
						//console.log("-----------------------watch changed value " + ngModel.$modelValue);
						var valor = convertirMonedaBaseActual(ngModel.$modelValue);
						//console.log("-----------------------watch conversion moneda " + valor);
						//var decimales = 2;
						if (angular.isDefined(scope.sinDecimales))
						{
							//decimales = 0;
							//console.log("sin decimales" + valor);
							element.html($filter('mycurrency')(valor, currencySymbol()));
						}
						else
							element.html($filter('currency')(valor, currencySymbol()));
					}
                })
				
				

                function runValidations(cVal) {
                    if (!scope.ngRequired && isNaN(cVal)) {
                        return
                    }
                    if (scope.min) {
                        var min = parseFloat(scope.min)
                        ngModel.$setValidity('min', cVal >= min)
                    }
                    if (scope.max) {
                        var max = parseFloat(scope.max)
                        ngModel.$setValidity('max', cVal <= max)
                    }
                }
            }
        }
    }]);
