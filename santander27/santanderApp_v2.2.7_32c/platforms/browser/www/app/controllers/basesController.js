santanderApp.controller('basesController', ['$scope', '$location','$translate', 'dataService','$rootScope', function ($scope,$location, $translate,dataService,$rootScope) {

	$scope.parseDate = function (fecha) {
		var fechi = new Date(fecha.substr(0,10));
		console.log(fechi);
		return fechi;
	};


	$scope.concurso = [];
	$scope.concurso['maxParticipantes'] = $rootScope.maxConcursantes;
	if ($scope.concurso['maxParticipantes'] == null)
		$scope.concurso['maxParticipantes'] = 3500;
	
	$scope.concurso['inicioInscripcion_Hora'] = '00:00';
	$scope.concurso['finInscripcion_Hora'] = '24:00';
	
	$scope.totalValMax = 0;
	$scope.totalValMaxReal = 0;
	$scope.valoracionMaximaFaseTexto = "14.000"
	$scope.valoracionMaximaFase = 14000;
	
	$scope.appleDevice = false;
	var dispositivo = navigator.userAgent.toLowerCase();
	if( dispositivo.search(/iphone|ipod|ipad/) > -1 ){
		$scope.appleDevice = true;
	}
	
	
	$scope.fasesInfo = new Array();
	
	//if($rootScope.dentroEpocaConcurso == true){
		dataService.getFasesConcurso($rootScope.idConcurso).then(function(result){
			var longitud = result.data.datos.length;
			for(var i=0; i < longitud; i++){
				if (result.data.datos[i].tipoMundo == '3')
				{
					$scope.fasesInfo.push(result.data.datos[i]);
					$scope.totalValMax += 14000;
					//$scope.totalValMaxReal += (14000 + 14000 * result.data.datos[i].porcentajePuntuacion / 100);
					$scope.totalValMaxReal += (result.data.datos[i].ponderacion * $scope.valoracionMaximaFase);
					console.log($scope.totalValMaxReal);
					$scope.faseNacionalfechaInicio = new Date($scope.parseDate(result.data.datos[i].mundofechaInicio));
					$scope.faseNacionalfechaFin = new Date($scope.parseDate(result.data.datos[i].mundofechaFin));
					console.log($scope.faseNacionalfechaInicio);
				}
			}
		});	
	//}

	dataService.getPremiosConcurso($rootScope.idConcurso).then(function (result){
		$scope.premiosInfo = result.data.datos;
	});
    
	dataService.getSorteosConcurso($rootScope.idConcurso, $rootScope.loggedInUserIdConcurso).then(function (result){
    	$scope.sorteosInfo = result.data.datos;
	});
	
	$scope.fechaInscripcionInicio = new Date($rootScope.fechaInscripcionInicio);
	$scope.fechaInscripcionFin = new Date($rootScope.fechaInscripcionFin);
	$scope.fechaInicio = new Date($rootScope.fechaComienzo);
	$scope.fechaFin = new Date($rootScope.fechaFin);
	$scope.faseNacionalfechaInicio = new Date($rootScope.fechaComienzo);
	$scope.faseNacionalfechaFin = new Date($rootScope.fechaFin);
	$scope.fechaInicioFaseFinal = new Date($rootScope.fechaInicioFaseFinal);


	
$scope.oneAtATime = true;

}]);
