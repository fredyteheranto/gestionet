santanderApp.controller('informacionController', ['$scope', '$routeParams', '$translate', function ($scope, $routeParams, $translate) {
    $scope.pantalla = $translate.instant('Informacion.' + $routeParams.pantalla);
    $scope.nombre = $translate.instant('Informacion.' + $routeParams.nombre);
    $scope.parrafo_uno = $translate.instant('Informacion.'+ $routeParams.parrafo_uno);
    $scope.parrafo_dos = $translate.instant('Informacion.' + $routeParams.parrafo_dos);



    $scope.volver = function () {
        window.history.back();
    };


}]);
