santanderApp.controller('presupuestosController', ['$scope', 'dataService','$rootScope', '$location','$translate', '$filter', function ($scope, dataService, $rootScope, $location,$translate, $filter) {
    
	$scope.parseInt = parseInt;
	$scope.origenProyecto = ['Presupuestos.mercadoLocal', 'Presupuestos.mercadoNacional', 'Presupuestos.mercadoInternacional'];
    //$scope.fechaEntrega = ["Enero - Año 1", "Febrero - Año 1", "Marzo - Año 1", "Abril - Año 1", "Mayo - Año 1", "Junio - Año 1","Julio - Año 1", "Agosto - Año 1", "Septiembre - Año 1", "Octubre - Año 1", "Noviembre - Año 1", "Diciembre - Año 1", "Enero - Año 2", "Febrero - Año 2", "Marzo - Año 2"];
    $scope.fechaEntrega = [];
	
	$scope.presupuesto = 0;
	$scope.presupConvertido = 0;
	
	$scope.presupuestosInfo = [];
	
	//variable de referencia para ajsutar las fechas
	if ($rootScope.tipoPartida == 1)
	{
		//singleplayer, momento de la empresa, al ser igual queda 0
		$scope.momentoReferencia = 0;
	}
	else
	{
		// multi, momento del mercado
		$scope.momentoReferencia = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral; 
	}
	
    for (var i = 0; i < 24; i++) {
        $scope.fechaEntrega.push($translate.instant('Subvenciones.FechaConAnio.' + i))
    };
    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombrePresupuestos/parrafo_unoPresupuestos/parrafo_dosPresupuestos');
    };

    dataService.getProyectosSolicitables($rootScope.idEmpresaGeneral,$rootScope.momentoGeneral).then(function (result) {
        $scope.presupuestosInfoObject = result.data.datos;
		$scope.presupuestosInfo = objetcToArray($scope.presupuestosInfo, $scope.presupuestosInfoObject);
        $scope.ningunPresupuesto = $scope.presupuestosInfo.length == 0;
    });

    $scope.solicitarProyecto = function(idProyecto, presupuesto, costeEstimado){
        $scope.idProyecto = idProyecto;
        
        bdatosCorrectos = true;
		//console.log("presupuesto inicial: " + presupuesto);
		/*presupuesto = $filter('unformat')(presupuesto);
		console.log("presupuesto adecuado: " + presupuesto); */
        if( parseInt(presupuesto) < parseInt(costeEstimado)) {
            $scope.errorPresupuesto = $translate.instant('Ups.errorPresupuesto');
            $rootScope.mostrarUps = true;
            bdatosCorrectos = false;
        }
		$scope.presupuesto = presupuesto;
        if (bdatosCorrectos)
        {
            dataService.solicitarProyecto($rootScope.idEmpresaGeneral,$scope.idProyecto, $scope.presupuesto, $rootScope.momentoGeneral).then(function (result) {

                var indexToDelete = indexToBeDeleted($scope.presupuestosInfo, $scope.idProyecto);
                $scope.presupuestosInfo.splice(indexToDelete, 1);
                swal($translate.instant('Alertas.operacionCorrecta'));
            });
        }
    };

    var indexToBeDeleted = function (array, id) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].idProyecto == id) {
                return i;
            }
        }
        return false;
    }
	
	var objetcToArray = function ($array, $object) {
		//reset
		$array = [];
		angular.forEach($object, function(element) {
			$array.push(element);
		});
		return $array;
	};	
}]);
