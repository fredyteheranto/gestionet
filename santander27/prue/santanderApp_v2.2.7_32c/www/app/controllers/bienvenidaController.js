santanderApp.controller('bienvenidaController', ['$scope','dataService','$location','$rootScope', function ($scope,dataService,$location,$rootScope) {
    $scope.noMostrar;
    $scope.showActions = true;
    $scope.showActions2 = false;
    $scope.showActions3 = false;
    if(window.localStorage.getItem("video") == "true"){
        $location.path("/menuPrincipal");
    }
    $scope.continuar = function(){

//        var dispositivo = navigator.userAgent.toLowerCase();
//        if( dispositivo.search(/iphone|ipod|ipad|android/) > -1 ){
//            if(device.platform != 'Android'){
//                window.localStorage.setItem("android", false);
//            }
//            else{
//                window.localStorage.setItem("android", true);
//            }
//        }


        if($scope.noMostrar){
            dataService.continuar($rootScope.loggedInUserId).then(function(){
                $location.path("/menuPrincipal");
            });
           // $location.path("/menuPrincipal");
        }else{
            $location.path("/menuPrincipal");
        }

    };

	/* check idioma */
	if ($rootScope.idioma == 'de')
	{
		$(".logoBienvenida").addClass("de");
		$(".logoBienvenida img").attr('src','img/Logo_Incio_de.png');
	}
	else
	{
		$(".logoBienvenida").removeClass("de");
		$(".logoBienvenida img").attr('src','img/Logo_Incio.png');
	}
	

}]);
