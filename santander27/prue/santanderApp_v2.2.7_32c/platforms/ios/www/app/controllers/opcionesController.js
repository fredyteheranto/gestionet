santanderApp.controller('opcionesController', ['$scope','$translate', '$location','$translate','$rootScope','dataService', function ($scope,$translate,$location,$translate,$rootScope,dataService) {
	$scope.languagedata;

	$scope.changeLanguage = function(idioma){
        //$translate.use(idioma);
		$rootScope.language(idioma);
		$rootScope.idioma = idioma;

    };

    $scope.aceptar = function(idioma){
		$scope.saveCurrency();
		switch (idioma){ 
			case 'es':
				$scope.languagedata = 'spanish';
				break;
			case 'en':
				$scope.languagedata = 'english';
				break;
			case 'pt':
				$scope.languagedata = 'portugues';
				break;
			case 'de':
				$scope.languagedata = 'deutsche'
		}
		if (idioma == 'de')
		{
			$("#constelacionFormacion").hide();
			$("#constelacionBecas img").attr('src','img/ico_APP_Becas_72x72_de.png');
			$(".logo img").attr('src','img/Logo_Incio_de.png');
		}
		else
		{		
			$("#constelacionFormacion").show();
			$("#constelacionBecas img").attr('src','img/ico_APP_Becas_72x72.png');
			$(".logo img").attr('src','img/Logo_Incio.png');
		}
		//console.log("aceptar: " + $scope.languagedata);
		dataService.actualizaridioma($scope.languagedata);
        $location.path("/menuPrincipal");
    }
	
	$scope.loadCurrency = function(){
		
		//console.log("loadCurrency : " + $rootScope.MonedaActual);
		if ($rootScope.MonedaActual != null)
		{
			$scope.togDif = $rootScope.MonedaActual;
		}
		else
		{
			//inicializar ambas variables de moneda
			$scope.updateCurrency($rootScope.MONEDAS.EURO);			
			$rootScope.saveCurrency($rootScope.MONEDAS.EURO);
		}
		//console.log("Moneda INICIAL : " +  $rootScope.MonedaActual);
	}
	
	$scope.updateCurrency = function(seleccion){
		$scope.togDif = seleccion;
		//console.log("Moneda Seleccionada : " + $scope.togDif);
	}
	
	$scope.saveCurrency = function(){
		$rootScope.saveCurrency($scope.togDif);
	}
	
	$scope.loadCurrency();
	
}]);

