santanderApp.controller('PartidaController', ['$scope','dataService','$location','$rootScope','$translate', function ($scope,dataService,$location,$rootScope,$translate) {
    $scope.tipocaso= 1;
    $scope.dificultad = 1;
    $scope.tipoPartida = 1;
    $scope.idEmpresa;
    $scope.idMundo;
    $scope.tipoImpositivo;
    $scope.visible = false;
    $scope.guardarFundacion = function(num){
        $scope.tipocaso = num;
    };
    $scope.guardarDificultad = function(num){
        $scope.dificultad = num;
		
    };
	
	$scope.guardarIdMundoMulti = function(id){
        $scope.idMundoMulti = id;
		$scope.togFund=id;
		console.log("PartidaController idMundoMulti " + $scope.idMundoMulti);
		console.log("PartidaController multi " + $scope.togFund);

		$scope.visible = true;
    };

	$scope.cargarPartida = function(){
		
		dataService.cargarPartida($rootScope.loggedInUserId, $rootScope.tipoPartida).then(function (result){
			$rootScope.idEmpresaGeneral = result.data.idEmpresa;
			$rootScope.idMundoGeneral = result.data.idMundo;
			$rootScope.momentoGeneral = result.data.momento;
			$scope.empresaInfo = result.data;
			var idEmpresa = result.data.idEmpresa;
			$rootScope.showMenu = true;			
			//$rootScope.tipoPartida = 1;
			if ($rootScope.tipoPartida == 1)
				$location.path('/miEmpresa/' + idEmpresa);
			$rootScope.isLogged = true;
			//si es partida multi obtenemos info del mercado para ver su estado etc..
			if ($rootScope.tipoPartida == 2)
				$rootScope.getInfoMercadoMulti();
		});

	};	
	
    $scope.crearNuevaPartida = function(){
        //console.log("Entra en crearNuevaPartida");
        dataService.crearNuevaPartida($scope.tipocaso,$scope.dificultad, $rootScope.tipoPartida, $scope.tipoImpositivo, $rootScope.loggedInUserId).then(function (result) {
            var idEmpresa = result.data.idEmpresa;

            $location.path('/miEmpresa/' + idEmpresa);
            $rootScope.idEmpresaGeneral = result.data.idEmpresa;
            $rootScope.idMundoGeneral = result.data.idMundo;
            window.localStorage.setItem("gameOverSingle", false);
            $rootScope.partidaTerminadaSingle = window.localStorage.getItem("gameOverSingle");
			$rootScope.gameOverSingle = false;
			

        });

        $rootScope.showMenu = true;
        $rootScope.isLogged = true;
    };
    $scope.logout = function () {
        //Hacer el logout
    };
	
    $scope.crearNuevaPartidaSingle = function(){
		$rootScope.tipoPartida = 1;
		$scope.checkPartidaExistente(false);
		$rootScope.mostrarDiezMinAlerta = false;
		
    };
   $scope.crearNuevaPartidaMulti = function(){
        $rootScope.tipoPartida = 2;
		$scope.checkPartidaExistente(false);	
		window.localStorage.setItem("entroPorMenu", true);	
		window.localStorage.setItem("menosDecionesMundoQueDecEmpresa", true);
			window.localStorage.setItem("unaDecisionReinicieMundo", true);
    };
	
	$scope.continuarPartidaSingle = function(){
		$rootScope.tipoPartida = 1;
		$scope.checkPartidaExistente(true);

		
    };
	$scope.continuarPartidaMulti = function(){
        $rootScope.tipoPartida = 2;
		$scope.checkPartidaExistente(true);
		$rootScope.mostrarDiezMinAlerta = false;
		window.localStorage.setItem("entroPorMenu", true);
		window.localStorage.setItem("menosDecionesMundoQueDecEmpresa", true);
		window.localStorage.setItem("unaDecisionReinicieMundo", true);
    };
	
	$scope.checkPartidaExistente = function (esContinuar) {
		if($rootScope.loggedInUserId) {
			dataService.checkPartidaExistente($rootScope.loggedInUserId, $rootScope.tipoPartida).then(function (result) {
				if (result.data.status == "success")
				{
					$scope.idEmpresa= result.data.idEmpresa;
					$scope.idMundo=result.data.idMundo;
					if (result.data.idEmpresa)
					{
						//existe partida
						if (!esContinuar)
						{
							//al ser nueva, preguntar si se quiere borrar la actual
							swal({
								title: $translate.instant('Alertas.PasoJornadaPartidaEnCurso'),
								text: $translate.instant('Alertas.PasoJornadaPartidaEnCursoTexto'),
								type: "warning",
								showCancelButton: true,
								confirmButtonColor: "#889641",
								confirmButtonText: $translate.instant('Alertas.si'),
								cancelButtonText: $translate.instant('Alertas.no'),
								closeOnConfirm: false
							},
							function(isConfirm)
							{
								if (isConfirm)
								{
									$scope.terminarEmpezarPartida(true);
								}
								else
								{
									//no hacer nada
								}
							});
						}
						else
						{
							//cargar directamente
							if($rootScope.tipoPartida == 1){
								$scope.hayPartida = true;
							}else if($rootScope.tipoPartida == 2){
								$rootScope.hayPartidaMulti = true;
							}
							$scope.cargarPartida();

						}
					}
					else
					{
						//no existe partida
						if (!esContinuar)
							$scope.terminarEmpezarPartida(false);
						else
						{
							//comprobar si jugó otras partidas para mostrar su última puntuación
							if (result.data.puntuacion != -1)
							{
								var aviso = $translate.instant('Alertas.sinPartida') + $translate.instant('Alertas.ultimaPuntuacion_01') + result.data.puntuacion + 
									$translate.instant('Alertas.ultimaPuntuacion_02');
								swal(aviso);
							}
							else
							{
								//No tenía partidas anteriores
								swal($translate.instant('Alertas.sinPartida'));
							}
							
						}
					}
				}
				else
				{
					swal($translate.instant('MenuPrincipal.error'));					
				}
			});
		}
		else
		{
			//crear usuario?
			swal($translate.instant('Alertas.sinPartida'));
		}
	}
	/*
	$scope.checkPartidaExistente_OLD = function (tipoPartida) {
		$scope.formSubmitted = true;
		$scope.idEmpresa;
		$scope.idMundo;
		$scope.puntuacion;
		$scope.hayPartida = false;
		$rootScope.showMenu= false;
		if($rootScope.loggedInUserId) {
			dataService.checkPartidaExistente($rootScope.loggedInUserId, tipoPartida).then(function (result) {
				if (result.data.status == "success")
				{
					$scope.idEmpresa= result.data.idEmpresa;
					$scope.idMundo=result.data.idMundo;
					if($scope.idEmpresa== null){
						$scope.hayPartida = false;
						swal($translate.instant('Alertas.sinPartida'));
					} else {
						$rootScope.tipoPartida = tipoPartida;
						$scope.hayPartida = true;
						$scope.cargarPartida();
					}
					//console.log("Empresa " + $scope.idEmpresa + "Mundo " + $scope.idMundo);
				}
				else
				{
					//poner el alert cuando acabe el menú
					swal($translate.instant('MenuPrincipal.error'));

				}
			});
		}else{
			swal($translate.instant('Alertas.sinPartida'));
		}
	};

	$scope.nuevaPartida = function ($event) {
        //creación de usuario en menuprincipalcontroller
		if($scope.idEmpresa== null || $scope.idEmpresa==undefined ){
			$rootScope.showAyuda= false;
			$scope.terminarEmpezarPartida(false);
		}
		else if ($scope.idEmpresa!= null){
			swal({
					title: $translate.instant('Alertas.PasoJornadaPartidaEnCurso'),
					text: $translate.instant('Alertas.PasoJornadaPartidaEnCursoTexto'),
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#889641",
					confirmButtonText: $translate.instant('Alertas.si'),
					cancelButtonText: $translate.instant('Alertas.no'),
					closeOnConfirm: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						$scope.terminarEmpezarPartida(true);
					}
				});
		}
		//$rootScope.showMenu = true;
	};
	*/
    $scope.terminarEmpezarPartida = function(puntuacionAnterior){
		//RESETEAR VARIABLES DE LA APP
        $rootScope.prestamoUnoPorJornada = true;
		
        if(puntuacionAnterior) {
			//existía partida, la cerramos y mostramos su puntuación
            if ($rootScope.tipoPartida == 1)
			{
				//single
				dataService.terminarEmpezarPartida($scope.idEmpresa, $scope.idMundo, $rootScope.tipoPartida).then(function (result) {
					$scope.puntuacion = result.data.puntuacion;
					swal(($translate.instant('Alertas.PasoJornadaPuntuacion') + $scope.puntuacion));
					//alert("Su puntuación era: " + $scope.puntuacion);				
					//redirigimos
					if(window.localStorage.getItem("noMostrarAyuda") != 'false'){
						$location.path("/ayudaConfigurarSingle");
					}else if(window.localStorage.getItem("noMostrarAyuda") == 'false'){
						$location.path("/nuevaPartidaSingle");
					}	
				});
			}
			else
			{
				//multi
				dataService.salirDePartidaMulti($scope.idEmpresa, $scope.idMundo, $rootScope.tipoPartida).then(function (result) {
					$scope.puntuacion = result.data.puntuacion;
					swal(($translate.instant('Alertas.PasoJornadaPuntuacion') + $scope.puntuacion));
					//redirigimos
					if(window.localStorage.getItem("noMostrarAyudaMulti") != 'false'){
						$location.path("/ayudaConfigurarMulti");
					}else if(window.localStorage.getItem("noMostrarAyudaMulti") == 'false'){
						$location.path("/nuevaPartidaMulti");
					}
				});
			}
        }
		else
		{
			//no existe ninguna partida, redirigimos directamente
			if ($rootScope.tipoPartida == 1){
				if(window.localStorage.getItem("noMostrarAyuda") != 'false'){
					$location.path("/ayudaConfigurarSingle");
				}else if(window.localStorage.getItem("noMostrarAyuda") == 'false'){
					$location.path("/nuevaPartidaSingle");
				}
				
			}
			else{
				if(window.localStorage.getItem("noMostrarAyudaMulti") != 'false'){
					$location.path("/ayudaConfigurarMulti");
				}else if(window.localStorage.getItem("noMostrarAyudaMulti") == 'false'){
					$location.path("/nuevaPartidaMulti");
				}
			}
		}
		/*
		if(window.localStorage.getItem("noMostrarAyuda") != 'false'){
			$location.path("/ayudaConfigurar");
		}else if(window.localStorage.getItem("noMostrarAyuda") == 'false'){
			$location.path("/nuevaPartida");
		}
		*/
    };	
	
	var checkPartida = function () {
		$scope.formSubmitted = true;
		$scope.idEmpresa;
		$scope.idMundo;
		$scope.puntuacion;
		$scope.hayPartida = false;
		$rootScope.showMenu= false;
		if($rootScope.loggedInUserId) {

			dataService.getInfoMantenimiento().then(function (result){
				if(result.data.status[0].estado == 1){
					$location.path("/mantenimiento");
				}else{
					dataService.checkPartidaExistente($rootScope.loggedInUserId).then(function (result) {
						if (result.data.status == "success")
						{
							$scope.idEmpresa= result.data.idEmpresa;
							$scope.idMundo=result.data.idMundo;
							if($scope.idEmpresa== null){
								$scope.hayPartida = false;
							} else {
								$scope.hayPartida = true;
							}
							//console.log("Empresa " + $scope.idEmpresa + "Mundo " + $scope.idMundo);
						}
						else
						{
							//poner el alert cuando acabe el menú
							swal($translate.instant('MenuPrincipal.error'));
						}
					});
				}
			});
		}
	};
	//checkPartida();	
	
	var getPartidasMultiDisponibles = function () {
	
		dataService.getPartidasMultiDisponibles().then(function (result){
			if (result.data.status == "success")
			{
				$scope.mundosMulti = result.data.mundos;
			}
			else
			{
				//poner el alert cuando acabe el menú
				swal($translate.instant('MenuPrincipal.error'));
			}
		});
	};
	
	$scope.unirseAPartidaMulti = function () { 	
		console.log("PartidaController unirseAPartidaMulti " + $scope.idMundoMulti);
		dataService.unirseAPartidaMulti($scope.idMundoMulti,$rootScope.loggedInUserId, $rootScope.tipoPartida).then(function (result){
            if (result.data.status == "success")
			{
				var idEmpresa = result.data.idEmpresa;
				//$location.path('/miEmpresa/' + idEmpresa);
				$rootScope.idEmpresaGeneral = result.data.idEmpresa;
				$rootScope.idMundoGeneral = result.data.idMundo;				
				window.localStorage.setItem("gameOverMulti", false);
				window.localStorage.setItem("pasoenProgreso", false);
				$rootScope.partidaTerminadaMulti = window.localStorage.getItem("gameOverMulti");
				$rootScope.gameOverMulti = false;
				$rootScope.showMenu = true;
				$rootScope.isLogged = true;	

				window.localStorage.setItem("partidaMultiEmpezada", true);
                
				//si es partida multi obtenemos info del mercado para ver su estado etc..
				$rootScope.getInfoMercadoMulti();
			}
			else
			{
				//poner el alert cuando acabe el menú, error al unirse a partida multi
				swal($translate.instant('MenuPrincipal.error'));
			}
        });
	};
	
	
	//guardar tipo de partida (single, multi) para saber qué ejecutar dependiendo de la pantalla
	//console.log("PartidaController init tipopartida " + $rootScope.tipoPartida);	
	if ($rootScope.tipoPartida == 2)
	{
		getPartidasMultiDisponibles();
	}
   
}]);
