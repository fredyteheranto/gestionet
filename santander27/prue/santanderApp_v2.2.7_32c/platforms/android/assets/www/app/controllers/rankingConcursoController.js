santanderApp.controller('rankingConcursoController', ['$scope', 'dataService', '$rootScope','$location','$translate', function ($scope, dataService, $rootScope,$location,$translate) {
    $scope.showRanking = true;
     // 2 ranking personal
    $scope.NoHayDatos = false;
    $scope.pais;
    $scope.paisSinSeleccionar = -1;
    $scope.tipo = "3";
    
    //$rootScope.idConcursoMundoBase - Aqui traigo la información de las fases, ahora paso -1 para coger todas 
	var getPaisesConcurso = function(){
		dataService.getPaisesConcurso().then(function (result){
			 $scope.paises = result.data.datos;
		});
	}
	
	dataService.getFasesConcurso($rootScope.idConcurso).then(function(result){
		var longitud = result.data.datos.length;
		$scope.rankingFinal = false;
		for(var i=0; i < longitud; i++){
			//fase final
			if (result.data.datos[i].tipoMundo == '4' && (result.data.datos[i].estadoFase == '1' || result.data.datos[i].estadoFase == '2' || result.data.datos[i].estadoFase == '3')){
				$scope.rankingFinal = true;
				cargarRanking ( -1, $scope.paisSinSeleccionar, "4", 1 );
			}
		}
		if (!$scope.rankingFinal)
		{
			//getPaisesConcurso();
			cargarRanking ( -1, $scope.paisSinSeleccionar, "4", 0 );
		}
	});	
	
	//FALTA COGER EL TIPO
    $scope.seleccionarPais = function(  ){
        cargarRanking (-1, $scope.pais, "3");
    };

    var convertirAFecha = function() {
        angular.forEach($scope.ranking, function(fila) {
            fila.fechaInicio = new Date(fila.fechaInicio);			
        })
    };
	var convertirAEntero = function() {
        angular.forEach($scope.ranking, function(fila) {
			fila.puntuacion = parseInt(fila.puntuacion);
        })
    };

  
    var cargarRanking = function ( concursoMundoBase, pais, tipo, esFinal ) {
		dataService.getRankingConcurso($rootScope.idConcurso, $rootScope.idUsuarioConcurso, concursoMundoBase, pais, 50, tipo, esFinal).then(function (result){
			
			if (result.data.datos == null || result.data.datos == ''){
                $scope.NoHayDatos = true;
                $scope.Datos = $translate.instant('Ranking.noDatos');
                $scope.mostrar = false;
            }else{
                $scope.ranking = result.data.datos.listado;
                $scope.posicion = result.data.datos.posicionUsuario;
                $scope.mostrar = true;
                if($scope.posicion <= 5){
                    $scope.mostrar = false;
                }
            }

            //convertirAFecha();
            convertirAEntero();
		});
	};

}]);
