santanderApp.controller('prestamosController', ['$scope', 'dataService','$location','$rootScope', '$translate', 'tmhDynamicLocale', '$locale','$filter', function ($scope, dataService,$location,$rootScope,$translate, tmhDynamicLocale, $locale,$filter) {
    $scope.importeMax;
	$scope.importeMaxFormateado;
    $scope.tipoInteres;
	$scope.tipoInteresFormateado;
    $scope.plazo;
    $scope.importe = 500;
	$scope.importeConvertido;
    $scope.datos;
    $scope.idPrestamo;
    $scope.prestamosInfo;
    $scope.prestamosInfoCancelados;
    $scope.showActions = false;
    $scope.showLeyendas = true;
	$scope.prestamoMenorMinimo = false;
    $scope.importeMinimo = $rootScope.convertirMonedaBaseActual(1000);

    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombrePrest/parrafo_unoPrest/parrafo_dosPrest');
    };
    $scope.pedirPrestamos = function(tipoInteres, plazo, importe){
		//console.log("click pedirPrestamos scope" + $scope.importe);
		//console.log("click pedirPrestamos param" + importe);
        $scope.tipoInteres =tipoInteres;
        $scope.plazo = plazo;
        $scope.importe = importe;
		bDatosCorrectos = true;

        $scope.errorPrestamosPlazo= '';
        $scope.errorPrestamosImporteMenor= '';
        $scope.errorPrestamosImporteMaximo= '';

     if (($scope.plazo == 0) ||($scope.importe < $scope.importeMinimo) || ($scope.importe > $scope.importeMax))
        {
            if($scope.plazo == 0){
                $scope.errorPrestamosPlazo = $translate.instant('Ups.errorPrestamosPlazo');
            }
            if ($scope.importe < $scope.importeMinimo)
            {
                $scope.errorPrestamosImporteMenor =  $translate.instant('Ups.errorPrestamosImporteMenor') + $filter('mycurrency')($scope.importeMinimo);

            }
            if ($scope.importe > $scope.importeMax)
            {
               $scope.errorPrestamosImporteMaximo = $translate.instant('Ups.errorPrestamosImporteMaximo');;
            }
            $rootScope.mostrarUps = true;
            bDatosCorrectos = false;
        }

		if (bDatosCorrectos)
		{
			$scope.datos = JSON.stringify({Cnt: importe, Plz: plazo, Int:tipoInteres});
			dataService.pedirPrestamos($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $rootScope.momentoGeneral,$scope.datos).then(function (result){				
				$scope.getDataPrestamos();
                if(result.data.status == 'success'){
                    $rootScope.prestamoUnoPorJornada = false;
                    swal($translate.instant('Alertas.operacionCorrecta'));
                }


			});
		}
    };

    $scope.getDataPrestamos = function(){
        dataService.getDataPrestamos($rootScope.idEmpresaGeneral,$rootScope.idMundoGeneral, $rootScope.momentoGeneral).then(function (result){
            $scope.importeMax = Math.ceil(result.data.datos.max);
			$scope.importeMaxFormateado = result.data.datos.maxFormateado;
            $scope.tipoInteres = parseFloat(result.data.datos.TDI) + parseFloat(result.data.datos.Difprest);
			$scope.tipoInteresFormateado = $scope.tipoInteres;
            $scope.prestamosInfo = result.data.datos.prestamosA;
            $scope.prestamosInfoActual = result.data.datos.prestamosAc;
            $scope.prestamosInfoCancelados = result.data.datos.prestamosC;
			//console.log("datos recibidos" + $scope.importeMax);
			//evento para actualizar valores
			//$rootScope.$emit("loadedDataEvent");
			//adecuar opción de mostrar botón para pedir préstamos
			if ($scope.importeMax < 1000)
				$scope.prestamoMenorMinimo = true;

            $scope.difPrestamo = parseFloat(result.data.datos.Difprest);
            var cargarGrafica = function(){
                var momentosPasados = [];
                var generateData = function() {
                    var data = [];
                    var i;
					var margen = 0;
                    if($rootScope.idEscenario == '21' || $rootScope.idEscenario == '22' || $rootScope.idEscenario == '23'){
                        i=4;
                    }else if($rootScope.idEscenario == '31' || $rootScope.idEscenario == '32' || $rootScope.idEscenario == '33'){
                        i=10;
                    }
                    else{
                        i=-1;						
						if ($rootScope.tipoPartida != 1)
						{
							//adecuar inicio de datos de tdi para ajustar la gráfica al momento correcto del mundo
							margen = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral;
						}
                    }
                    for(i; i <= $rootScope.momentoGeneral; ++i) {
						console.log(i+margen);
                        momentosPasados = parseFloat(result.data.datos.HistoricoTDI[i+margen]) + $scope.difPrestamo;
                        data.push(momentosPasados);
                    }
                    return data;
                };

                var ctx = document.getElementById("chartLine").getContext("2d");
                var data = {
                    labels: $rootScope.devolverMesGrafica($rootScope.momentoGeneral, $rootScope.idEscenario),
                    datasets: [
                        {
                            label: "Diferencial de préstamos",
                            fillColor: "rgba(3,151,20,0)",
                            strokeColor: "#D0A426",
                            pointColor: "#D0A426",
                            pointStrokeColor: "#D0A426",
                            pointHighlightFill: "#D0A426",
                            pointHighlightStroke: "#D0A426",
                            data: generateData()
                        }
                    ]
                };
                new Chart(ctx).Line(data, {
                    showTooltips: true,
                    animation: true,
                    responsive: true
                });

            }

            cargarGrafica();



        });
    };

    $scope.descancelarPrestamos = function(idPrestamo){
        $scope.idPrestamo = idPrestamo.idPrestamo;
        $scope.datos = JSON.stringify({idP: $scope.idPrestamo});
        dataService.descancelarPrestamos($scope.datos).then(function (result){
            $scope.getDataPrestamos();
            swal($translate.instant('Alertas.operacionCorrecta'));
        });
    };

    $scope.cancelarPrestamos = function(idPrestamo){
        $scope.idPrestamo = idPrestamo.idPrestamo;
        $scope.datos = JSON.stringify({idP: $scope.idPrestamo});
        dataService.cancelarPrestamos($scope.datos).then(function (result){
            $scope.getDataPrestamos();
            swal($translate.instant('Alertas.operacionCorrecta'));
        });
    };

    $scope.getDataPrestamos();



}]);
