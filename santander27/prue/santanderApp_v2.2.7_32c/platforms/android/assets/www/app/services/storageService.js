
santanderApp.factory('storageService', function ($rootScope) {
	var namespace = "santanderApp";
	
	var initialize = function (key) {
        window.ss = new cordova.plugins.SecureStorage(
			function () { 
				console.log('Init Success');				
			},
			function (error) { 
				console.log('Init Error ' + error); 				
			},
			namespace);
    };
	
	var loadData = function () {
		$rootScope.loggedInUserId = null;
		$rootScope.loggedInUserName = null;
		$rootScope.loggedInUserPass = null;
		window.ss.get(
			function (loggedInUserId) { 
				console.log('loadData Success, got ' + loggedInUserId); 
				
				if ( (loggedInUserId == "undefined") || (loggedInUserId == null) )
				{
					console.log("loadedData " + loggedInUserId);
					$rootScope.loggedInUserId = null;
				}
				if (loggedInUserId != null) {
					//Hay usuario
					console.log("loadedData Hay usuario ");
					$rootScope.loggedInUserId = loggedInUserId;
					window.ss.get(
						function (loggedInUserName) { 
							$rootScope.loggedInUserName = loggedInUserName;
							window.ss.get(
								function (loggedInUserPass) { 
									$rootScope.loggedInUserPass = loggedInUserPass;
									console.log("loggedInUserId " + loggedInUserId);
									console.log("loggedInUserName " + loggedInUserName);
									console.log("loggedInUserPass " + loggedInUserPass);
								},
								function (error) { 
									console.log('loadData Error ' + error); 
									return null;
								},
								"loggedInUserPass");
						},
						function (error) { 
							console.log('loadData Error ' + error); 
							return null;
						},
						"loggedInUserName");	
					$rootScope.showAyuda = true;
					//Recibir valor de mostrar ayuda con el idUsuario
				}			
			},
			function (error) { 
				console.log('loadData Error ' + error); 
				return null;
			},
			"loggedInUserId");		
	};
	
	var storeData = function () {
		if ($rootScope.loggedInUserId)
		{
			window.ss.set(
				function (loggedInUserId) { 
					console.log('storeData Success for ' + loggedInUserId); 
					window.ss.set(
						function (loggedInUserName) { 
							console.log('storeData Success for ' + loggedInUserName); 
							window.ss.set(
								function (loggedInUserPass) { 
									console.log('storeData Success for ' + loggedInUserPass);
									navigator.app.exitApp();
								},
								function (error) { 
									console.log('storeData Error ' + error); 
									navigator.app.exitApp();
								},
								"loggedInUserPass", $rootScope.loggedInUserPass);
						},
						function (error) { 
							console.log('storeData Error ' + error); 
							navigator.app.exitApp();
						},
						"loggedInUserName", $rootScope.loggedInUserName);
				},
				function (error) { 
					console.log('storeData Error ' + error); 
					navigator.app.exitApp();
				},
			"loggedInUserId", $rootScope.loggedInUserId);	
		}
		else
		{
			console.log("no credentials to store");
			navigator.app.exitApp();
		}
	};

	var clearData = function () {
		window.ss.remove(
			function (loggedInUserId) { 
				console.log('clearData Success for ' + loggedInUserId); 
				window.ss.remove(
					function (loggedInUserName) { 
						console.log('clearData Success for ' + loggedInUserName); 
						window.ss.remove(
							function (loggedInUserPass) { 
								console.log('clearData Success for ' + loggedInUserPass);								
							},
							function (error) { 
								console.log('clearData Error ' + error); 								
							},
							"loggedInUserPass");
					},
					function (error) { 
						console.log('clearData Error ' + error);						
					},
					"loggedInUserName");
			},
			function (error) { 
				console.log('clearData Error ' + error);				
			},
		"loggedInUserId");
	};	
	
    var getData = function (key) {
		var val = null;
		window.ss.get(
			function (value) { 
				console.log('getData Success, got ' + value); 
				return value;
			},
			function (error) { 
				console.log('getData Error ' + error); 
				return null;
			},
			key);
    };
	
	var setData = function (key, value) {
		 window.ss.set(
			function (key) {
				console.log('setData Set ' + key);				
			},
			function (error) { 
				console.log('setData Error ' + error); 
			},
			key, value);
    };
	
	var removeData = function (key, value) {
		window.ss.remove(
			function (key) { 
				console.log('removeData Removed ' + key); 
			},
			function (error) { 
				console.log('removeData Error, ' + error); 
			},
			key);
    };

    var service = {
        initialize: initialize,
		loadData: loadData,
		storeData: storeData,
		getData: getData,
		setData: setData,
		removeData: removeData,
		clearData: clearData
    };
    return service;
});