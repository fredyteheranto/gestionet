santanderApp.controller('efeController', ['$scope', '$routeParams', 'dataService','$location','$rootScope', function ($scope, $routeParams, dataService,$location,$rootScope) {

	$scope.momentoVisualizado = $rootScope.momentoGeneral;
	$scope.mesVisualizada = $rootScope.mesActual;
	$scope.dataBwdAvailable = false;
	$scope.dataFwdAvailable = false;

    $scope.pantallaInformacion = function(){
        $location.path('informacion/pantalla/nombreEfe/parrafo_unoEfe/parrafo_dosEfe');
    };
	$scope.historico = function(direccion){
		//adecuamos la jornada a visualizar
		$scope.momentoVisualizado = parseInt($scope.momentoVisualizado) + parseInt(direccion);
		if ($scope.momentoVisualizado < $rootScope.momentoGeneral)
		{
			$scope.dataFwdAvailable = true;
		}
		else
		{
			$scope.momentoVisualizado = $rootScope.momentoGeneral;
			$scope.dataFwdAvailable = false;
		}
			
		if  ($scope.momentoVisualizado > 0)
		{
			$scope.dataBwdAvailable = true;
		}
		else
		{
			$scope.dataBwdAvailable = false;
			$scope.momentoVisualizado = 0;
		}
		//actualizamos fecha visualizada
		if ($rootScope.tipoPartida == 1)
		{
			$scope.mesVisualizado = $rootScope.devolverMes($scope.momentoVisualizado);
		}
		else
		{
			$scope.momentoVisualizadoMundo = $rootScope.mundoMultiData.mundo.jornadaActual - $rootScope.momentoGeneral + parseInt($scope.momentoVisualizado);
			$scope.mesVisualizado = $rootScope.devolverMes($scope.momentoVisualizadoMundo);
		}
		
		//llamamos al servidor para obtener los datos de esa jornada
		dataService.getEFE($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $scope.momentoVisualizado-1).then($scope.showData);
	};


//    dataService.getEFE($rootScope.idEmpresaGeneral, $rootScope.idMundoGeneral, $rootScope.momentoGeneral).then(function (result){
	$scope.showData = function (result){
		$scope.efeInfo = result.data;
    };

	//inicializar
	$scope.historico(0);	
	
}]);
